import 'package:flutter/material.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';

/// A responsive Flutter widget that centers its child within a responsive grid layout.
///
/// This widget uses the `ResponsiveGrid` package to create a layout with a central
/// child and filler columns on both sides to ensure centering. It is designed to
/// be responsive to different screen sizes.
class ResponsiveCenter extends StatelessWidget {
  const ResponsiveCenter({
    super.key,
    required this.child,
    this.alignment = Alignment.center,
    this.margin = 16,
    this.segments = 12,
    this.xs = 12,
    this.sm,
    this.md,
    this.lg,
    this.xl,
  });
  final Widget child;
  final Alignment alignment;
  final double margin;
  final int segments;
  final int xs;
  final int? sm;
  final int? md;
  final int? lg;
  final int? xl;

  /// Calculates the number of filler columns needed to center the child within the grid.
  ///
  /// The function ensures that the total number of columns is even and returns the
  /// calculated filler value.
  int calc(int cols) {
    if (cols == 0) return 0;
    assert(cols.isEven,
        "To align center, the amount of columns should always be even. Got: $cols");
    int result = (segments - cols) ~/ 2;
    return result;
  }

  @override
  Widget build(BuildContext context) {
    final filler = ResponsiveGridCol(
        xs: calc(xs),
        sm: sm != null ? calc(sm!) : null,
        md: md != null ? calc(md!) : null,
        lg: lg != null ? calc(lg!) : null,
        xl: xl != null ? calc(xl!) : null,
        child: const SizedBox(
          width: double.infinity,
        ));
    return Align(
      alignment: alignment,
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: maxContentWidth),
        child: ResponsiveGridRow(
          margin: margin,
          rowSegments: segments,
          children: [
            filler,
            ResponsiveGridCol(
              xs: xs,
              sm: sm,
              md: md,
              lg: lg,
              xl: xl,
              child: child,
            ),
            filler,
          ],
        ),
      ),
    );
  }
}
