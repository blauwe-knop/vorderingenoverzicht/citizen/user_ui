import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/themed_back_button.dart';
import 'package:user_ui/screens/common/themed_side_menu.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class BasicScaffold extends StatelessWidget {
  const BasicScaffold({
    super.key,
    this.title,
    this.onBackbuttonPress,
    this.showDrawer = false,
    this.useGreyBackground = false,
    this.actions = const [],
    required this.content,
  });

  final String? title;
  final bool showDrawer;
  final void Function()? onBackbuttonPress;
  final Widget content;
  final bool useGreyBackground;
  final List<Widget> actions;

  static const double backButtonLeadingWidth = 130;

  @override
  Widget build(BuildContext context) {
    final Color backgroundColor = responsiveValue(
      context,
      xs: context.colorScheme.surface,
      md: useGreyBackground
          ? context.colorScheme.surfaceContainerLowest
          : context.colorScheme.surface,
    );

    return Scaffold(
      drawer: showDrawer ? const ThemedSideMenu() : null,
      backgroundColor: backgroundColor,
      appBar: showDrawer == false &&
              onBackbuttonPress == null &&
              title == null &&
              actions.isEmpty
          ? null
          : AppBar(
              leading: onBackbuttonPress != null
                  ? ThemedBackButton(
                      onBackbuttonPress: onBackbuttonPress,
                    )
                  : showDrawer
                      ? null
                      : const SizedBox.shrink(),
              leadingWidth:
                  onBackbuttonPress != null ? backButtonLeadingWidth : null,
              backgroundColor: backgroundColor,
              foregroundColor: context.colorScheme.onSurface,
              elevation: 0.0,
              title: title != null ? Text(title!) : null,
              titleTextStyle: context.textTheme.displaySmall,
              actions: actions,
            ),
      body: SafeArea(
        child: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: maxContentWidth),
            child: content,
          ),
        ),
      ),
    );
  }
}
