import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class ThemedCloseButton extends StatelessWidget {
  const ThemedCloseButton({
    super.key,
    required this.onPressed,
    required this.color,
    this.alignment = Alignment.center,
  });
  final Color? color;
  final void Function()? onPressed;
  final Alignment alignment;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
        padding: EdgeInsets.all(0),
      ),
      onPressed: onPressed,
      child: Align(
        alignment: alignment,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            FaIcon(
              FontAwesomeIcons.xmark,
              size: IconSize.large.value,
              color: color,
            ),
            Text(
              'sluit',
              style: context.textTheme.bodyLarge?.copyWith(
                color: color,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
