import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class ThemedBackButton extends StatelessWidget {
  const ThemedBackButton({
    super.key,
    this.onBackbuttonPress,
  });

  final void Function()? onBackbuttonPress;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: onBackbuttonPress,
      style: TextButton.styleFrom(
        foregroundColor: context.colorScheme.primary,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          const BackButtonIcon(),
          Text(
            'Vorige',
            style: context.textTheme.bodyLarge?.copyWith(
              color: context.colorScheme.primary,
            ),
          ),
        ],
      ),
    );
  }
}
