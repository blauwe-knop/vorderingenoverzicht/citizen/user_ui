import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:user_ui/screens/common/themed_close_button.dart';
import 'package:user_ui/screens/features/activation/deactivation_screen.dart';
import 'package:user_ui/screens/features/clean_up/remove_old_user_data_and_use_latest_registration_screen.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/toggles/feature_toggles.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ThemedSideMenu extends ConsumerStatefulWidget {
  const ThemedSideMenu({super.key});

  @override
  ConsumerState<ThemedSideMenu> createState() => _ThemedSideMenuState();
}

class _ThemedSideMenuState extends ConsumerState<ThemedSideMenu> {
  late PackageInfo _packageInfo = PackageInfo(
    appName: 'Unknown',
    packageName: 'Unknown',
    version: 'Unknown',
    buildNumber: 'Unknown',
  );

  @override
  void initState() {
    super.initState();
    _initPackageInfo();
  }

  Future<void> _initPackageInfo() async {
    final info = await PackageInfo.fromPlatform();
    setState(() {
      _packageInfo = info;
    });
  }

  @override
  Widget build(BuildContext context) {
    final bool isDense = responsiveValue(context, xs: true, sm: false);
    return Drawer(
      backgroundColor: context.colorScheme.secondary,
      child: Padding(
        padding: EdgeInsets.all(Spacing.large.value),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ThemedCloseButton(
              onPressed: () => Scaffold.of(context).closeDrawer(),
              color: context.colorScheme.onSurface,
              alignment: Alignment.centerLeft,
            ),
            if (!isDense) addVerticalSpace(Spacing.large.value),
            ListTile(
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              title: Text(
                "Menu",
                style: context.textTheme.displayLarge,
              ),
            ),
            if (!isDense) addVerticalSpace(Spacing.xlarge.value),
            ListTile(
              title: const Text(
                "Home",
              ),
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              onTap: () {
                context.goNamed(HomeScreen.routeName);
                Scaffold.of(context).closeDrawer();
              },
            ),
            ListTile(
              title: const Text("Deactiveer de app"),
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              onTap: () {
                context.goNamed(DeactivationScreen.routeName);
                Scaffold.of(context).closeDrawer();
              },
            ),
            Spacer(),
            Divider(),
            ListTile(
              title: const Text("Data verwijderen"),
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              onTap: () {
                context.goNamed(
                    RemoveOldUserDataAndUseLatestRegistrationScreen.routeName);

                Scaffold.of(context).closeDrawer();
              },
            ),
            ListTile(
              title: const Text("Foutenlogboek"),
              contentPadding: EdgeInsets.symmetric(horizontal: 0),
              onTap: () {
                context.goNamed(ErrorLogScreen.routeName,
                    pathParameters: {'showBackButton': 'false'});
                Scaffold.of(context).closeDrawer();
              },
            ),
            ListTile(
              onTap: () {
                context.goNamed(FeatureToggleScreen.routeName);
                Scaffold.of(context).closeDrawer();
              },
              title: SvgPicture.asset(
                'assets/icons/digilab.svg',
                width: 32,
                height: 32,
              ),
            ),
            addVerticalSpace(Spacing.xlarge.value),
            Center(
              child: Text('v${_packageInfo.version}'),
            ),
          ],
        ),
      ),
    );
  }
}
