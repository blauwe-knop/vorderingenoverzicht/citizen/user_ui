// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:flutter/material.dart';
import 'package:user_ui/screens/common/themed_progress_indicator.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

enum ThemedButtonStyle {
  normal,
  primary,
  tertiary,
  delete,
  quarternary,
  quinary,
}

extension ThemedButtonStyles on ThemedButtonStyle {
  Color getTextColor(BuildContext context) {
    switch (this) {
      case ThemedButtonStyle.primary:
        return context.colorScheme.surfaceContainerLowest;
      case ThemedButtonStyle.normal:
      case ThemedButtonStyle.tertiary:
      case ThemedButtonStyle.quarternary:
        return context.colorScheme.primary;
      case ThemedButtonStyle.delete:
        return context.colorScheme.error;
      case ThemedButtonStyle.quinary:
        return context.colorScheme.onSurface;
    }
  }

  Color getBackgroundColor(BuildContext context) {
    switch (this) {
      case ThemedButtonStyle.primary:
        return context.colorScheme.primary;
      case ThemedButtonStyle.normal:
      case ThemedButtonStyle.quinary:
        return context.colorScheme.surfaceContainerLowest;
      case ThemedButtonStyle.tertiary:
      case ThemedButtonStyle.delete:
        return context.colorScheme.surface;
      case ThemedButtonStyle.quarternary:
        return context.colorScheme.surfaceContainerLowest;
    }
  }
}

class ThemedButton extends StatelessWidget {
  const ThemedButton({
    super.key,
    required this.buttonText,
    this.style = ThemedButtonStyle.normal,
    required this.onPress,
    this.width = double.infinity,
    this.buttonIcon,
    this.isLoading = false,
  });

  final void Function()? onPress;
  final String buttonText;
  final double width;
  final ThemedButtonStyle style;
  final Widget? buttonIcon;
  final bool isLoading;

  @override
  Widget build(BuildContext context) {
    final Color textColor, backgroundColor;
    if (onPress == null) {
      textColor = context.colorScheme.surfaceContainer;
      backgroundColor = context.colorScheme.surfaceContainerLowest;
    } else {
      textColor = style.getTextColor(context);
      backgroundColor = style.getBackgroundColor(context);
    }

    return SizedBox(
      width: width,
      child: ElevatedButton(
        onPressed: onPress,
        style: ElevatedButton.styleFrom(
          foregroundColor: textColor,
          disabledForegroundColor:
              onPress == null ? context.colorScheme.surfaceContainerHigh : null,
          disabledBackgroundColor: backgroundColor,
          backgroundColor: backgroundColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(RadiusSize.medium.value),
          ),
          minimumSize: Size(0, Spacing.xxlarge.value),
          elevation: 0.0,
        ),
        child: (style != ThemedButtonStyle.quinary)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (buttonIcon != null) ...[
                    buttonIcon!,
                    addHorizontalSpace(Spacing.large.value),
                  ],
                  Flexible(
                    child: FittedBox(
                      child: !isLoading
                          ? Text(
                              buttonText,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge
                                  ?.copyWith(
                                    color: textColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                            )
                          : ThemedProgressIndicator(color: textColor),
                    ),
                  ),
                ],
              )
            : Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  if (buttonIcon != null) ...[
                    buttonIcon!,
                    addHorizontalSpace(Spacing.large.value),
                  ],
                  Flexible(
                    child: FittedBox(
                      child: !isLoading
                          ? Text(
                              buttonText,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge
                                  ?.copyWith(
                                    color: textColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                            )
                          : ThemedProgressIndicator(color: textColor),
                    ),
                  ),
                  Icon(
                    Icons.chevron_right,
                    color: context.colorScheme.surfaceContainer,
                  )
                ],
              ),
      ),
    );
  }
}
