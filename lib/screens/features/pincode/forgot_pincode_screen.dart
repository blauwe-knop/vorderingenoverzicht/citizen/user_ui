// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/clean_up/remove_all_data_and_user_settings_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';
import 'package:user_ui/screens/features/pincode/themed_alert_dialog.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ForgotPincodeScreen extends ConsumerWidget {
  static const routeName = 'ForgotPincodeScreen';
  static const String reactivateButtonKey = 'reactivateButtonKey';
  const ForgotPincodeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return BasicScaffold(
      onBackbuttonPress: () {
        if (context.canPop()) {
          context.pop();
        } else {
          context.goNamed(PincodeScreen.routeName);
        }
      },
      content: Align(
        alignment: Alignment.topCenter,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                const _Body(),
                const _Footer(reactivateButtonKey),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();
  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      alignment: Alignment.topCenter,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Pincode vergeten",
              style: context.textTheme.displayLarge,
            ),
          ),
          addVerticalSpace(Spacing.xlarge.value),
          Text(
            "Het is helaas niet mogelijk om uw pincode te wijzigen als u deze vergeten bent.",
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: context.colorScheme.onSurface),
          ),
          addVerticalSpace(Spacing.large.value),
          Text(
            "U moet de app opnieuw activeren en een nieuwe pincode aanmaken.",
            style: Theme.of(context)
                .textTheme
                .titleLarge
                ?.copyWith(color: context.colorScheme.onSurface),
          ),
          addVerticalSpace(Spacing.xlarge.value),
        ],
      ),
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer(this.reactivateButtonKey);
  final String reactivateButtonKey;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: Column(
        children: [
          ThemedButton(
            key: Key(reactivateButtonKey),
            buttonText: "Activeer opnieuw",
            style: ThemedButtonStyle.primary,
            onPress: () async {
              await showDialog(
                context: context,
                builder: (BuildContext context) {
                  return ThemedAlertDialog(
                    title: const Text('Activeer opnieuw'),
                    content: const Text(
                      'Weet u zeker dat u de app opnieuw wilt activeren?',
                    ),
                    cancelButtonText: 'Annuleren',
                    confirmButtonText: 'Bevestigen',
                    onCancel: () {
                      context.pop();
                    },
                    onConfirm: () {
                      ref
                          .read(pincodeScreenStateNotifierProvider.notifier)
                          .setState(PincodeScreenState.choose);
                      context.goNamed(
                          RemoveAllDataAndUserSettingsScreen.routeName);
                    },
                  );
                },
              );
            },
          ),
          addVerticalSpace(Spacing.large.value),
        ],
      ),
    );
  }
}
