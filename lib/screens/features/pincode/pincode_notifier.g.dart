// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pincode_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pincodeNotifierHash() => r'b24f09d806450a018ed277285f35425b153d45c1';

/// See also [PincodeNotifier].
@ProviderFor(PincodeNotifier)
final pincodeNotifierProvider =
    AutoDisposeNotifierProvider<PincodeNotifier, String>.internal(
  PincodeNotifier.new,
  name: r'pincodeNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$pincodeNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$PincodeNotifier = AutoDisposeNotifier<String>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
