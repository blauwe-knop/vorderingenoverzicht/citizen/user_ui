// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pincode_screen_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$pincodeScreenStateNotifierHash() =>
    r'69b9e11839fd481137128328e9a8646615f6bf80';

/// See also [PincodeScreenStateNotifier].
@ProviderFor(PincodeScreenStateNotifier)
final pincodeScreenStateNotifierProvider = AutoDisposeAsyncNotifierProvider<
    PincodeScreenStateNotifier, PincodeScreenState>.internal(
  PincodeScreenStateNotifier.new,
  name: r'pincodeScreenStateNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$pincodeScreenStateNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$PincodeScreenStateNotifier
    = AutoDisposeAsyncNotifier<PincodeScreenState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
