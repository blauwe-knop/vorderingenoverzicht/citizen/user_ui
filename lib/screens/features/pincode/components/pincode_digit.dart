import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

// ignore: must_be_immutable
class PincodeDigit extends StatelessWidget {
  final bool dense;
  final bool filled;
  final bool selected;

  late double width;
  late double height;
  late double textHeight;
  late double fontSize;
  late double fontHeight;

  PincodeDigit({
    super.key,
    required this.dense,
    required this.filled,
    required this.selected,
  }) {
    width = dense ? 40 : 50;
    height = dense ? 50 : 60;
    fontSize = dense ? 62 : 72;
    fontHeight = dense ? 74 : 86;
    textHeight = dense ? fontHeight / fontSize : fontHeight / fontSize;
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedContainer(
      decoration: BoxDecoration(
        border: Border.all(color: context.colorScheme.surfaceContainer),
        borderRadius: BorderRadius.circular(RadiusSize.small.value),
        color: selected || filled
            ? context.colorScheme.surface
            : context.colorScheme.surfaceContainerLowest,
        boxShadow: selected
            ? [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 1,
                  offset: const Offset(0, 1),
                ),
              ]
            : null,
      ),
      width: width,
      height: height,
      duration: Durations.medium1,
      transform: Matrix4.translationValues(
          0.0, selected ? -Spacing.small.value : 0.0, 0.0),
      child: Center(
        child: Text(
          filled ? "*" : "",
          style: context.textTheme.headlineMedium?.copyWith(
            color: context.colorScheme.tertiary,
            fontWeight: FontWeight.w400,
            fontSize: fontSize,
            height: textHeight,
          ),
        ),
      ),
    );
  }
}
