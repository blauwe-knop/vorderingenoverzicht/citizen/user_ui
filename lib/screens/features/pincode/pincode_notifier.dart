// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/pincode_error.dart';

part 'pincode_notifier.g.dart';

@riverpod
class PincodeNotifier extends _$PincodeNotifier {
  @override
  String build() {
    return "";
  }

  void addDigit(String digit) {
    if (state.length < 5) {
      state = (state + digit);
    }
  }

  void removeDigit() {
    if (state.isEmpty) {
      return;
    }
    state = state.substring(0, state.length - 1);
  }

  void clear() {
    state = "";
  }

  Future<List<PincodeError>> validate() async {
    final String pincode = state;
    final List<PincodeError?> errors = [];

    errors.add(_consistsOnlyOfDigits(pincode));

    if (errors.first == null) {
      errors.add(
        _hasThreeDifferentDigits(pincode),
      );
    }

    // Filter out null values
    final List<PincodeError> filteredErrors =
        errors.whereType<PincodeError>().toList();

    return filteredErrors;
  }

  Future<List<PincodeError>> compare(String firstPincode) async {
    final String secondPincode = state;

    final List<PincodeError?> errors = [
      _pincodesAreTheSame(firstPincode, secondPincode)
    ];

    // Filter out null values
    final List<PincodeError> filteredErrors =
        errors.whereType<PincodeError>().toList();

    return filteredErrors;
  }

  PincodeError? _pincodesAreTheSame(String firstPincode, String secondPincode) {
    return firstPincode == secondPincode
        ? null
        : const PincodeError(
            title: 'Dit is niet dezelfde pincode',
            message: "Probeer het nog een keer");
  }

  PincodeError? _hasThreeDifferentDigits(String input) {
    Set<int> uniqueDigits = {};
    for (String digit in input.split('')) {
      uniqueDigits.add(int.parse(digit));
    }

    return uniqueDigits.length >= 3
        ? null
        : const PincodeError(
            title: 'Deze pincode is te makkelijk te raden',
            message: "Kies een andere pincode");
  }

  PincodeError? _consistsOnlyOfDigits(String input) {
    return double.tryParse(input) != null
        ? null
        : const PincodeError(
            title: 'Een pincode mag alleen cijfers bevatten',
            message: "Kies een andere pincode");
  }
}
