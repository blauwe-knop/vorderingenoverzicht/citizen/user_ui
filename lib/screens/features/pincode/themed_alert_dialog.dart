// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ThemedAlertDialog extends StatelessWidget {
  final Widget title;
  final Widget content;
  final String cancelButtonText;
  final String confirmButtonText;
  final void Function()? onCancel;
  final void Function()? onConfirm;

  const ThemedAlertDialog({
    super.key,
    required this.title,
    required this.content,
    required this.cancelButtonText,
    required this.confirmButtonText,
    required this.onCancel,
    required this.onConfirm,
  });

  @override
  Widget build(BuildContext context) {
    final materialAlertDialog = AlertDialog(
      title: title,
      content: content,
      actions: [
        TextButton(
          onPressed: onCancel,
          child: Text(cancelButtonText),
        ),
        TextButton(
          onPressed: onConfirm,
          style: ButtonStyle(
            foregroundColor: WidgetStateProperty.all(Colors.red),
          ),
          child: Text(confirmButtonText),
        ),
      ],
    );
    final cupertinoDialog = CupertinoAlertDialog(
      title: title,
      content: content,
      actions: [
        TextButton(
          onPressed: onCancel,
          child: Text(cancelButtonText),
        ),
        TextButton(
          onPressed: onConfirm,
          style: ButtonStyle(
            foregroundColor: WidgetStateProperty.all(Colors.red),
          ),
          child: Text(confirmButtonText),
        ),
      ],
    );
    return Theme.of(context).platform == TargetPlatform.iOS
        ? cupertinoDialog
        : materialAlertDialog;
  }
}
