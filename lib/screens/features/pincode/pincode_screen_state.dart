import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/utils/shared_preferences_provider.dart';

part 'pincode_screen_state.g.dart';

enum PincodeScreenState {
  choose,
  confirm,
  unlocked,
  locked;
}

@riverpod
class PincodeScreenStateNotifier extends _$PincodeScreenStateNotifier {
  String get _pincodeStateKey => 'pincodeScreenStateKey';

  @override
  Future<PincodeScreenState> build() async {
    final sharedPreferences = ref.watch(sharedPreferencesProvider);

    final pincodeScreenStateName =
        sharedPreferences.getString(_pincodeStateKey);

    final pincodeScreenState = PincodeScreenState.values.firstWhere(
      (s) => s.name.toLowerCase() == pincodeScreenStateName?.toLowerCase(),
      orElse: () => PincodeScreenState.choose,
    );

    return pincodeScreenState;
  }

  Future<void> setState(PincodeScreenState newState) async {
    state = const AsyncLoading();
    final sharedPreferences = ref.read(sharedPreferencesProvider);
    await sharedPreferences.setString(_pincodeStateKey, newState.name);
    state = AsyncValue.data(newState);
  }

  Future<void> clear() async {
    state = const AsyncLoading();
    final sharedPreferences = ref.read(sharedPreferencesProvider);
    await sharedPreferences.setString(
        _pincodeStateKey, PincodeScreenState.choose.name);
    state = const AsyncValue.data(PincodeScreenState.choose);
  }
}
