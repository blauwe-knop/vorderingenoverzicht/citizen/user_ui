// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class StartupScreen extends ConsumerWidget {
  const StartupScreen({super.key});

  static const routeName = 'StartupScreen';

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    const double imageSize = 128.0;

    final loadingSaffold = BasicScaffold(
      content: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(RadiusSize.medium.value),
                child: Image.asset(
                  'assets/icons/app-icon.png',
                  height: imageSize,
                  width: imageSize,
                  semanticLabel: 'vo rijk image',
                ),
              ),
            ],
          ),
          addVerticalSpace(Spacing.xxlarge.value),
          const Text('Nog heel even geduld,\n de app wordt ingesteld'),
          addVerticalSpace(Spacing.xxlarge.value),
          SizedBox.square(
            dimension: IconSize.xlarge.value,
            child: const CircularProgressIndicator(),
          ),
        ],
      ),
    );

    return ref.watch(appRootKeypairProvider).when(
          data: (available) {
            if (!available) {
              ref
                  .read(appRootKeypairProvider.notifier)
                  .generateAndStoreAppKeyPair();
            } else {
              if (!ref.read(userSettingsRepositoryProvider).hasSeenOnboarding) {
                context.goNamed(PropositionScreen.routeName);
              } else if (!ref
                  .read(userSettingsRepositoryProvider)
                  .hasCompletedRegistration) {
                context.goNamed(ActivationInfoScreen.routeName);
              } else {
                context.goNamed(HomeScreen.routeName);
              }
            }

            return loadingSaffold;
          },
          error: (err, stack) {
            context.goNamed(
              ThemedErrorScreen.routeName,
              pathParameters: {
                'isCritical': 'true',
                'redirectRoute': StartupScreen.routeName,
              },
            );
            return const SizedBox.shrink();
          },
          loading: () => loadingSaffold,
        );
  }
}
