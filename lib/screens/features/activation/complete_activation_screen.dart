// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/usecases/registration/complete_activation.dart';

class CompleteActivationScreen extends ConsumerWidget {
  static const routeName = 'CompleteActivationScreen';

  const CompleteActivationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(completeActivationProvider).when(
          data: (_) {
            context.goNamed(HomeScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) {
            context.goNamed(
              ThemedErrorScreen.routeName,
              pathParameters: {
                'isCritical': 'false',
                'redirectRoute': ActivationInfoScreen.routeName,
              },
            );
            return const SizedBox.shrink();
          },
          loading: () => const ThemedLoadingIndicator(
            showDrawer: true,
            text: "Bezig met activeren van de app...",
          ),
        );
  }
}
