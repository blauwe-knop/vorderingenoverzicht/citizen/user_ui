// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/usecases/registration/start_registration_and_get_redirect_uri.dart';

class StartActivationScreen extends ConsumerStatefulWidget {
  static const routeName = 'StartActivationScreen';

  const StartActivationScreen({super.key});

  @override
  ConsumerState<StartActivationScreen> createState() =>
      _StartActivationScreenState();
}

class _StartActivationScreenState extends ConsumerState<StartActivationScreen> {
  bool urlLaunched = false;

  @override
  Widget build(BuildContext context) {
    final loadingIndicatorScreen = ThemedLoadingIndicator(
      onBackbuttonPress: () => context.goNamed(ActivationInfoScreen.routeName),
      text: "App activeren voorbereiden...",
      addDeepLink: true,
    );

    void goThemedErrorScreen() =>
        context.goNamed(ThemedErrorScreen.routeName, pathParameters: {
          'isCritical': 'false',
          'redirectRoute': ActivationInfoScreen.routeName,
        });

    return ref.watch(startRegistrationAndGetRedirectUriProvider).when(
      data: (redirectUri) {
        if (!urlLaunched) {
          urlLaunched = true;
          launchURL(redirectUri).onError((error, stackTrace) {
            final loggingHelper = ref.read(loggingProvider);
            loggingHelper.addLog(DeviceEvent.uu_85, error.toString());
            goThemedErrorScreen();
          });
        }

        return loadingIndicatorScreen;
      },
      error: (error, stack) {
        goThemedErrorScreen();
        return const SizedBox.shrink();
      },
      loading: () {
        return loadingIndicatorScreen;
      },
    );
  }

  Future<void> launchURL(Uri redirectUri) async {
    if (await canLaunchUrl(redirectUri)) {
      await launchUrl(
        redirectUri,
        mode: LaunchMode.externalApplication,
        webOnlyWindowName: '_self',
      );
    } else {
      throw Exception('Failed to launch url: $redirectUri');
    }
  }
}
