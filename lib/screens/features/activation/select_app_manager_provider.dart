import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/usecases/app_manager_selection_usecase.dart';

part 'select_app_manager_provider.g.dart';

@riverpod
Future<void> selectAppManager(Ref ref) async {
  try {
    final schemeAppManagerRepository =
        ref.watch(schemeAppManagerRepositoryProvider);

    if (schemeAppManagerRepository
        .where((appManager) => appManager.available)
        .isEmpty) {
      throw Exception('No app managers available');
    }

    final appManager = schemeAppManagerRepository
        .where((appManager) => appManager.available)
        .first;

    final appManagerSelectionUsecase =
        ref.read(appManagerSelectionUsecaseProvider);

    await appManagerSelectionUsecase.setAppManagerSelection(appManager.oin);
  } catch (error) {
    final loggingHelper = ref.read(loggingProvider);
    loggingHelper.addLog(DeviceEvent.uu_84, error.toString());
  }
}
