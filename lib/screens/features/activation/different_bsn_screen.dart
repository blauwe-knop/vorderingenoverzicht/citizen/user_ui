// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/activation/undo_latest_activation_screen.dart';
import 'package:user_ui/screens/features/clean_up/remove_old_user_data_and_use_latest_registration_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class DifferentBsnScreen extends ConsumerWidget {
  static const routeName = 'DifferentBsnScreen';

  const DifferentBsnScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return BasicScaffold(
      onBackbuttonPress: () => context.pop(),
      content: Align(
        alignment: Alignment.topCenter,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: ResponsiveCenter(
              xs: 12,
              md: 10,
              lg: 8,
              xl: 6,
              alignment: Alignment.topCenter,
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  addVerticalSpace(Spacing.large.value),
                  const _Header(),
                  const _Body(),
                  const _Footer(),
                  addVerticalSpace(Spacing.large.value),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class _Header extends StatelessWidget {
  const _Header();

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topLeft,
      child: Text(
        "Doorgaan met activeren?",
        style: context.textTheme.displayLarge,
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        addVerticalSpace(Spacing.xlarge.value),
        Text(
          "Je wilt de app activeren voor [Nieuwe Naam], maar in de app zijn nog gegevens van [Oude Naam] aanwezig.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: context.colorScheme.onSurface),
        ),
        addVerticalSpace(Spacing.large.value),
        Text(
          "Als u doorgaat met activeren, worden de gegevens van [Oude Naam] uit de app verwijderd, zodat u gegevens op kunt halen voor [Nieuwe Naam].",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: context.colorScheme.onSurface),
        ),
        addVerticalSpace(Spacing.large.value),
        Text(
          "Als u de activatie stopt blijven de gegevens van [Oude Naam] bewaard, maar kunnen er geen nieuwe gegevens worden opgehaald.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: context.colorScheme.onSurface),
        ),
        addVerticalSpace(Spacing.xlarge.value),
      ],
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: Column(
        children: [
          ThemedButton(
            buttonText: "Doorgaan met activeren",
            style: ThemedButtonStyle.primary,
            onPress: () => context.goNamed(
                RemoveOldUserDataAndUseLatestRegistrationScreen.routeName),
          ),
          addVerticalSpace(Spacing.large.value),
          ThemedButton(
            buttonText: 'Activeren stoppen',
            style: ThemedButtonStyle.tertiary,
            onPress: () =>
                context.goNamed(UndoLatestActivationScreen.routeName),
          ),
        ],
      ),
    );
  }
}
