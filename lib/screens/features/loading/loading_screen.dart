// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/financial_claims_information/copy_financial_claims_information_inbox_to_storage.dart';
import 'package:user_ui/usecases/request_financial_claims_information/create_financial_claims_information_requests_for_selected_organizations.dart';

class LoadingScreen extends ConsumerStatefulWidget {
  const LoadingScreen({super.key});

  static const routeName = 'LoadingScreen';

  @override
  LoadingScreenViewState createState() => LoadingScreenViewState();
}

class LoadingScreenViewState extends ConsumerState<LoadingScreen> {
  int _phase = 0;

  @override
  void initState() {
    super.initState();
    ref
        .read(appUsecaseProvider)
        .setHasCompletedOrganizationSelection(true)
        .then((_) => ref.read(
            createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider
                .future))
        .then((_) {
      Future.delayed(const Duration(seconds: 3)).then((_) {
        setState(() {
          _phase = 1;
        });
      }).then((value) {
        Future.delayed(const Duration(seconds: 3)).then((_) {
          setState(() {
            _phase = 2;
          });
        });
      });
    });
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    var numberOfSelectedOrganizations =
        ref.watch(numberOfSelectedOrganizationsProvider);

    var numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
        numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);
    String asset;
    String headerText;
    String? bodyText;
    bool showButton = false;

    if (_phase == 2) {
      showButton = numberOfCompletedFinancialClaimsInformationRequests > 0;

      if (numberOfCompletedFinancialClaimsInformationRequests ==
          numberOfSelectedOrganizations) {
        asset = "assets/lottie/108373-job-list.json";

        headerText = numberOfSelectedOrganizations == 1
            ? "De organisatie heeft gereageerd"
            : "Alle organisaties hebben gereageerd";

        bodyText = null;
      } else {
        asset = "assets/lottie/42766-paper-planes.json";

        headerText = "Uw gegevens worden opgehaald...";

        bodyText = numberOfSelectedOrganizations == 1
            ? "De organisatie heeft nog niet gereageerd"
            : "$numberOfCompletedFinancialClaimsInformationRequests van de $numberOfSelectedOrganizations organisaties hebben gereageerd";
      }
    } else if (_phase == 1) {
      asset = "assets/lottie/42766-paper-planes.json";

      headerText = "Uw gegevens worden opgehaald...";

      bodyText = numberOfSelectedOrganizations == 1
          ? "De organisatie heeft nog niet gereageerd"
          : "$numberOfCompletedFinancialClaimsInformationRequests van de $numberOfSelectedOrganizations organisaties hebben gereageerd";

      showButton = numberOfCompletedFinancialClaimsInformationRequests > 0;
    } else {
      asset = "assets/lottie/9688-origami-email-to-paper-plane-poof.json";

      headerText = numberOfSelectedOrganizations == 1
          ? "Uw verzoek wordt verstuurd..."
          : "Uw verzoeken worden verstuurd...";

      bodyText = numberOfSelectedOrganizations == 1
          ? "De organisatie wordt bevraagd"
          : "$numberOfSelectedOrganizations organisaties worden bevraagd";
    }

    const lottieHeight = 200.0;

    return BasicScaffold(
      content: Align(
        alignment: Alignment.center,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                _Body(
                  asset: asset,
                  lottieHeight: lottieHeight,
                  headerText: headerText,
                  bodyText: bodyText,
                ),
                addVerticalSpace(Spacing.xlarge.value),
                if (showButton) const _Footer(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.asset,
    required this.lottieHeight,
    required this.headerText,
    required this.bodyText,
  });

  final String asset;
  final double lottieHeight;
  final String headerText;
  final String? bodyText;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: SizedBox(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            AspectRatio(
              aspectRatio: 4 / 3,
              child: Lottie.asset(
                asset,
                height: lottieHeight,
              ),
            ),
            addVerticalSpace(Spacing.large.value),
            Text(
              headerText,
              style: context.textTheme.displayMedium,
              textAlign: TextAlign.center,
            ),
            addVerticalSpace(Spacing.xlarge.value),
            if (bodyText != null)
              Text(
                bodyText!,
                style: context.textTheme.bodyMedium,
                textAlign: TextAlign.center,
              ),
          ],
        ),
      ),
    );
  }
}

class _Footer extends ConsumerWidget {
  const _Footer();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: ThemedButton(
        buttonText: "Bekijk het overzicht",
        style: ThemedButtonStyle.primary,
        onPress: () async {
          await ref.read(
              copyFinancialClaimsInformationInboxToStorageProvider.future);
          if (context.mounted) {
            context.goNamed(FinancialClaimsOverviewScreen.routeName);
          }
        },
      ),
    );
  }
}
