// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/registration/has_usable_registration_notifier.dart';
import 'package:user_ui/repositories/registration/latest_completed_registration_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/reactivation_banner.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/clean_up/remove_all_data_and_user_settings_screen.dart';
import 'package:user_ui/screens/features/home/components/start_organization_selection_theme_card.dart';
import 'package:user_ui/screens/features/home/components/summary_theme_card.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class HomeScreen extends ConsumerWidget {
  static const String routeName = 'HomeScreen';

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    //TODO: Change all ref watches to 1 screen notifier
    final userSettings = ref.watch(userSettingsRepositoryProvider);

    if (userSettings.pincode.isEmpty) {
      context.goNamed(PincodeScreen.routeName);
    } else if (!userSettings.hasCompletedRegistration) {
      context.goNamed(ActivationInfoScreen.routeName);
    }

    int numberOfSelectedOrganizations = 0;
    int numberOfCompletedFinancialClaimsInformationRequests = 0;
    int numberOfFinancialClaimsInformationStorages = 0;

    if (userSettings.hasCompletedOrganizationSelection) {
      ref.watch(organizationSelectionRepositoryProvider);

      numberOfFinancialClaimsInformationStorages =
          ref.watch(financialClaimsInformationStorageRepositoryProvider).length;

      numberOfSelectedOrganizations =
          ref.watch(numberOfSelectedOrganizationsProvider);

      numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
          numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);
    }

    return BasicScaffold(
      showDrawer: true,
      title: ref.watch(latestCompletedRegistrationProvider).when(
            data: (latestCompletedRegistration) =>
                "Hallo ${latestCompletedRegistration.givenName}",
            error: (err, stack) {
              return "Error bij ophalen van naam";
            },
            loading: () => "Hallo",
          ),
      content: ref.watch(hasUsableRegistrationProvider).when(
            data: (hasUsableRegistration) {
              if (!hasUsableRegistration && kIsWeb) {
                context.goNamed(RemoveAllDataAndUserSettingsScreen.routeName);
              }
              if (!hasUsableRegistration && !kIsWeb) {
                return Column(
                  mainAxisAlignment: responsiveValue(context,
                      xs: MainAxisAlignment.start, md: MainAxisAlignment.start),
                  children: [
                    const ReactivationBanner(),
                    Expanded(
                      child: _Body(
                        hasCompletedOrganizationSelection:
                            userSettings.hasCompletedOrganizationSelection,
                        numberOfSelectedOrganizations:
                            numberOfSelectedOrganizations,
                        numberOfCompletedFinancialClaimsInformationRequests:
                            numberOfCompletedFinancialClaimsInformationRequests,
                        numberOfFinancialClaimsInformationStorages:
                            numberOfFinancialClaimsInformationStorages,
                      ),
                    ),
                  ],
                );
              }
              return _Body(
                hasCompletedOrganizationSelection:
                    userSettings.hasCompletedOrganizationSelection,
                numberOfSelectedOrganizations: numberOfSelectedOrganizations,
                numberOfCompletedFinancialClaimsInformationRequests:
                    numberOfCompletedFinancialClaimsInformationRequests,
                numberOfFinancialClaimsInformationStorages:
                    numberOfFinancialClaimsInformationStorages,
              );
            },
            error: (err, stack) =>
                const Text("Error bij ophalen van registratie"),
            loading: () => _Body(
              hasCompletedOrganizationSelection:
                  userSettings.hasCompletedOrganizationSelection,
              numberOfSelectedOrganizations: numberOfSelectedOrganizations,
              numberOfCompletedFinancialClaimsInformationRequests:
                  numberOfCompletedFinancialClaimsInformationRequests,
              numberOfFinancialClaimsInformationStorages:
                  numberOfFinancialClaimsInformationStorages,
            ),
          ),
    );
  }
}

class _Body extends StatelessWidget {
  static const double negativePadding = -32;

  const _Body({
    required this.hasCompletedOrganizationSelection,
    required this.numberOfSelectedOrganizations,
    required this.numberOfCompletedFinancialClaimsInformationRequests,
    required this.numberOfFinancialClaimsInformationStorages,
  });

  final bool hasCompletedOrganizationSelection;
  final int numberOfSelectedOrganizations;
  final int numberOfCompletedFinancialClaimsInformationRequests;
  final int numberOfFinancialClaimsInformationStorages;

  @override
  Widget build(BuildContext context) {
    return Container(
      transform: Matrix4.translationValues(
          0.0, -negativePadding, 0.0), // Needed to dissolve negative padding
      child: Column(
        mainAxisAlignment: responsiveValue(context,
            xs: MainAxisAlignment.end, md: MainAxisAlignment.center),
        children: [
          Flexible(
            child: SvgPicture.asset(
              "assets/images/home.svg",
              semanticsLabel: 'home image',
            ),
          ),
          ResponsiveCenter(
            xs: 12,
            md: 8,
            lg: 6,
            xl: 4,
            child: Container(
              transform: Matrix4.translationValues(0.0, negativePadding, 0.0),
              child: hasCompletedOrganizationSelection
                  ? SummaryThemeCard(
                      numberOfSelectedOrganizations:
                          numberOfSelectedOrganizations,
                      numberOfCompletedFinancialClaimsInformationRequests:
                          numberOfCompletedFinancialClaimsInformationRequests,
                      numberOfFinancialClaimsInformationStorages:
                          numberOfFinancialClaimsInformationStorages,
                    )
                  : const StartOrganizationSelectionThemeCard(),
            ),
          ),
          addVerticalSpace(
            responsiveValue(
              context,
              xs: Spacing.large.value,
              sm: Spacing.small.value,
            ),
          ),
        ],
      ),
    );
  }
}
