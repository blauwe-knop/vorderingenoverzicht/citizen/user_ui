import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class PropositionTextRow extends StatelessWidget {
  const PropositionTextRow({
    super.key,
    this.text,
  });

  final InlineSpan? text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset(
          width: IconSize.xlarge.value,
          height: IconSize.xlarge.value,
          "assets/icons/checkmark.svg",
          semanticsLabel: 'Checkmark Icon',
        ),
        addHorizontalSpace(Spacing.large.value),
        Flexible(
          child: RichText(
            softWrap: true,
            text: text ?? const TextSpan(text: ''),
          ),
        ),
      ],
    );
  }
}
