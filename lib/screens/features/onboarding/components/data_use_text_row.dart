import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class DataUseTextRow extends StatelessWidget {
  const DataUseTextRow({
    super.key,
    this.text,
  });

  final InlineSpan? text;

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.only(top: Spacing.small.value),
          child: SvgPicture.asset(
            width: IconSize.xlarge.value,
            height: IconSize.xlarge.value,
            "assets/icons/lock.svg",
            semanticsLabel: 'Lock Icon',
          ),
        ),
        addHorizontalSpace(Spacing.large.value),
        Flexible(
          child: RichText(
            softWrap: true,
            text: text ?? const TextSpan(text: ''),
          ),
        ),
      ],
    );
  }
}
