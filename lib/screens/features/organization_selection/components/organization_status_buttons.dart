// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_state.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class OrganizationStatusButtons extends ConsumerWidget {
  const OrganizationStatusButtons({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Column(
      children: [
        addVerticalSpace(Spacing.large.value),
        ThemedButton(
          buttonText: 'Wijzig selectie',
          style: ThemedButtonStyle.quarternary,
          onPress: () => ref
              .read(organizationSelectionScreenStateNotifierProvider.notifier)
              .setState(OrganizationSelectionScreenState.selection),
        ),
        addVerticalSpace(Spacing.medium.value),
      ],
    );
  }
}
