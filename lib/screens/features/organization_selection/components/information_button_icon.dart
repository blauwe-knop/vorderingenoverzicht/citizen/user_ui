import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class InformationButtonIcon extends StatelessWidget {
  const InformationButtonIcon({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: IconSize.medium.value,
      child: Center(
        child: CircleAvatar(
          backgroundColor: context.colorScheme.tertiary,
          child: Text(
            'i',
            style: context.textTheme.labelLarge?.copyWith(
              fontWeight: FontWeight.w600,
              fontSize: 16,
              height: 19 / 16,
              color: context.colorScheme.onPrimary,
            ),
          ),
        ),
      ),
    );
  }
}
