// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_mobile_alert_dialog.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_web_alert_dialog.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';
import 'package:user_ui/usecases/request_financial_claims_information/create_financial_claims_information_requests_for_selected_organizations.dart';

class OrganizationSelectionButtons extends ConsumerStatefulWidget {
  const OrganizationSelectionButtons({super.key});

  @override
  ConsumerState<OrganizationSelectionButtons> createState() =>
      _PickOrganizationButtonsState();
}

class _PickOrganizationButtonsState
    extends ConsumerState<OrganizationSelectionButtons> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final hasSelectedOrganizations = ref.watch(
      organizationSelectionScreenNotifierProvider.select(
        (value) =>
            value.asData?.value.hasSelectedOrganizations ??
            true, // When null (in loading state) return true to prevent flickering buttons.
      ),
    );

    final hasSelectedAllOrganizations = ref.watch(
      organizationSelectionScreenNotifierProvider.select(
        (value) => value.asData?.value.hasSelectedAllOrganizations ?? false,
      ),
    );

    return Column(
      children: [
        addVerticalSpace(Spacing.large.value),
        ThemedButton(
          style: ThemedButtonStyle.tertiary,
          onPress: () => hasSelectedOrganizations
              ? deselectAllOrganizations()
              : selectAllOrganizations(),
          buttonText:
              '${hasSelectedOrganizations ? 'Deselecteer' : 'Selecteer'} alle organisaties',
          isLoading: _isLoading,
        ),
        addVerticalSpace(Spacing.large.value),
        ThemedButton(
          buttonText: ref
                  .read(userSettingsRepositoryProvider)
                  .hasCompletedOrganizationSelection
              ? 'Bewaar selectie'
              : 'Volgende',
          style: ThemedButtonStyle.primary,
          onPress: hasSelectedAllOrganizations
              ? completeOrganizationSelection
              : hasSelectedOrganizations
                  ? showConfirmation
                  : null,
          isLoading: _isLoading,
        ),
        addVerticalSpace(Spacing.large.value),
      ],
    );
  }

  void setLoading(bool value) {
    setState(() {
      if (mounted) {
        _isLoading = value;
      }
    });
  }

  Future<void> completeOrganizationSelection() async {
    setLoading(true);
    if (ref
        .read(userSettingsRepositoryProvider)
        .hasCompletedOrganizationSelection) {
      await ref
          .read(appUsecaseProvider)
          .setHasCompletedOrganizationSelection(true);

      await ref.read(
          createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider
              .future);

      if (mounted) {
        context.pop();
      }
    } else {
      if (mounted) {
        context.goNamed(LoadingScreen.routeName);
      }
    }
  }

  void showConfirmation() async {
    final result = await showDialog<bool>(
      context: context,
      builder: (context) {
        return kIsWeb
            ? const OrganizationWebAlertDialog()
            : const OrganizationMobileAlertDialog();
      },
    );

    if (result != null && result && mounted) {
      context.pop();
    }
  }

  void deselectAllOrganizations() async {
    setLoading(true);
    await ref
        .read(organizationSelectionUsecaseProvider)
        .setAutoSelectAllOrganizations(false)
        .then((_) => ref
            .read(organizationSelectionUsecaseProvider)
            .deselectAllOrganizations());
    setLoading(false);
  }

  void selectAllOrganizations() async {
    setLoading(true);
    await ref
        .read(organizationSelectionUsecaseProvider)
        .setAutoSelectAllOrganizations(false)
        .then((_) => ref
            .read(organizationSelectionUsecaseProvider)
            .selectAllOrganizations());
    setLoading(false);
  }
}
