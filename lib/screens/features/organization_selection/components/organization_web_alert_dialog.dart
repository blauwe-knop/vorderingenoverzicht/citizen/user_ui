import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/common/themed_close_button.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/request_financial_claims_information/create_financial_claims_information_requests_for_selected_organizations.dart';

class OrganizationWebAlertDialog extends ConsumerStatefulWidget {
  const OrganizationWebAlertDialog({super.key});

  @override
  ConsumerState<OrganizationWebAlertDialog> createState() =>
      _OrganizationWebAlertDialogState();
}

class _OrganizationWebAlertDialogState
    extends ConsumerState<OrganizationWebAlertDialog> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    final customPadding = EdgeInsets.only(
        left: Spacing.large.value,
        right: Spacing.large.value,
        top: Spacing.large.value);

    return AlertDialog(
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text('Weet u het zeker?', style: context.textTheme.displayMedium),
          ThemedCloseButton(
            onPressed: () => Navigator.pop(context),
            color: context.colorScheme.primary,
          ),
        ],
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(RadiusSize.medium.value)),
      titlePadding: customPadding,
      contentPadding: EdgeInsets.only(
          left: Spacing.large.value,
          right: Spacing.large.value,
          top: Spacing.medium.value),
      actionsPadding: customPadding,
      content: Text("Mogelijk ziet u hierdoor een rekening over het hoofd.",
          style: Theme.of(context)
              .textTheme
              .titleLarge
              ?.copyWith(color: context.colorScheme.onSurface)),
      actions: [
        Column(
          children: [
            ThemedButton(
              style: ThemedButtonStyle.primary,
              onPress: () => completeOrganizationSelection(),
              buttonText: 'Ja, ga verder',
              isLoading: _isLoading,
            ),
            addVerticalSpace(Spacing.large.value),
            ThemedButton(
              style: ThemedButtonStyle.quarternary,
              onPress: () => context.pop(),
              buttonText: 'Nee, terug',
            ),
            addVerticalSpace(Spacing.xlarge.value),
          ],
        ),
      ],
    );
  }

  void setLoading(bool value) {
    setState(() {
      if (mounted) {
        _isLoading = value;
      }
    });
  }

  Future<void> completeOrganizationSelection() async {
    setLoading(true);

    if (ref
        .read(userSettingsRepositoryProvider)
        .hasCompletedOrganizationSelection) {
      await ref
          .read(appUsecaseProvider)
          .setHasCompletedOrganizationSelection(true);

      await ref.read(
          createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider
              .future);

      if (mounted) {
        context.pop(true);
      }
    } else {
      if (mounted) {
        context.goNamed(LoadingScreen.routeName);
      }
    }
  }
}
