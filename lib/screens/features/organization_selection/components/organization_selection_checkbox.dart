import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';

class OrganizationSelectionCheckbox extends ConsumerWidget {
  const OrganizationSelectionCheckbox(
      {super.key, required this.organizationSelection});
  final OrganizationSelectionViewModel organizationSelection;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return SizedBox.square(
      dimension: IconSize.large.value,
      child: Checkbox(
        checkColor: context.colorScheme.onPrimary,
        fillColor: WidgetStateColor.resolveWith((states) {
          if (states.contains(WidgetState.selected)) {
            return context.colorScheme.primary;
          }
          return context.colorScheme.surface;
        }),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(RadiusSize.small.value),
          ),
        ),
        side: WidgetStateBorderSide.resolveWith(
          (states) => BorderSide(
            width: 1.0,
            color: context.colorScheme.outline,
          ),
        ),
        value: organizationSelection.selected,
        onChanged: organizationSelection.available
            ? (selected) {
                if (selected == null) {
                  return;
                }
                final organizationSelectionUsecase =
                    ref.read(organizationSelectionUsecaseProvider);
                organizationSelectionUsecase.setOrganizationSelection(
                    organizationSelection.oin, selected);
              }
            : null,
      ),
    );
  }
}
