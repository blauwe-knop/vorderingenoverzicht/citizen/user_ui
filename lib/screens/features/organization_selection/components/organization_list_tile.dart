import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_information_button.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_selection_checkbox.dart';
import 'package:user_ui/screens/features/organization_selection/components/organization_status_badge.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_notifier.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen_state.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/usecases/organization_selection_usecase.dart';

class OrganizationListTile extends ConsumerWidget {
  const OrganizationListTile({
    super.key,
    required this.organizationSelection,
  });

  final OrganizationSelectionViewModel organizationSelection;

  Widget? getLeadingWidget(
      OrganizationSelectionViewModel organizationSelectionViewModel,
      OrganizationSelectionScreenState state) {
    switch (state) {
      case OrganizationSelectionScreenState.overview:
        return null;
      case OrganizationSelectionScreenState.selection:
        return SizedBox.square(
          dimension: minTapSize,
          child: Align(
            alignment: Alignment.topLeft,
            child: OrganizationSelectionCheckbox(
                organizationSelection: organizationSelectionViewModel),
          ),
        );
      case OrganizationSelectionScreenState.status:
        return SizedBox.square(
          dimension: minTapSize,
          child: Align(
            alignment: Alignment.topLeft,
            child: OrganizationStatusBadge(
                organizationSelection: organizationSelectionViewModel),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final screenState =
        ref.watch(organizationSelectionScreenStateNotifierProvider);
    var textType = context.textTheme.bodyLarge;

    return screenState.when(
      skipError: true,
      skipLoadingOnReload: true,
      data: (state) {
        bool organizationNotSelected = !organizationSelection.selected &&
            state == OrganizationSelectionScreenState.status;

        return InkWell(
          onTap: organizationSelection.available &&
                  state == OrganizationSelectionScreenState.selection
              ? () {
                  final organizationSelectionUsecase =
                      ref.read(organizationSelectionUsecaseProvider);
                  organizationSelectionUsecase.setOrganizationSelection(
                      organizationSelection.oin,
                      !organizationSelection.selected);
                }
              : null,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              getLeadingWidget(organizationSelection, state) ??
                  const SizedBox.shrink(),
              Expanded(
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Text(
                    organizationSelection.name,
                    style: textType?.copyWith(
                      fontWeight: FontWeight.w400,
                      color: organizationNotSelected
                          ? textType.color?.withOpacity(0.5)
                          : textType.color,
                    ),
                    maxLines: 2,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
              ),
              OrganizationInformationButton(
                oin: organizationSelection.oin,
              ),
            ],
          ),
        );
      },
      error: (error, stackTrace) => const Text('Failed to load organization'),
      loading: () => const Text('Loading'),
    );
  }
}
