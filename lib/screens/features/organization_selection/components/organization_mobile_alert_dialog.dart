import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/screens/common/themed_progress_indicator.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/usecases/app_usecase.dart';
import 'package:user_ui/usecases/request_financial_claims_information/create_financial_claims_information_requests_for_selected_organizations.dart';

class OrganizationMobileAlertDialog extends ConsumerStatefulWidget {
  const OrganizationMobileAlertDialog({super.key});

  @override
  ConsumerState<OrganizationMobileAlertDialog> createState() =>
      _OrganizationMobileAlertDialogState();
}

class _OrganizationMobileAlertDialogState
    extends ConsumerState<OrganizationMobileAlertDialog> {
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return CupertinoAlertDialog(
      title: const Text('Weet u het zeker?'),
      content:
          const Text("Mogelijk ziet u hierdoor een rekening over het hoofd."),
      actions: [
        TextButton(
          style: TextButton.styleFrom(
            textStyle: context.textTheme.labelLarge,
          ),
          child: !_isLoading
              ? Text(
                  'Ja, ga verder',
                  style: context.textTheme.bodyLarge?.copyWith(
                    fontWeight: FontWeight.w600,
                    color: context.colorScheme.primary,
                  ),
                )
              : ThemedProgressIndicator(color: context.colorScheme.primary),
          onPressed: () async => await completeOrganizationSelection(),
        ),
        TextButton(
          style: TextButton.styleFrom(
            textStyle: context.textTheme.labelLarge,
          ),
          child: Text(
            'Nee, terug',
            style: context.textTheme.bodyLarge?.copyWith(
              color: context.colorScheme.primary,
            ),
          ),
          onPressed: () => context.pop(),
        ),
      ],
    );
  }

  void setLoading(bool value) {
    setState(() {
      if (mounted) {
        _isLoading = value;
      }
    });
  }

  Future<void> completeOrganizationSelection() async {
    setLoading(true);
    if (ref
        .read(userSettingsRepositoryProvider)
        .hasCompletedOrganizationSelection) {
      await ref
          .read(appUsecaseProvider)
          .setHasCompletedOrganizationSelection(true);

      await ref.read(
          createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider
              .future);

      if (mounted) {
        context.pop(true);
      }
    } else {
      if (mounted) {
        context.goNamed(LoadingScreen.routeName);
      }
    }
  }
}
