import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extended_colors.dart';
import 'package:user_ui/theme/theme_extensions.dart';

class StatusNotRespondedIcon extends StatelessWidget {
  const StatusNotRespondedIcon({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox.square(
      dimension: IconSize.medium.value,
      child: Center(
        child: CircleAvatar(
          backgroundColor: ThemeExtendedColors.iconOrange,
          child: Text(
            '...',
            style: context.textTheme.labelLarge?.copyWith(
              fontWeight: FontWeight.w600,
              fontSize: 14,
              height: 1,
              color: context.colorScheme.onPrimary,
            ),
          ),
        ),
      ),
    );
  }
}
