// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'organization_selection_screen_state.g.dart';

enum OrganizationSelectionScreenState {
  overview,
  selection,
  status;
}

@riverpod
class OrganizationSelectionScreenStateNotifier
    extends _$OrganizationSelectionScreenStateNotifier {
  @override
  Future<OrganizationSelectionScreenState> build() async {
    return ref
            .read(userSettingsRepositoryProvider)
            .hasCompletedOrganizationSelection
        ? OrganizationSelectionScreenState.status
        : OrganizationSelectionScreenState.overview;
  }

  void setState(OrganizationSelectionScreenState newState) {
    state = const AsyncLoading();
    state = AsyncValue.data(newState);
  }
}
