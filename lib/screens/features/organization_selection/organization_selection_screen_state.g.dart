// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_selection_screen_state.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$organizationSelectionScreenStateNotifierHash() =>
    r'962b47eee2b38184a2f7b1d1659e2217f0b7c301';

/// See also [OrganizationSelectionScreenStateNotifier].
@ProviderFor(OrganizationSelectionScreenStateNotifier)
final organizationSelectionScreenStateNotifierProvider =
    AutoDisposeAsyncNotifierProvider<OrganizationSelectionScreenStateNotifier,
        OrganizationSelectionScreenState>.internal(
  OrganizationSelectionScreenStateNotifier.new,
  name: r'organizationSelectionScreenStateNotifierProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$organizationSelectionScreenStateNotifierHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$OrganizationSelectionScreenStateNotifier
    = AutoDisposeAsyncNotifier<OrganizationSelectionScreenState>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
