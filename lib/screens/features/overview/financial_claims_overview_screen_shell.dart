// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/registration/has_usable_registration_notifier.dart';
import 'package:user_ui/screens/common/reactivation_banner.dart';
import 'package:user_ui/screens/common/themed_side_menu.dart';
import 'package:user_ui/screens/features/clean_up/remove_all_data_and_user_settings_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen.dart';
import 'package:user_ui/screens/features/overview/components/unopened_inbox_snack_bar.dart';
import 'package:user_ui/screens/providers/number_of_completed_financial_claims_information_requests_state_notifier.dart';
import 'package:user_ui/screens/providers/number_of_selected_organizations_state_notifier.dart';
import 'package:user_ui/usecases/financial_claims_information/copy_financial_claims_information_inbox_to_storage.dart';
import 'package:user_ui/usecases/request_financial_claims_information/create_financial_claims_information_requests_for_selected_organizations.dart';

class FinancialClaimsOverviewScreenShell extends ConsumerWidget {
  const FinancialClaimsOverviewScreenShell({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var numberOfSelectedOrganizations =
        ref.watch(numberOfSelectedOrganizationsProvider);

    var numberOfCompletedFinancialClaimsInformationRequests = ref.watch(
        numberOfCompletedFinancialClaimsInformationRequestsStateNotifierProvider);

    return FinancialClaimsOverviewScreen(
      numberOfSelectedOrganizations: numberOfSelectedOrganizations,
      numberOfCompletedFinancialClaimsInformationRequests:
          numberOfCompletedFinancialClaimsInformationRequests,
      snackBar: const _SnackBarWidget(),
      reactivationBanner: const _ReactivationWidget(),
      organizationSelectionScreen: const OrganizationSelectionScreen(),
      sideMenu: const ThemedSideMenu(),
      onError: (error, stack) {
        context.goNamed(
          ThemedErrorScreen.routeName,
          pathParameters: {
            'isCritical': 'false',
            'redirectRoute': HomeScreen.routeName,
          },
        );
        return const SizedBox.shrink();
      },
      onRefresh: () async {
        await ref.read(
          createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider
              .future,
        );
      },
    );
  }
}

class _ReactivationWidget extends ConsumerWidget {
  const _ReactivationWidget();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(hasUsableRegistrationProvider).when(
      data: (hasUsableRegistration) {
        if (!hasUsableRegistration && kIsWeb) {
          context.goNamed(RemoveAllDataAndUserSettingsScreen.routeName);
        }
        if (!hasUsableRegistration && !kIsWeb) {
          return const ReactivationBanner();
        }
        return SizedBox();
      },
      error: (err, stack) {
        return const Text("Error bij ophalen van registratie");
      },
      loading: () {
        return const SizedBox.shrink();
      },
    );
  }
}

class _SnackBarWidget extends ConsumerWidget {
  const _SnackBarWidget();

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var financialClaimsInformationInboxRepository = ref.watch(
        financialClaimsInformationInboxRepositoryProvider.select((inboxItems) =>
            inboxItems.where((inboxItem) => !inboxItem.copiedToStorage)));

    return Align(
      alignment: Alignment.bottomCenter,
      child: Column(
        children: [
          if (financialClaimsInformationInboxRepository.isNotEmpty)
            UnopenedInboxSnackBar(
              onPressed: () async {
                await ref.read(
                  copyFinancialClaimsInformationInboxToStorageProvider.future,
                );
              },
            ),
        ],
      ),
    );
  }
}
