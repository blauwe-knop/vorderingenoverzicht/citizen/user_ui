// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

class ErrorHandlerWidget extends ConsumerStatefulWidget {
  final Widget child;

  const ErrorHandlerWidget({super.key, required this.child});

  @override
  ConsumerState<ErrorHandlerWidget> createState() => _ErrorHandlerWidgetState();
}

class _ErrorHandlerWidgetState extends ConsumerState<ErrorHandlerWidget> {
  bool showCriticalErrorScreen = false;
  // Error handling logic
  void onFlutterError(FlutterErrorDetails errorDetails) {
    final loggingHelper = ref.read(loggingProvider);
    loggingHelper.addLog(DeviceEvent.uu_82, errorDetails.exceptionAsString());
    FlutterError.dumpErrorToConsole(errorDetails);
  }

  bool onPlatformError(Object object, StackTrace stackTrace) {
    final loggingHelper = ref.read(loggingProvider);
    loggingHelper.addLog(DeviceEvent.uu_83, stackTrace.toString());
    debugPrint(object.toString());
    debugPrint(stackTrace.toString());
    return true;
  }

  @override
  void initState() {
    super.initState();
    FlutterError.onError = onFlutterError;
    PlatformDispatcher.instance.onError = onPlatformError;
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
