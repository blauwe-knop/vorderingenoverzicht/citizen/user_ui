// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:lottie/lottie.dart';
import 'package:user_ui/screens/common/basic_scaffold.dart';
import 'package:user_ui/screens/common/responsive_center.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ThemedErrorScreen extends StatelessWidget {
  final bool isCritical;
  final String redirectRoute;

  const ThemedErrorScreen({
    super.key,
    required this.isCritical,
    required this.redirectRoute,
  });

  static const String routeName = 'CriticalErrorScreen';

  @override
  Widget build(BuildContext context) {
    return BasicScaffold(
      actions: [
        IconButton(
          onPressed: () => context.pushNamed(
            ErrorLogScreen.routeName,
            pathParameters: {'showBackButton': 'true'},
          ),
          icon: const Icon(Icons.info_outline),
        ),
      ],
      content: Align(
        alignment: Alignment.topCenter,
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: [
                addVerticalSpace(Spacing.large.value),
                _Body(isCritical: isCritical),
                if (!isCritical) _Footer(redirectRoute: redirectRoute),
                addVerticalSpace(Spacing.large.value),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _Body extends StatelessWidget {
  const _Body({
    required this.isCritical,
  });

  final bool isCritical;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 10,
      lg: 8,
      xl: 6,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Er is een ${isCritical ? 'kritische ' : ''}fout opgetreden',
            style: context.textTheme.displayLarge,
          ),
          addVerticalSpace(Spacing.large.value),
          Text(
            isCritical
                ? 'Sluit de app en probeer het opnieuw.'
                : 'Probeer het opnieuw.',
          ),
          Lottie.asset(
            'assets/lottie/no-internet.json',
          ),
        ],
      ),
    );
  }
}

class _Footer extends StatelessWidget {
  const _Footer({
    required this.redirectRoute,
  });

  final String redirectRoute;

  @override
  Widget build(BuildContext context) {
    return ResponsiveCenter(
      xs: 12,
      md: 8,
      lg: 4,
      child: ThemedButton(
        style: ThemedButtonStyle.primary,
        buttonText: 'Probeer opnieuw',
        onPress: () => context.goNamed(redirectRoute),
      ),
    );
  }
}
