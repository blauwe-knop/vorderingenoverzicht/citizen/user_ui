import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/theme/theme_constants.dart';
import 'package:user_ui/theme/theme_extensions.dart';
import 'package:user_ui/theme/theme_spacing.dart';

class ErrorMessageScreen extends StatelessWidget {
  const ErrorMessageScreen(this.errorMessage, {super.key});

  static const routeName = 'ErrorMessageScreen';

  final String errorMessage;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(Spacing.large.value),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            errorMessage,
            style: Theme.of(context)
                .textTheme
                .titleLarge!
                .copyWith(color: context.colorScheme.error),
          ),
          addVerticalSpace(Spacing.large.value),
          ThemedButton(
              buttonText: "Verder",
              onPress: () => context.goNamed(HomeScreen.routeName))
        ],
      ),
    );
  }
}
