// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:user_ui/screens/common/themed_loading_indicator.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/usecases/app/remove_old_user_data_and_use_latest_registration.dart';

class RemoveOldUserDataAndUseLatestRegistrationScreen extends ConsumerWidget {
  static const routeName = 'RemoveOldUserDataAndUseLatestRegistrationScreen';

  const RemoveOldUserDataAndUseLatestRegistrationScreen({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ref.watch(removeOldUserDataAndUseLatestRegistrationProvider).when(
          data: (_) {
            context.goNamed(HomeScreen.routeName);
            return const SizedBox.shrink();
          },
          error: (err, stack) => Text('Error: $err'),
          loading: () => ThemedLoadingIndicator(
            onBackbuttonPress: () => context.goNamed(HomeScreen.routeName),
            text: "Oude gegevens verwijderen...",
            addDeepLink: true,
          ),
        );
  }
}
