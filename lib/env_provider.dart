import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

enum EnvironmentType {
  mock("mock"),
  local("local"),
  test("test"),
  demo("demo");

  const EnvironmentType(this.value);

  final String value;

  static EnvironmentType fromString(String value) {
    switch (value) {
      case "mock":
        return EnvironmentType.mock;
      case "local":
        return EnvironmentType.local;
      case "test":
        return EnvironmentType.test;
      default:
        return EnvironmentType.demo;
    }
  }
}

class Environment {
  late String schemeUrl;
  late EnvironmentType type;

  Environment({
    this.schemeUrl = 'https://api.stelsel.vorijk-demo.blauweknop.app/v1',
    this.type = EnvironmentType.demo,
  });

  Future<void> initialize() async {
    var value = await rootBundle.loadString('assets/env.json');

    if (value.isEmpty) {
      debugPrint("assets/env.json not found");
      //no logging to db since there is no db yet
      throw Exception("assets/env.json not found");
    }

    var responseJson = json.decode(value);
    if (responseJson == null) {
      debugPrint("assets/env.json is invalid");
      //no logging to db since there is no db yet
      throw Exception("assets/env.json is invalid");
    }

    var environmentTypeFromEnvironmentJson = responseJson["environmentType"];
    if (environmentTypeFromEnvironmentJson == null) {
      debugPrint("environmentType is not set in assets/env.json");
      //no logging to db since there is no db yet
      throw Exception("environmentType is not set in assets/env.json");
    }
    type = EnvironmentType.fromString(environmentTypeFromEnvironmentJson);

    var schemeUrlFromEnvironmentJson = responseJson["schemeUrl"];
    if (schemeUrlFromEnvironmentJson == null) {
      debugPrint("schemeUrl is not set in assets/env.json");
      //no logging to db since there is no db yet
      throw Exception("schemeUrl is not set in assets/env.json");
    }
    schemeUrl = schemeUrlFromEnvironmentJson;
  }

  Future<bool> isRunningOnWebLocalhost() async {
    if (!kIsWeb) return false;
    try {
      return Uri.base.host == 'localhost' || Uri.base.host == '127.0.0.1';
    } catch (_) {
      return false;
    }
  }
}

final environmentProvider = Provider<Environment>((ref) {
  return Environment();
});
