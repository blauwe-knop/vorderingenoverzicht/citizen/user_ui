// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:go_router/go_router.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/screens/features/activation/activation_info_screen.dart';
import 'package:user_ui/screens/features/activation/complete_activation_screen.dart';
import 'package:user_ui/screens/features/activation/deactivation_screen.dart';
import 'package:user_ui/screens/features/activation/different_bsn_screen.dart';
import 'package:user_ui/screens/features/activation/reactivation_screen.dart';
import 'package:user_ui/screens/features/activation/start_activation_screen.dart';
import 'package:user_ui/screens/features/activation/undo_latest_activation_screen.dart';
import 'package:user_ui/screens/features/app_startup/startup_screen.dart';
import 'package:user_ui/screens/features/clean_up/remove_all_data_and_user_settings_screen.dart';
import 'package:user_ui/screens/features/clean_up/remove_old_user_data_and_use_latest_registration_screen.dart';
import 'package:user_ui/screens/features/error/error_log_screen.dart';
import 'package:user_ui/screens/features/error/error_message_screen.dart';
import 'package:user_ui/screens/features/error/themed_error_screen.dart';
import 'package:user_ui/screens/features/home/home_screen.dart';
import 'package:user_ui/screens/features/loading/loading_screen.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';
import 'package:user_ui/screens/features/organization_selection/organization_selection_screen.dart';
import 'package:user_ui/screens/features/overview/financial_claims_overview_screen_shell.dart';
import 'package:user_ui/screens/features/pincode/forgot_pincode_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';
import 'package:user_ui/screens/features/toggles/feature_toggles.dart';

final goRouterProvider = Provider<GoRouter>((ref) {
  return GoRouter(
    debugLogDiagnostics: false,
    initialLocation: '/',
    redirect: (context, state) async {
      // redirect user to key generation or onboarding if not completed yet
      var keypairAvailable = await ref.read(appRootKeypairProvider.future);
      if (!keypairAvailable) {
        return '/voorbereiden';
      }

      var pincodeScreenState =
          await ref.read(pincodeScreenStateNotifierProvider.future);
      if (pincodeScreenState == PincodeScreenState.locked &&
          state.fullPath != '/pincode-vergeten') {
        return '/pincode';
      }

      return null;
    },
    routes: <RouteBase>[
      GoRoute(
        name: HomeScreen.routeName,
        path: '/',
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        name: StartupScreen.routeName,
        path: '/voorbereiden',
        builder: (context, state) => const StartupScreen(),
      ),
      GoRoute(
        name: PropositionScreen.routeName,
        path: '/wat-heb-je-aan-de-app',
        builder: (context, state) => const PropositionScreen(),
      ),
      GoRoute(
        name: DataUseScreen.routeName,
        path: '/zo-gaat-de-app-om-met-je-data',
        builder: (context, state) => const DataUseScreen(),
      ),
      GoRoute(
        name: PincodeScreen.routeName,
        path: '/pincode',
        builder: (context, state) => PincodeScreen(),
      ),
      GoRoute(
        name: ForgotPincodeScreen.routeName,
        path: '/pincode-vergeten',
        builder: (context, state) => const ForgotPincodeScreen(),
      ),
      GoRoute(
        name: OrganizationSelectionScreen.routeName,
        path: '/organisaties-kiezen',
        builder: (context, state) => const OrganizationSelectionScreen(),
      ),
      GoRoute(
        name: ActivationInfoScreen.routeName,
        path: '/activatie-informatie',
        builder: (context, state) => const ActivationInfoScreen(),
      ),
      GoRoute(
        name: StartActivationScreen.routeName,
        path: '/activatie-starten',
        builder: (context, state) => const StartActivationScreen(),
      ),
      GoRoute(
        name: CompleteActivationScreen.routeName,
        path: '/activatie-voltooien',
        builder: (context, state) => const CompleteActivationScreen(),
      ),
      GoRoute(
        name: ReactivationScreen.routeName,
        path: '/heractivatie-starten',
        builder: (context, state) => const ReactivationScreen(),
      ),
      GoRoute(
        name: DeactivationScreen.routeName,
        path: '/deactivatie-starten',
        builder: (context, state) => const DeactivationScreen(),
      ),
      GoRoute(
        name: DifferentBsnScreen.routeName,
        path: '/verschillende-bsn',
        builder: (context, state) => const DifferentBsnScreen(),
      ),
      GoRoute(
        name: RemoveOldUserDataAndUseLatestRegistrationScreen.routeName,
        path: '/oude-gegevens-verwijderen',
        builder: (context, state) =>
            const RemoveOldUserDataAndUseLatestRegistrationScreen(),
      ),
      GoRoute(
        name: RemoveAllDataAndUserSettingsScreen.routeName,
        path: '/app-leegmaken',
        builder: (context, state) => const RemoveAllDataAndUserSettingsScreen(),
      ),
      GoRoute(
        name: UndoLatestActivationScreen.routeName,
        path: '/laatste-activatie-ongedaan-maken',
        builder: (context, state) => const UndoLatestActivationScreen(),
      ),
      GoRoute(
        name: LoadingScreen.routeName,
        path: '/gegevens-ophalen',
        builder: (context, state) => const LoadingScreen(),
      ),
      GoRoute(
        name: FinancialClaimsOverviewScreen.routeName,
        path: '/overzicht',
        builder: (context, state) => const FinancialClaimsOverviewScreenShell(),
      ),
      GoRoute(
        name: FeatureToggleScreen.routeName,
        path: '/features-toggles',
        builder: (context, state) => const FeatureToggleScreen(),
      ),
      GoRoute(
        name: ErrorMessageScreen.routeName,
        path: '/foutmelding',
        builder: (context, state) => ErrorMessageScreen(state.error.toString()),
      ),
      GoRoute(
        name: ErrorLogScreen.routeName,
        path: '/foutenlogboek/:showBackButton',
        builder: (context, state) => ErrorLogScreen(
            showBackButton: state.pathParameters['showBackButton'] == 'true'),
      ),
      GoRoute(
        name: ThemedErrorScreen.routeName,
        path: '/critische-fouten/:isCritical/:redirectRoute',
        builder: (context, state) => ThemedErrorScreen(
            isCritical: state.pathParameters['isCritical'] == 'true',
            redirectRoute:
                state.pathParameters['redirectRoute'] ?? HomeScreen.routeName),
      ),
    ],
    errorBuilder: (context, state) =>
        ErrorMessageScreen(state.error.toString()),
  );
});
