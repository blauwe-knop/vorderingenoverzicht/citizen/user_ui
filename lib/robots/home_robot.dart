import 'package:flutter_test/flutter_test.dart';
import 'package:user_ui/screens/common/themed_button.dart';

class HomeRobot {
  HomeRobot(this.tester);

  WidgetTester tester;
  // Home screen
  Future<void> assertThatUserIsOnTheHomeScreen() async {
    expect(find.byType(ThemedButton), findsOneWidget);
    expect(find.bySemanticsLabel('home image'), findsOneWidget);
    expect(find.text('Hallo Piet'), findsOneWidget);
  }
}
