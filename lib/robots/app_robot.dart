import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/setup_mocked_providers.dart' as app;

class AppRobot {
  AppRobot(this.tester);

  WidgetTester tester;

  // Initialization
  Future<void> launchTheApp() async {
    WidgetsFlutterBinding.ensureInitialized();

    final environment = Environment(
      type: EnvironmentType.mock,
      schemeUrl: 'schemeUrl',
    );

    await app.setupMockedProviders(environment);
    await tester.pump(const Duration(seconds: 5));
  }
}
