// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unencrypted_database.dart';

// ignore_for_file: type=lint
class $LogRecordTableTable extends LogRecordTable
    with TableInfo<$LogRecordTableTable, LogRecordDriftModel> {
  @override
  final GeneratedDatabase attachedDatabase;
  final String? _alias;
  $LogRecordTableTable(this.attachedDatabase, [this._alias]);
  static const VerificationMeta _idMeta = const VerificationMeta('id');
  @override
  late final GeneratedColumn<int> id = GeneratedColumn<int>(
      'id', aliasedName, false,
      hasAutoIncrement: true,
      type: DriftSqlType.int,
      requiredDuringInsert: false,
      defaultConstraints:
          GeneratedColumn.constraintIsAlways('PRIMARY KEY AUTOINCREMENT'));
  static const VerificationMeta _timestampMeta =
      const VerificationMeta('timestamp');
  @override
  late final GeneratedColumn<DateTime> timestamp = GeneratedColumn<DateTime>(
      'timestamp', aliasedName, false,
      type: DriftSqlType.dateTime, requiredDuringInsert: true);
  static const VerificationMeta _hostMeta = const VerificationMeta('host');
  @override
  late final GeneratedColumn<String> host = GeneratedColumn<String>(
      'host', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _cefMeta = const VerificationMeta('cef');
  @override
  late final GeneratedColumn<int> cef = GeneratedColumn<int>(
      'cef', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _deviceVendorMeta =
      const VerificationMeta('deviceVendor');
  @override
  late final GeneratedColumn<String> deviceVendor = GeneratedColumn<String>(
      'device_vendor', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceProductMeta =
      const VerificationMeta('deviceProduct');
  @override
  late final GeneratedColumn<String> deviceProduct = GeneratedColumn<String>(
      'device_product', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceVersionMeta =
      const VerificationMeta('deviceVersion');
  @override
  late final GeneratedColumn<String> deviceVersion = GeneratedColumn<String>(
      'device_version', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _deviceEventClassIdMeta =
      const VerificationMeta('deviceEventClassId');
  @override
  late final GeneratedColumn<String> deviceEventClassId =
      GeneratedColumn<String>('device_event_class_id', aliasedName, false,
          type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _nameMeta = const VerificationMeta('name');
  @override
  late final GeneratedColumn<String> name = GeneratedColumn<String>(
      'name', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _severityMeta =
      const VerificationMeta('severity');
  @override
  late final GeneratedColumn<int> severity = GeneratedColumn<int>(
      'severity', aliasedName, false,
      type: DriftSqlType.int, requiredDuringInsert: true);
  static const VerificationMeta _flexString1LabelMeta =
      const VerificationMeta('flexString1Label');
  @override
  late final GeneratedColumn<String> flexString1Label = GeneratedColumn<String>(
      'flex_string1_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString1Meta =
      const VerificationMeta('flexString1');
  @override
  late final GeneratedColumn<String> flexString1 = GeneratedColumn<String>(
      'flex_string1', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2LabelMeta =
      const VerificationMeta('flexString2Label');
  @override
  late final GeneratedColumn<String> flexString2Label = GeneratedColumn<String>(
      'flex_string2_label', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _flexString2Meta =
      const VerificationMeta('flexString2');
  @override
  late final GeneratedColumn<String> flexString2 = GeneratedColumn<String>(
      'flex_string2', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _actMeta = const VerificationMeta('act');
  @override
  late final GeneratedColumn<String> act = GeneratedColumn<String>(
      'act', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _appMeta = const VerificationMeta('app');
  @override
  late final GeneratedColumn<String> app = GeneratedColumn<String>(
      'app', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMeta =
      const VerificationMeta('request');
  @override
  late final GeneratedColumn<String> request = GeneratedColumn<String>(
      'request', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  static const VerificationMeta _requestMethodMeta =
      const VerificationMeta('requestMethod');
  @override
  late final GeneratedColumn<String> requestMethod = GeneratedColumn<String>(
      'request_method', aliasedName, false,
      type: DriftSqlType.string, requiredDuringInsert: true);
  @override
  List<GeneratedColumn> get $columns => [
        id,
        timestamp,
        host,
        cef,
        deviceVendor,
        deviceProduct,
        deviceVersion,
        deviceEventClassId,
        name,
        severity,
        flexString1Label,
        flexString1,
        flexString2Label,
        flexString2,
        act,
        app,
        request,
        requestMethod
      ];
  @override
  String get aliasedName => _alias ?? actualTableName;
  @override
  String get actualTableName => $name;
  static const String $name = 'log_record_table';
  @override
  VerificationContext validateIntegrity(
      Insertable<LogRecordDriftModel> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id']!, _idMeta));
    }
    if (data.containsKey('timestamp')) {
      context.handle(_timestampMeta,
          timestamp.isAcceptableOrUnknown(data['timestamp']!, _timestampMeta));
    } else if (isInserting) {
      context.missing(_timestampMeta);
    }
    if (data.containsKey('host')) {
      context.handle(
          _hostMeta, host.isAcceptableOrUnknown(data['host']!, _hostMeta));
    } else if (isInserting) {
      context.missing(_hostMeta);
    }
    if (data.containsKey('cef')) {
      context.handle(
          _cefMeta, cef.isAcceptableOrUnknown(data['cef']!, _cefMeta));
    } else if (isInserting) {
      context.missing(_cefMeta);
    }
    if (data.containsKey('device_vendor')) {
      context.handle(
          _deviceVendorMeta,
          deviceVendor.isAcceptableOrUnknown(
              data['device_vendor']!, _deviceVendorMeta));
    } else if (isInserting) {
      context.missing(_deviceVendorMeta);
    }
    if (data.containsKey('device_product')) {
      context.handle(
          _deviceProductMeta,
          deviceProduct.isAcceptableOrUnknown(
              data['device_product']!, _deviceProductMeta));
    } else if (isInserting) {
      context.missing(_deviceProductMeta);
    }
    if (data.containsKey('device_version')) {
      context.handle(
          _deviceVersionMeta,
          deviceVersion.isAcceptableOrUnknown(
              data['device_version']!, _deviceVersionMeta));
    } else if (isInserting) {
      context.missing(_deviceVersionMeta);
    }
    if (data.containsKey('device_event_class_id')) {
      context.handle(
          _deviceEventClassIdMeta,
          deviceEventClassId.isAcceptableOrUnknown(
              data['device_event_class_id']!, _deviceEventClassIdMeta));
    } else if (isInserting) {
      context.missing(_deviceEventClassIdMeta);
    }
    if (data.containsKey('name')) {
      context.handle(
          _nameMeta, name.isAcceptableOrUnknown(data['name']!, _nameMeta));
    } else if (isInserting) {
      context.missing(_nameMeta);
    }
    if (data.containsKey('severity')) {
      context.handle(_severityMeta,
          severity.isAcceptableOrUnknown(data['severity']!, _severityMeta));
    } else if (isInserting) {
      context.missing(_severityMeta);
    }
    if (data.containsKey('flex_string1_label')) {
      context.handle(
          _flexString1LabelMeta,
          flexString1Label.isAcceptableOrUnknown(
              data['flex_string1_label']!, _flexString1LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString1LabelMeta);
    }
    if (data.containsKey('flex_string1')) {
      context.handle(
          _flexString1Meta,
          flexString1.isAcceptableOrUnknown(
              data['flex_string1']!, _flexString1Meta));
    } else if (isInserting) {
      context.missing(_flexString1Meta);
    }
    if (data.containsKey('flex_string2_label')) {
      context.handle(
          _flexString2LabelMeta,
          flexString2Label.isAcceptableOrUnknown(
              data['flex_string2_label']!, _flexString2LabelMeta));
    } else if (isInserting) {
      context.missing(_flexString2LabelMeta);
    }
    if (data.containsKey('flex_string2')) {
      context.handle(
          _flexString2Meta,
          flexString2.isAcceptableOrUnknown(
              data['flex_string2']!, _flexString2Meta));
    } else if (isInserting) {
      context.missing(_flexString2Meta);
    }
    if (data.containsKey('act')) {
      context.handle(
          _actMeta, act.isAcceptableOrUnknown(data['act']!, _actMeta));
    } else if (isInserting) {
      context.missing(_actMeta);
    }
    if (data.containsKey('app')) {
      context.handle(
          _appMeta, app.isAcceptableOrUnknown(data['app']!, _appMeta));
    } else if (isInserting) {
      context.missing(_appMeta);
    }
    if (data.containsKey('request')) {
      context.handle(_requestMeta,
          request.isAcceptableOrUnknown(data['request']!, _requestMeta));
    } else if (isInserting) {
      context.missing(_requestMeta);
    }
    if (data.containsKey('request_method')) {
      context.handle(
          _requestMethodMeta,
          requestMethod.isAcceptableOrUnknown(
              data['request_method']!, _requestMethodMeta));
    } else if (isInserting) {
      context.missing(_requestMethodMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  LogRecordDriftModel map(Map<String, dynamic> data, {String? tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : '';
    return LogRecordDriftModel(
      id: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}id'])!,
      timestamp: attachedDatabase.typeMapping
          .read(DriftSqlType.dateTime, data['${effectivePrefix}timestamp'])!,
      host: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}host'])!,
      cef: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}cef'])!,
      deviceVendor: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_vendor'])!,
      deviceProduct: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_product'])!,
      deviceVersion: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}device_version'])!,
      deviceEventClassId: attachedDatabase.typeMapping.read(DriftSqlType.string,
          data['${effectivePrefix}device_event_class_id'])!,
      name: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}name'])!,
      severity: attachedDatabase.typeMapping
          .read(DriftSqlType.int, data['${effectivePrefix}severity'])!,
      flexString1Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string1_label'])!,
      flexString1: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string1'])!,
      flexString2Label: attachedDatabase.typeMapping.read(
          DriftSqlType.string, data['${effectivePrefix}flex_string2_label'])!,
      flexString2: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}flex_string2'])!,
      act: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}act'])!,
      app: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}app'])!,
      request: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request'])!,
      requestMethod: attachedDatabase.typeMapping
          .read(DriftSqlType.string, data['${effectivePrefix}request_method'])!,
    );
  }

  @override
  $LogRecordTableTable createAlias(String alias) {
    return $LogRecordTableTable(attachedDatabase, alias);
  }
}

class LogRecordDriftModel extends DataClass
    implements Insertable<LogRecordDriftModel> {
  final int id;
  final DateTime timestamp;
  final String host;
  final int cef;
  final String deviceVendor;
  final String deviceProduct;
  final String deviceVersion;
  final String deviceEventClassId;
  final String name;
  final int severity;
  final String flexString1Label;
  final String flexString1;
  final String flexString2Label;
  final String flexString2;
  final String act;
  final String app;
  final String request;
  final String requestMethod;
  const LogRecordDriftModel(
      {required this.id,
      required this.timestamp,
      required this.host,
      required this.cef,
      required this.deviceVendor,
      required this.deviceProduct,
      required this.deviceVersion,
      required this.deviceEventClassId,
      required this.name,
      required this.severity,
      required this.flexString1Label,
      required this.flexString1,
      required this.flexString2Label,
      required this.flexString2,
      required this.act,
      required this.app,
      required this.request,
      required this.requestMethod});
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    map['id'] = Variable<int>(id);
    map['timestamp'] = Variable<DateTime>(timestamp);
    map['host'] = Variable<String>(host);
    map['cef'] = Variable<int>(cef);
    map['device_vendor'] = Variable<String>(deviceVendor);
    map['device_product'] = Variable<String>(deviceProduct);
    map['device_version'] = Variable<String>(deviceVersion);
    map['device_event_class_id'] = Variable<String>(deviceEventClassId);
    map['name'] = Variable<String>(name);
    map['severity'] = Variable<int>(severity);
    map['flex_string1_label'] = Variable<String>(flexString1Label);
    map['flex_string1'] = Variable<String>(flexString1);
    map['flex_string2_label'] = Variable<String>(flexString2Label);
    map['flex_string2'] = Variable<String>(flexString2);
    map['act'] = Variable<String>(act);
    map['app'] = Variable<String>(app);
    map['request'] = Variable<String>(request);
    map['request_method'] = Variable<String>(requestMethod);
    return map;
  }

  LogRecordTableCompanion toCompanion(bool nullToAbsent) {
    return LogRecordTableCompanion(
      id: Value(id),
      timestamp: Value(timestamp),
      host: Value(host),
      cef: Value(cef),
      deviceVendor: Value(deviceVendor),
      deviceProduct: Value(deviceProduct),
      deviceVersion: Value(deviceVersion),
      deviceEventClassId: Value(deviceEventClassId),
      name: Value(name),
      severity: Value(severity),
      flexString1Label: Value(flexString1Label),
      flexString1: Value(flexString1),
      flexString2Label: Value(flexString2Label),
      flexString2: Value(flexString2),
      act: Value(act),
      app: Value(app),
      request: Value(request),
      requestMethod: Value(requestMethod),
    );
  }

  factory LogRecordDriftModel.fromJson(Map<String, dynamic> json,
      {ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return LogRecordDriftModel(
      id: serializer.fromJson<int>(json['id']),
      timestamp: serializer.fromJson<DateTime>(json['timestamp']),
      host: serializer.fromJson<String>(json['host']),
      cef: serializer.fromJson<int>(json['cef']),
      deviceVendor: serializer.fromJson<String>(json['deviceVendor']),
      deviceProduct: serializer.fromJson<String>(json['deviceProduct']),
      deviceVersion: serializer.fromJson<String>(json['deviceVersion']),
      deviceEventClassId:
          serializer.fromJson<String>(json['deviceEventClassId']),
      name: serializer.fromJson<String>(json['name']),
      severity: serializer.fromJson<int>(json['severity']),
      flexString1Label: serializer.fromJson<String>(json['flexString1Label']),
      flexString1: serializer.fromJson<String>(json['flexString1']),
      flexString2Label: serializer.fromJson<String>(json['flexString2Label']),
      flexString2: serializer.fromJson<String>(json['flexString2']),
      act: serializer.fromJson<String>(json['act']),
      app: serializer.fromJson<String>(json['app']),
      request: serializer.fromJson<String>(json['request']),
      requestMethod: serializer.fromJson<String>(json['requestMethod']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer? serializer}) {
    serializer ??= driftRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'timestamp': serializer.toJson<DateTime>(timestamp),
      'host': serializer.toJson<String>(host),
      'cef': serializer.toJson<int>(cef),
      'deviceVendor': serializer.toJson<String>(deviceVendor),
      'deviceProduct': serializer.toJson<String>(deviceProduct),
      'deviceVersion': serializer.toJson<String>(deviceVersion),
      'deviceEventClassId': serializer.toJson<String>(deviceEventClassId),
      'name': serializer.toJson<String>(name),
      'severity': serializer.toJson<int>(severity),
      'flexString1Label': serializer.toJson<String>(flexString1Label),
      'flexString1': serializer.toJson<String>(flexString1),
      'flexString2Label': serializer.toJson<String>(flexString2Label),
      'flexString2': serializer.toJson<String>(flexString2),
      'act': serializer.toJson<String>(act),
      'app': serializer.toJson<String>(app),
      'request': serializer.toJson<String>(request),
      'requestMethod': serializer.toJson<String>(requestMethod),
    };
  }

  LogRecordDriftModel copyWith(
          {int? id,
          DateTime? timestamp,
          String? host,
          int? cef,
          String? deviceVendor,
          String? deviceProduct,
          String? deviceVersion,
          String? deviceEventClassId,
          String? name,
          int? severity,
          String? flexString1Label,
          String? flexString1,
          String? flexString2Label,
          String? flexString2,
          String? act,
          String? app,
          String? request,
          String? requestMethod}) =>
      LogRecordDriftModel(
        id: id ?? this.id,
        timestamp: timestamp ?? this.timestamp,
        host: host ?? this.host,
        cef: cef ?? this.cef,
        deviceVendor: deviceVendor ?? this.deviceVendor,
        deviceProduct: deviceProduct ?? this.deviceProduct,
        deviceVersion: deviceVersion ?? this.deviceVersion,
        deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
        name: name ?? this.name,
        severity: severity ?? this.severity,
        flexString1Label: flexString1Label ?? this.flexString1Label,
        flexString1: flexString1 ?? this.flexString1,
        flexString2Label: flexString2Label ?? this.flexString2Label,
        flexString2: flexString2 ?? this.flexString2,
        act: act ?? this.act,
        app: app ?? this.app,
        request: request ?? this.request,
        requestMethod: requestMethod ?? this.requestMethod,
      );
  LogRecordDriftModel copyWithCompanion(LogRecordTableCompanion data) {
    return LogRecordDriftModel(
      id: data.id.present ? data.id.value : this.id,
      timestamp: data.timestamp.present ? data.timestamp.value : this.timestamp,
      host: data.host.present ? data.host.value : this.host,
      cef: data.cef.present ? data.cef.value : this.cef,
      deviceVendor: data.deviceVendor.present
          ? data.deviceVendor.value
          : this.deviceVendor,
      deviceProduct: data.deviceProduct.present
          ? data.deviceProduct.value
          : this.deviceProduct,
      deviceVersion: data.deviceVersion.present
          ? data.deviceVersion.value
          : this.deviceVersion,
      deviceEventClassId: data.deviceEventClassId.present
          ? data.deviceEventClassId.value
          : this.deviceEventClassId,
      name: data.name.present ? data.name.value : this.name,
      severity: data.severity.present ? data.severity.value : this.severity,
      flexString1Label: data.flexString1Label.present
          ? data.flexString1Label.value
          : this.flexString1Label,
      flexString1:
          data.flexString1.present ? data.flexString1.value : this.flexString1,
      flexString2Label: data.flexString2Label.present
          ? data.flexString2Label.value
          : this.flexString2Label,
      flexString2:
          data.flexString2.present ? data.flexString2.value : this.flexString2,
      act: data.act.present ? data.act.value : this.act,
      app: data.app.present ? data.app.value : this.app,
      request: data.request.present ? data.request.value : this.request,
      requestMethod: data.requestMethod.present
          ? data.requestMethod.value
          : this.requestMethod,
    );
  }

  @override
  String toString() {
    return (StringBuffer('LogRecordDriftModel(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => Object.hash(
      id,
      timestamp,
      host,
      cef,
      deviceVendor,
      deviceProduct,
      deviceVersion,
      deviceEventClassId,
      name,
      severity,
      flexString1Label,
      flexString1,
      flexString2Label,
      flexString2,
      act,
      app,
      request,
      requestMethod);
  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is LogRecordDriftModel &&
          other.id == this.id &&
          other.timestamp == this.timestamp &&
          other.host == this.host &&
          other.cef == this.cef &&
          other.deviceVendor == this.deviceVendor &&
          other.deviceProduct == this.deviceProduct &&
          other.deviceVersion == this.deviceVersion &&
          other.deviceEventClassId == this.deviceEventClassId &&
          other.name == this.name &&
          other.severity == this.severity &&
          other.flexString1Label == this.flexString1Label &&
          other.flexString1 == this.flexString1 &&
          other.flexString2Label == this.flexString2Label &&
          other.flexString2 == this.flexString2 &&
          other.act == this.act &&
          other.app == this.app &&
          other.request == this.request &&
          other.requestMethod == this.requestMethod);
}

class LogRecordTableCompanion extends UpdateCompanion<LogRecordDriftModel> {
  final Value<int> id;
  final Value<DateTime> timestamp;
  final Value<String> host;
  final Value<int> cef;
  final Value<String> deviceVendor;
  final Value<String> deviceProduct;
  final Value<String> deviceVersion;
  final Value<String> deviceEventClassId;
  final Value<String> name;
  final Value<int> severity;
  final Value<String> flexString1Label;
  final Value<String> flexString1;
  final Value<String> flexString2Label;
  final Value<String> flexString2;
  final Value<String> act;
  final Value<String> app;
  final Value<String> request;
  final Value<String> requestMethod;
  const LogRecordTableCompanion({
    this.id = const Value.absent(),
    this.timestamp = const Value.absent(),
    this.host = const Value.absent(),
    this.cef = const Value.absent(),
    this.deviceVendor = const Value.absent(),
    this.deviceProduct = const Value.absent(),
    this.deviceVersion = const Value.absent(),
    this.deviceEventClassId = const Value.absent(),
    this.name = const Value.absent(),
    this.severity = const Value.absent(),
    this.flexString1Label = const Value.absent(),
    this.flexString1 = const Value.absent(),
    this.flexString2Label = const Value.absent(),
    this.flexString2 = const Value.absent(),
    this.act = const Value.absent(),
    this.app = const Value.absent(),
    this.request = const Value.absent(),
    this.requestMethod = const Value.absent(),
  });
  LogRecordTableCompanion.insert({
    this.id = const Value.absent(),
    required DateTime timestamp,
    required String host,
    required int cef,
    required String deviceVendor,
    required String deviceProduct,
    required String deviceVersion,
    required String deviceEventClassId,
    required String name,
    required int severity,
    required String flexString1Label,
    required String flexString1,
    required String flexString2Label,
    required String flexString2,
    required String act,
    required String app,
    required String request,
    required String requestMethod,
  })  : timestamp = Value(timestamp),
        host = Value(host),
        cef = Value(cef),
        deviceVendor = Value(deviceVendor),
        deviceProduct = Value(deviceProduct),
        deviceVersion = Value(deviceVersion),
        deviceEventClassId = Value(deviceEventClassId),
        name = Value(name),
        severity = Value(severity),
        flexString1Label = Value(flexString1Label),
        flexString1 = Value(flexString1),
        flexString2Label = Value(flexString2Label),
        flexString2 = Value(flexString2),
        act = Value(act),
        app = Value(app),
        request = Value(request),
        requestMethod = Value(requestMethod);
  static Insertable<LogRecordDriftModel> custom({
    Expression<int>? id,
    Expression<DateTime>? timestamp,
    Expression<String>? host,
    Expression<int>? cef,
    Expression<String>? deviceVendor,
    Expression<String>? deviceProduct,
    Expression<String>? deviceVersion,
    Expression<String>? deviceEventClassId,
    Expression<String>? name,
    Expression<int>? severity,
    Expression<String>? flexString1Label,
    Expression<String>? flexString1,
    Expression<String>? flexString2Label,
    Expression<String>? flexString2,
    Expression<String>? act,
    Expression<String>? app,
    Expression<String>? request,
    Expression<String>? requestMethod,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (timestamp != null) 'timestamp': timestamp,
      if (host != null) 'host': host,
      if (cef != null) 'cef': cef,
      if (deviceVendor != null) 'device_vendor': deviceVendor,
      if (deviceProduct != null) 'device_product': deviceProduct,
      if (deviceVersion != null) 'device_version': deviceVersion,
      if (deviceEventClassId != null)
        'device_event_class_id': deviceEventClassId,
      if (name != null) 'name': name,
      if (severity != null) 'severity': severity,
      if (flexString1Label != null) 'flex_string1_label': flexString1Label,
      if (flexString1 != null) 'flex_string1': flexString1,
      if (flexString2Label != null) 'flex_string2_label': flexString2Label,
      if (flexString2 != null) 'flex_string2': flexString2,
      if (act != null) 'act': act,
      if (app != null) 'app': app,
      if (request != null) 'request': request,
      if (requestMethod != null) 'request_method': requestMethod,
    });
  }

  LogRecordTableCompanion copyWith(
      {Value<int>? id,
      Value<DateTime>? timestamp,
      Value<String>? host,
      Value<int>? cef,
      Value<String>? deviceVendor,
      Value<String>? deviceProduct,
      Value<String>? deviceVersion,
      Value<String>? deviceEventClassId,
      Value<String>? name,
      Value<int>? severity,
      Value<String>? flexString1Label,
      Value<String>? flexString1,
      Value<String>? flexString2Label,
      Value<String>? flexString2,
      Value<String>? act,
      Value<String>? app,
      Value<String>? request,
      Value<String>? requestMethod}) {
    return LogRecordTableCompanion(
      id: id ?? this.id,
      timestamp: timestamp ?? this.timestamp,
      host: host ?? this.host,
      cef: cef ?? this.cef,
      deviceVendor: deviceVendor ?? this.deviceVendor,
      deviceProduct: deviceProduct ?? this.deviceProduct,
      deviceVersion: deviceVersion ?? this.deviceVersion,
      deviceEventClassId: deviceEventClassId ?? this.deviceEventClassId,
      name: name ?? this.name,
      severity: severity ?? this.severity,
      flexString1Label: flexString1Label ?? this.flexString1Label,
      flexString1: flexString1 ?? this.flexString1,
      flexString2Label: flexString2Label ?? this.flexString2Label,
      flexString2: flexString2 ?? this.flexString2,
      act: act ?? this.act,
      app: app ?? this.app,
      request: request ?? this.request,
      requestMethod: requestMethod ?? this.requestMethod,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (timestamp.present) {
      map['timestamp'] = Variable<DateTime>(timestamp.value);
    }
    if (host.present) {
      map['host'] = Variable<String>(host.value);
    }
    if (cef.present) {
      map['cef'] = Variable<int>(cef.value);
    }
    if (deviceVendor.present) {
      map['device_vendor'] = Variable<String>(deviceVendor.value);
    }
    if (deviceProduct.present) {
      map['device_product'] = Variable<String>(deviceProduct.value);
    }
    if (deviceVersion.present) {
      map['device_version'] = Variable<String>(deviceVersion.value);
    }
    if (deviceEventClassId.present) {
      map['device_event_class_id'] = Variable<String>(deviceEventClassId.value);
    }
    if (name.present) {
      map['name'] = Variable<String>(name.value);
    }
    if (severity.present) {
      map['severity'] = Variable<int>(severity.value);
    }
    if (flexString1Label.present) {
      map['flex_string1_label'] = Variable<String>(flexString1Label.value);
    }
    if (flexString1.present) {
      map['flex_string1'] = Variable<String>(flexString1.value);
    }
    if (flexString2Label.present) {
      map['flex_string2_label'] = Variable<String>(flexString2Label.value);
    }
    if (flexString2.present) {
      map['flex_string2'] = Variable<String>(flexString2.value);
    }
    if (act.present) {
      map['act'] = Variable<String>(act.value);
    }
    if (app.present) {
      map['app'] = Variable<String>(app.value);
    }
    if (request.present) {
      map['request'] = Variable<String>(request.value);
    }
    if (requestMethod.present) {
      map['request_method'] = Variable<String>(requestMethod.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('LogRecordTableCompanion(')
          ..write('id: $id, ')
          ..write('timestamp: $timestamp, ')
          ..write('host: $host, ')
          ..write('cef: $cef, ')
          ..write('deviceVendor: $deviceVendor, ')
          ..write('deviceProduct: $deviceProduct, ')
          ..write('deviceVersion: $deviceVersion, ')
          ..write('deviceEventClassId: $deviceEventClassId, ')
          ..write('name: $name, ')
          ..write('severity: $severity, ')
          ..write('flexString1Label: $flexString1Label, ')
          ..write('flexString1: $flexString1, ')
          ..write('flexString2Label: $flexString2Label, ')
          ..write('flexString2: $flexString2, ')
          ..write('act: $act, ')
          ..write('app: $app, ')
          ..write('request: $request, ')
          ..write('requestMethod: $requestMethod')
          ..write(')'))
        .toString();
  }
}

abstract class _$UnencryptedDatabase extends GeneratedDatabase {
  _$UnencryptedDatabase(QueryExecutor e) : super(e);
  $UnencryptedDatabaseManager get managers => $UnencryptedDatabaseManager(this);
  late final $LogRecordTableTable logRecordTable = $LogRecordTableTable(this);
  @override
  Iterable<TableInfo<Table, Object?>> get allTables =>
      allSchemaEntities.whereType<TableInfo<Table, Object?>>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [logRecordTable];
}

typedef $$LogRecordTableTableCreateCompanionBuilder = LogRecordTableCompanion
    Function({
  Value<int> id,
  required DateTime timestamp,
  required String host,
  required int cef,
  required String deviceVendor,
  required String deviceProduct,
  required String deviceVersion,
  required String deviceEventClassId,
  required String name,
  required int severity,
  required String flexString1Label,
  required String flexString1,
  required String flexString2Label,
  required String flexString2,
  required String act,
  required String app,
  required String request,
  required String requestMethod,
});
typedef $$LogRecordTableTableUpdateCompanionBuilder = LogRecordTableCompanion
    Function({
  Value<int> id,
  Value<DateTime> timestamp,
  Value<String> host,
  Value<int> cef,
  Value<String> deviceVendor,
  Value<String> deviceProduct,
  Value<String> deviceVersion,
  Value<String> deviceEventClassId,
  Value<String> name,
  Value<int> severity,
  Value<String> flexString1Label,
  Value<String> flexString1,
  Value<String> flexString2Label,
  Value<String> flexString2,
  Value<String> act,
  Value<String> app,
  Value<String> request,
  Value<String> requestMethod,
});

class $$LogRecordTableTableFilterComposer
    extends Composer<_$UnencryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableFilterComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnFilters<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnFilters(column));

  ColumnFilters<DateTime> get timestamp => $composableBuilder(
      column: $table.timestamp, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get host => $composableBuilder(
      column: $table.host, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get cef => $composableBuilder(
      column: $table.cef, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnFilters(column));

  ColumnFilters<int> get severity => $composableBuilder(
      column: $table.severity, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label,
      builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get act => $composableBuilder(
      column: $table.act, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get app => $composableBuilder(
      column: $table.app, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get request => $composableBuilder(
      column: $table.request, builder: (column) => ColumnFilters(column));

  ColumnFilters<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod, builder: (column) => ColumnFilters(column));
}

class $$LogRecordTableTableOrderingComposer
    extends Composer<_$UnencryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableOrderingComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  ColumnOrderings<int> get id => $composableBuilder(
      column: $table.id, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<DateTime> get timestamp => $composableBuilder(
      column: $table.timestamp, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get host => $composableBuilder(
      column: $table.host, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get cef => $composableBuilder(
      column: $table.cef, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get name => $composableBuilder(
      column: $table.name, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<int> get severity => $composableBuilder(
      column: $table.severity, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label,
      builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get act => $composableBuilder(
      column: $table.act, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get app => $composableBuilder(
      column: $table.app, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get request => $composableBuilder(
      column: $table.request, builder: (column) => ColumnOrderings(column));

  ColumnOrderings<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod,
      builder: (column) => ColumnOrderings(column));
}

class $$LogRecordTableTableAnnotationComposer
    extends Composer<_$UnencryptedDatabase, $LogRecordTableTable> {
  $$LogRecordTableTableAnnotationComposer({
    required super.$db,
    required super.$table,
    super.joinBuilder,
    super.$addJoinBuilderToRootComposer,
    super.$removeJoinBuilderFromRootComposer,
  });
  GeneratedColumn<int> get id =>
      $composableBuilder(column: $table.id, builder: (column) => column);

  GeneratedColumn<DateTime> get timestamp =>
      $composableBuilder(column: $table.timestamp, builder: (column) => column);

  GeneratedColumn<String> get host =>
      $composableBuilder(column: $table.host, builder: (column) => column);

  GeneratedColumn<int> get cef =>
      $composableBuilder(column: $table.cef, builder: (column) => column);

  GeneratedColumn<String> get deviceVendor => $composableBuilder(
      column: $table.deviceVendor, builder: (column) => column);

  GeneratedColumn<String> get deviceProduct => $composableBuilder(
      column: $table.deviceProduct, builder: (column) => column);

  GeneratedColumn<String> get deviceVersion => $composableBuilder(
      column: $table.deviceVersion, builder: (column) => column);

  GeneratedColumn<String> get deviceEventClassId => $composableBuilder(
      column: $table.deviceEventClassId, builder: (column) => column);

  GeneratedColumn<String> get name =>
      $composableBuilder(column: $table.name, builder: (column) => column);

  GeneratedColumn<int> get severity =>
      $composableBuilder(column: $table.severity, builder: (column) => column);

  GeneratedColumn<String> get flexString1Label => $composableBuilder(
      column: $table.flexString1Label, builder: (column) => column);

  GeneratedColumn<String> get flexString1 => $composableBuilder(
      column: $table.flexString1, builder: (column) => column);

  GeneratedColumn<String> get flexString2Label => $composableBuilder(
      column: $table.flexString2Label, builder: (column) => column);

  GeneratedColumn<String> get flexString2 => $composableBuilder(
      column: $table.flexString2, builder: (column) => column);

  GeneratedColumn<String> get act =>
      $composableBuilder(column: $table.act, builder: (column) => column);

  GeneratedColumn<String> get app =>
      $composableBuilder(column: $table.app, builder: (column) => column);

  GeneratedColumn<String> get request =>
      $composableBuilder(column: $table.request, builder: (column) => column);

  GeneratedColumn<String> get requestMethod => $composableBuilder(
      column: $table.requestMethod, builder: (column) => column);
}

class $$LogRecordTableTableTableManager extends RootTableManager<
    _$UnencryptedDatabase,
    $LogRecordTableTable,
    LogRecordDriftModel,
    $$LogRecordTableTableFilterComposer,
    $$LogRecordTableTableOrderingComposer,
    $$LogRecordTableTableAnnotationComposer,
    $$LogRecordTableTableCreateCompanionBuilder,
    $$LogRecordTableTableUpdateCompanionBuilder,
    (
      LogRecordDriftModel,
      BaseReferences<_$UnencryptedDatabase, $LogRecordTableTable,
          LogRecordDriftModel>
    ),
    LogRecordDriftModel,
    PrefetchHooks Function()> {
  $$LogRecordTableTableTableManager(
      _$UnencryptedDatabase db, $LogRecordTableTable table)
      : super(TableManagerState(
          db: db,
          table: table,
          createFilteringComposer: () =>
              $$LogRecordTableTableFilterComposer($db: db, $table: table),
          createOrderingComposer: () =>
              $$LogRecordTableTableOrderingComposer($db: db, $table: table),
          createComputedFieldComposer: () =>
              $$LogRecordTableTableAnnotationComposer($db: db, $table: table),
          updateCompanionCallback: ({
            Value<int> id = const Value.absent(),
            Value<DateTime> timestamp = const Value.absent(),
            Value<String> host = const Value.absent(),
            Value<int> cef = const Value.absent(),
            Value<String> deviceVendor = const Value.absent(),
            Value<String> deviceProduct = const Value.absent(),
            Value<String> deviceVersion = const Value.absent(),
            Value<String> deviceEventClassId = const Value.absent(),
            Value<String> name = const Value.absent(),
            Value<int> severity = const Value.absent(),
            Value<String> flexString1Label = const Value.absent(),
            Value<String> flexString1 = const Value.absent(),
            Value<String> flexString2Label = const Value.absent(),
            Value<String> flexString2 = const Value.absent(),
            Value<String> act = const Value.absent(),
            Value<String> app = const Value.absent(),
            Value<String> request = const Value.absent(),
            Value<String> requestMethod = const Value.absent(),
          }) =>
              LogRecordTableCompanion(
            id: id,
            timestamp: timestamp,
            host: host,
            cef: cef,
            deviceVendor: deviceVendor,
            deviceProduct: deviceProduct,
            deviceVersion: deviceVersion,
            deviceEventClassId: deviceEventClassId,
            name: name,
            severity: severity,
            flexString1Label: flexString1Label,
            flexString1: flexString1,
            flexString2Label: flexString2Label,
            flexString2: flexString2,
            act: act,
            app: app,
            request: request,
            requestMethod: requestMethod,
          ),
          createCompanionCallback: ({
            Value<int> id = const Value.absent(),
            required DateTime timestamp,
            required String host,
            required int cef,
            required String deviceVendor,
            required String deviceProduct,
            required String deviceVersion,
            required String deviceEventClassId,
            required String name,
            required int severity,
            required String flexString1Label,
            required String flexString1,
            required String flexString2Label,
            required String flexString2,
            required String act,
            required String app,
            required String request,
            required String requestMethod,
          }) =>
              LogRecordTableCompanion.insert(
            id: id,
            timestamp: timestamp,
            host: host,
            cef: cef,
            deviceVendor: deviceVendor,
            deviceProduct: deviceProduct,
            deviceVersion: deviceVersion,
            deviceEventClassId: deviceEventClassId,
            name: name,
            severity: severity,
            flexString1Label: flexString1Label,
            flexString1: flexString1,
            flexString2Label: flexString2Label,
            flexString2: flexString2,
            act: act,
            app: app,
            request: request,
            requestMethod: requestMethod,
          ),
          withReferenceMapper: (p0) => p0
              .map((e) => (e.readTable(table), BaseReferences(db, table, e)))
              .toList(),
          prefetchHooksCallback: null,
        ));
}

typedef $$LogRecordTableTableProcessedTableManager = ProcessedTableManager<
    _$UnencryptedDatabase,
    $LogRecordTableTable,
    LogRecordDriftModel,
    $$LogRecordTableTableFilterComposer,
    $$LogRecordTableTableOrderingComposer,
    $$LogRecordTableTableAnnotationComposer,
    $$LogRecordTableTableCreateCompanionBuilder,
    $$LogRecordTableTableUpdateCompanionBuilder,
    (
      LogRecordDriftModel,
      BaseReferences<_$UnencryptedDatabase, $LogRecordTableTable,
          LogRecordDriftModel>
    ),
    LogRecordDriftModel,
    PrefetchHooks Function()>;

class $UnencryptedDatabaseManager {
  final _$UnencryptedDatabase _db;
  $UnencryptedDatabaseManager(this._db);
  $$LogRecordTableTableTableManager get logRecordTable =>
      $$LogRecordTableTableTableManager(_db, _db.logRecordTable);
}
