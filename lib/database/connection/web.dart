import 'dart:async';

import 'package:drift/drift.dart';
import 'package:drift/wasm.dart';
import 'package:http/http.dart' as http;
import 'package:sqlite3/wasm.dart';

/// Obtains a database connection for running drift on the web.
DatabaseConnection connect(String databaseFileName, String passphrase) {
  return DatabaseConnection.delayed(Future(() async {
    final response = await http.get(Uri.parse('sqlite3.wasm'));
    final fs = await IndexedDbFileSystem.open(dbName: '/$databaseFileName/');
    final sqlite3 = await WasmSqlite3.load(
      response.bodyBytes,
    );
    sqlite3.registerVirtualFileSystem(fs);

    const path = '/drift/db/vo_rijk';
    final db = DatabaseConnection(WasmDatabase(
      sqlite3: sqlite3,
      path: path,
      fileSystem: fs,
      logStatements: true,
    ));
    return db;
  }));
}

Future<void> validateDatabaseSchema(GeneratedDatabase database) async {
  // Unfortunately, validating database schemas only works for native platforms
  // right now.
  // As we also have migration tests (see the `Testing migrations` section in
  // the readme of this example), this is not a huge issue.
}
