import 'package:flutter/material.dart';
import 'package:user_ui/theme/theme_extended_colors.dart';
import 'package:user_ui/theme/theme_extensions.dart';

extension SeverityExtension on int {
  Color getSeverityColor(BuildContext context) {
    if (this == 0) {
      return ThemeExtendedColors.severityUnknown;
    } else if (this >= 1 && this <= 2) {
      return ThemeExtendedColors.severityInfo;
    } else if (this >= 3 && this <= 5) {
      return ThemeExtendedColors.severityLow;
    } else if (this >= 6 && this <= 7) {
      return ThemeExtendedColors.severityMedium;
    } else if (this >= 8 && this <= 10) {
      return ThemeExtendedColors.severityHigh;
    } else {
      return context.colorScheme.surfaceContainerHigh;
    }
  }

  IconData get severityIcon {
    if (this >= 0 && this <= 2) {
      return Icons.info_outline;
    } else if (this >= 3 && this <= 5) {
      return Icons.warning_amber_outlined;
    } else if (this >= 6 && this <= 10) {
      return Icons.error_outline;
    } else {
      return Icons.question_mark;
    }
  }
}
