import 'package:drift/drift.dart';

@DataClassName('AppManagerSelectionDriftModel')
class AppManagerSelectionTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
}
