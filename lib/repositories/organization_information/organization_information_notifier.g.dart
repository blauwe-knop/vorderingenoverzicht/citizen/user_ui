// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'organization_information_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$getOrganizationInformationHash() =>
    r'8cb92e293cfa483c454ecf1df6c045d3339ea971';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [getOrganizationInformation].
@ProviderFor(getOrganizationInformation)
const getOrganizationInformationProvider = GetOrganizationInformationFamily();

/// See also [getOrganizationInformation].
class GetOrganizationInformationFamily
    extends Family<AsyncValue<OrganizationInformation>> {
  /// See also [getOrganizationInformation].
  const GetOrganizationInformationFamily();

  /// See also [getOrganizationInformation].
  GetOrganizationInformationProvider call({
    required String oin,
  }) {
    return GetOrganizationInformationProvider(
      oin: oin,
    );
  }

  @override
  GetOrganizationInformationProvider getProviderOverride(
    covariant GetOrganizationInformationProvider provider,
  ) {
    return call(
      oin: provider.oin,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'getOrganizationInformationProvider';
}

/// See also [getOrganizationInformation].
class GetOrganizationInformationProvider
    extends AutoDisposeFutureProvider<OrganizationInformation> {
  /// See also [getOrganizationInformation].
  GetOrganizationInformationProvider({
    required String oin,
  }) : this._internal(
          (ref) => getOrganizationInformation(
            ref as GetOrganizationInformationRef,
            oin: oin,
          ),
          from: getOrganizationInformationProvider,
          name: r'getOrganizationInformationProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$getOrganizationInformationHash,
          dependencies: GetOrganizationInformationFamily._dependencies,
          allTransitiveDependencies:
              GetOrganizationInformationFamily._allTransitiveDependencies,
          oin: oin,
        );

  GetOrganizationInformationProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.oin,
  }) : super.internal();

  final String oin;

  @override
  Override overrideWith(
    FutureOr<OrganizationInformation> Function(
            GetOrganizationInformationRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GetOrganizationInformationProvider._internal(
        (ref) => create(ref as GetOrganizationInformationRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        oin: oin,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<OrganizationInformation> createElement() {
    return _GetOrganizationInformationProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GetOrganizationInformationProvider && other.oin == oin;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, oin.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GetOrganizationInformationRef
    on AutoDisposeFutureProviderRef<OrganizationInformation> {
  /// The parameter `oin` of this provider.
  String get oin;
}

class _GetOrganizationInformationProviderElement
    extends AutoDisposeFutureProviderElement<OrganizationInformation>
    with GetOrganizationInformationRef {
  _GetOrganizationInformationProviderElement(super.provider);

  @override
  String get oin => (origin as GetOrganizationInformationProvider).oin;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
