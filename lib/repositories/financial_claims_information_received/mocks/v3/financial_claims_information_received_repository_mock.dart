// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:fcid_library/fcid_v3.dart';
import 'package:flutter/services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_received.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';

part 'financial_claims_information_received_repository_mock.g.dart';

@riverpod
class FinancialClaimsInformationReceivedRepositoryMock
    extends _$FinancialClaimsInformationReceivedRepositoryMock
    implements FinancialClaimsInformationReceivedRepository {
  @override
  Future<List<FinancialClaimsInformationReceived>> build() {
    return _financialClaimsInformationReceived;
  }

  Future<List<FinancialClaimsInformationReceived>>
      get _financialClaimsInformationReceived async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final aesKey = keys["aesKey"];
    final encryptedMessage = Aes.encrypt(
      Base64.toBytes(aesKey),
      utf8.encode('1234'),
    );

    final document = FinancialClaimsInformationDocument(
      type: 'type',
      version: 'version',
      body: FinancialClaimsInformationBody(
        aangeleverdDoor: 'aangeleverdDoor',
        documentDatumtijd: DateTime.now(),
        bsn: 'bsn',
        financieleZaken: [
          FinancialClaimsInformationFinancieleZaak(
            zaakkenmerk: 'zaakkenmerk',
            bsn: 'bsn',
            totaalOpgelegd: 1000,
            totaalReedsBetaald: 1,
            saldo: 999,
            saldoDatumtijd: DateTime.now(),
            gebeurtenissen: [],
            achterstanden: [],
          )
        ],
      ),
    );

    return [
      FinancialClaimsInformationReceived(
        id: 1,
        oin: 'oin',
        dateTimeRequested: DateTime.now(),
        dateTimeReceived: DateTime.now(),
        encryptedFinancialClaimsInformationDocument:
            Base64.parseFromBytes(encryptedMessage),
        financialClaimsInformationDocument: json.encode(document.toJson()),
        copiedToInbox: false,
        changed: null,
      ),
      FinancialClaimsInformationReceived(
        id: 1,
        oin: 'oin',
        dateTimeRequested: DateTime.now(),
        dateTimeReceived: DateTime.now(),
        encryptedFinancialClaimsInformationDocument:
            Base64.parseFromBytes(encryptedMessage),
        financialClaimsInformationDocument: json.encode(document.toJson()),
        copiedToInbox: false,
        changed: true,
      ),
    ];
  }

  @override
  Future<void> add({
    required String oin,
    required DateTime dateTimeRequested,
    required DateTime dateTimeReceived,
    required String encryptedFinancialClaimsInformationDocument,
  }) async =>
      state = AsyncData(await _financialClaimsInformationReceived);

  @override
  Future<void> updateFinancialClaimsInformationReceived(
          FinancialClaimsInformationReceived
              financialClaimsInformationReceived) async =>
      state = AsyncData(await _financialClaimsInformationReceived);

  @override
  Future<void> clear() async => state = const AsyncData([]);
}
