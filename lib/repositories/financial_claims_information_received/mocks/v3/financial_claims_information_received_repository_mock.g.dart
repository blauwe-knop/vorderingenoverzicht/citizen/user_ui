// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'financial_claims_information_received_repository_mock.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$financialClaimsInformationReceivedRepositoryMockHash() =>
    r'f12c52adfb809ee981455fab280435b3cf5caf1e';

/// See also [FinancialClaimsInformationReceivedRepositoryMock].
@ProviderFor(FinancialClaimsInformationReceivedRepositoryMock)
final financialClaimsInformationReceivedRepositoryMockProvider =
    AutoDisposeAsyncNotifierProvider<
        FinancialClaimsInformationReceivedRepositoryMock,
        List<FinancialClaimsInformationReceived>>.internal(
  FinancialClaimsInformationReceivedRepositoryMock.new,
  name: r'financialClaimsInformationReceivedRepositoryMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$financialClaimsInformationReceivedRepositoryMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$FinancialClaimsInformationReceivedRepositoryMock
    = AutoDisposeAsyncNotifier<List<FinancialClaimsInformationReceived>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
