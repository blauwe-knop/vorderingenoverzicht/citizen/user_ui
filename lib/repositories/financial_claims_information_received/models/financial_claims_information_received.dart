import 'package:drift/drift.dart';

@DataClassName('FinancialClaimsInformationReceivedDriftModel')
class FinancialClaimsInformationReceivedTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  DateTimeColumn get dateTimeRequested => dateTime()();
  DateTimeColumn get dateTimeReceived => dateTime()();
  TextColumn get encryptedFinancialClaimsInformationDocument => text()();
  TextColumn get financialClaimsInformationDocument => text().nullable()();
  BoolColumn get changed => boolean().nullable()();
  BoolColumn get copiedToInbox => boolean()();
}
