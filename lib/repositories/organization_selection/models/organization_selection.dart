import 'package:drift/drift.dart';

@DataClassName('OrganizationSelectionDriftModel')
class OrganizationSelectionTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  BoolColumn get selected => boolean()();
}
