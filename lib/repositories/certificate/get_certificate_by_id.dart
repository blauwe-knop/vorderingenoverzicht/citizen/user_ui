// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';

part 'get_certificate_by_id.g.dart';

@riverpod
class GetCertificateById extends _$GetCertificateById {
  @override
  Future<Certificate> build({required int id}) async {
    final certificates = await ref.watch(certificateRepositoryProvider.future);

    return certificates.firstWhere((certificate) => certificate.id == id);
  }
}
