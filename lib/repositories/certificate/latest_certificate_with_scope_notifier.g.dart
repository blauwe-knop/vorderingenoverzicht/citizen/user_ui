// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_certificate_with_scope_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestCertificateWithScopeHash() =>
    r'afee49f7881f2f1599565f313341babac8b187a4';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$LatestCertificateWithScope
    extends BuildlessAutoDisposeAsyncNotifier<Certificate?> {
  late final String scope;

  FutureOr<Certificate?> build(
    String scope,
  );
}

/// See also [LatestCertificateWithScope].
@ProviderFor(LatestCertificateWithScope)
const latestCertificateWithScopeProvider = LatestCertificateWithScopeFamily();

/// See also [LatestCertificateWithScope].
class LatestCertificateWithScopeFamily
    extends Family<AsyncValue<Certificate?>> {
  /// See also [LatestCertificateWithScope].
  const LatestCertificateWithScopeFamily();

  /// See also [LatestCertificateWithScope].
  LatestCertificateWithScopeProvider call(
    String scope,
  ) {
    return LatestCertificateWithScopeProvider(
      scope,
    );
  }

  @override
  LatestCertificateWithScopeProvider getProviderOverride(
    covariant LatestCertificateWithScopeProvider provider,
  ) {
    return call(
      provider.scope,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'latestCertificateWithScopeProvider';
}

/// See also [LatestCertificateWithScope].
class LatestCertificateWithScopeProvider
    extends AutoDisposeAsyncNotifierProviderImpl<LatestCertificateWithScope,
        Certificate?> {
  /// See also [LatestCertificateWithScope].
  LatestCertificateWithScopeProvider(
    String scope,
  ) : this._internal(
          () => LatestCertificateWithScope()..scope = scope,
          from: latestCertificateWithScopeProvider,
          name: r'latestCertificateWithScopeProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$latestCertificateWithScopeHash,
          dependencies: LatestCertificateWithScopeFamily._dependencies,
          allTransitiveDependencies:
              LatestCertificateWithScopeFamily._allTransitiveDependencies,
          scope: scope,
        );

  LatestCertificateWithScopeProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.scope,
  }) : super.internal();

  final String scope;

  @override
  FutureOr<Certificate?> runNotifierBuild(
    covariant LatestCertificateWithScope notifier,
  ) {
    return notifier.build(
      scope,
    );
  }

  @override
  Override overrideWith(LatestCertificateWithScope Function() create) {
    return ProviderOverride(
      origin: this,
      override: LatestCertificateWithScopeProvider._internal(
        () => create()..scope = scope,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        scope: scope,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<LatestCertificateWithScope,
      Certificate?> createElement() {
    return _LatestCertificateWithScopeProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LatestCertificateWithScopeProvider && other.scope == scope;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, scope.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin LatestCertificateWithScopeRef
    on AutoDisposeAsyncNotifierProviderRef<Certificate?> {
  /// The parameter `scope` of this provider.
  String get scope;
}

class _LatestCertificateWithScopeProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<LatestCertificateWithScope,
        Certificate?> with LatestCertificateWithScopeRef {
  _LatestCertificateWithScopeProviderElement(super.provider);

  @override
  String get scope => (origin as LatestCertificateWithScopeProvider).scope;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
