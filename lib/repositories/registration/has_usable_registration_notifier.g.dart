// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'has_usable_registration_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$hasUsableRegistrationHash() =>
    r'bda33a48d8e9e55ca4db3e0f6daaaa4d79350c8d';

/// See also [HasUsableRegistration].
@ProviderFor(HasUsableRegistration)
final hasUsableRegistrationProvider =
    AutoDisposeAsyncNotifierProvider<HasUsableRegistration, bool>.internal(
  HasUsableRegistration.new,
  name: r'hasUsableRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$hasUsableRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$HasUsableRegistration = AutoDisposeAsyncNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
