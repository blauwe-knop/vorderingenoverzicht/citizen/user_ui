// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';

part 'latest_started_registration_notifier.g.dart';

@riverpod
class LatestStartedRegistration extends _$LatestStartedRegistration {
  @override
  Future<Registration> build() async {
    var registrations = await ref.watch(registrationRepositoryProvider.future);

    var startedRegistrations = registrations.where((registration) =>
        !registration.revoked &&
        !registration.expired &&
        registration.dateTimeCompleted == null);

    return startedRegistrations.last;
  }
}
