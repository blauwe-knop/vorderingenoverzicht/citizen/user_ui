// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_started_registration_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestStartedRegistrationHash() =>
    r'839e564829b1399567e6168de700258062f40038';

/// See also [LatestStartedRegistration].
@ProviderFor(LatestStartedRegistration)
final latestStartedRegistrationProvider = AutoDisposeAsyncNotifierProvider<
    LatestStartedRegistration, Registration>.internal(
  LatestStartedRegistration.new,
  name: r'latestStartedRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestStartedRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestStartedRegistration = AutoDisposeAsyncNotifier<Registration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
