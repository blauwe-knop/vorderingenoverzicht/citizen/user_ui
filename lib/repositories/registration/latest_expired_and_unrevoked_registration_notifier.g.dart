// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_expired_and_unrevoked_registration_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestExpiredAndUnrevokedRegistrationHash() =>
    r'5ea78e0ebf0bd6dac35a337ee06ef4a87f99e04f';

/// See also [LatestExpiredAndUnrevokedRegistration].
@ProviderFor(LatestExpiredAndUnrevokedRegistration)
final latestExpiredAndUnrevokedRegistrationProvider =
    AutoDisposeAsyncNotifierProvider<LatestExpiredAndUnrevokedRegistration,
        Registration>.internal(
  LatestExpiredAndUnrevokedRegistration.new,
  name: r'latestExpiredAndUnrevokedRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestExpiredAndUnrevokedRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestExpiredAndUnrevokedRegistration
    = AutoDisposeAsyncNotifier<Registration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
