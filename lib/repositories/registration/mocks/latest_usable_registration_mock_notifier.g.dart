// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_usable_registration_mock_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestUsableRegistrationMockHash() =>
    r'6f68502c43c26b532c7ae0dedfac84a83e872ee0';

/// See also [LatestUsableRegistrationMock].
@ProviderFor(LatestUsableRegistrationMock)
final latestUsableRegistrationMockProvider = AutoDisposeAsyncNotifierProvider<
    LatestUsableRegistrationMock, Registration>.internal(
  LatestUsableRegistrationMock.new,
  name: r'latestUsableRegistrationMockProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestUsableRegistrationMockHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LatestUsableRegistrationMock = AutoDisposeAsyncNotifier<Registration>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
