// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';

part 'latest_expired_and_unrevoked_registration_notifier.g.dart';

@riverpod
class LatestExpiredAndUnrevokedRegistration
    extends _$LatestExpiredAndUnrevokedRegistration {
  @override
  Future<Registration> build() async {
    final registrations =
        await ref.watch(registrationRepositoryProvider.future);

    var usableRegistrations = registrations
        .where((registration) => !registration.revoked && registration.expired);

    return usableRegistrations.last;
  }
}
