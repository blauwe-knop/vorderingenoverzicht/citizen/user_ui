// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';

class SchemeOrganizationRepositoryMock
    extends StateNotifier<List<SchemeOrganization>>
    implements SchemeOrganizationRepository {
  List<SchemeOrganization> listOfSchemeOrganization = [];

  SchemeOrganizationRepositoryMock() : super([]) {
    organizations.then((organizations) => state = organizations);
  }

  @override
  Future<void> add(
      {required String oin,
      required String name,
      required String publicKey,
      required String discoveryUrl,
      required bool available}) async {
    final SchemeOrganization schemeOrganization = SchemeOrganization(
      id: 1,
      oin: oin,
      name: name,
      publicKey: publicKey,
      discoveryUrl: discoveryUrl,
      available: available,
    );

    listOfSchemeOrganization.add(schemeOrganization);

    state = listOfSchemeOrganization;
  }

  @override
  Future<List<SchemeOrganization>> get organizations async {
    return listOfSchemeOrganization;
  }

  @override
  Future<void> update(
    SchemeOrganization organization,
  ) async {
    listOfSchemeOrganization[listOfSchemeOrganization
        .indexWhere((o) => o.id == organization.id)] = organization;
    state = listOfSchemeOrganization;
  }

  @override
  Future<void> clear() async {
    listOfSchemeOrganization = [];

    state = listOfSchemeOrganization;
  }
}

final schemeOrganizationRepositoryMockProvider = StateNotifierProvider<
    SchemeOrganizationRepository,
    List<SchemeOrganization>>((ref) => SchemeOrganizationRepositoryMock());
