// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_root_keypair.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appRootKeypairHash() => r'8110f934eccd4fa1df461ab2a0d8bb01d32410a3';

/// See also [AppRootKeypair].
@ProviderFor(AppRootKeypair)
final appRootKeypairProvider =
    AutoDisposeAsyncNotifierProvider<AppRootKeypair, bool>.internal(
  AppRootKeypair.new,
  name: r'appRootKeypairProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$appRootKeypairHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AppRootKeypair = AutoDisposeAsyncNotifier<bool>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
