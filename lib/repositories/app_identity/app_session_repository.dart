// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/app_session.dart';

part 'app_session_repository.g.dart';

@riverpod
class AppSessionRepository extends _$AppSessionRepository {
  Future<List<AppSession>> get _key async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var appSessionModelList =
        await database.select(database.appSessionTable).get();

    return appSessionModelList
        .map(
          (model) => AppSession(
            id: model.id,
            key: model.key,
            token: model.token,
            oin: model.oin,
          ),
        )
        .toList();
  }

  @override
  Future<List<AppSession>> build() {
    return _key;
  }

  Future<void> add({required String key, required String oin}) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.into(database.appSessionTable).insert(
          AppSessionTableCompanion.insert(
            key: key,
            token: '',
            oin: oin,
          ),
        );

    state = AsyncData(await _key);
  }

  Future<void> updateAppSession(AppSession session) async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    var appSessionModel = await (database.select(database.appSessionTable)
          ..where((table) => table.id.equals(session.id)))
        .getSingle();

    var appSessionUpdateModel = appSessionModel.copyWith(
      key: session.key,
      token: session.token,
    );

    await database
        .update(database.appSessionTable)
        .replace(appSessionUpdateModel);

    state = AsyncData(await _key);
  }

  Future<void> clear() async {
    // ignore: avoid_manual_providers_as_generated_provider_dependency
    var database = ref.read(encryptedDatabaseProvider);

    await database.delete(database.appSessionTable).go();

    state = AsyncData(await _key);
  }
}
