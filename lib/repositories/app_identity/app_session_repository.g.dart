// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_session_repository.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$appSessionRepositoryHash() =>
    r'63c64d20a9253a2a291651b94cf5ff3e4e4d9273';

/// See also [AppSessionRepository].
@ProviderFor(AppSessionRepository)
final appSessionRepositoryProvider = AutoDisposeAsyncNotifierProvider<
    AppSessionRepository, List<AppSession>>.internal(
  AppSessionRepository.new,
  name: r'appSessionRepositoryProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$appSessionRepositoryHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$AppSessionRepository = AutoDisposeAsyncNotifier<List<AppSession>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
