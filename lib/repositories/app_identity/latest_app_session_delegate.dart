// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/repositories/app_identity/app_session_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'latest_app_session_delegate.g.dart';

@riverpod
Future<AppSession> Function(Ref ref, String oin) latestAppSessionDelegate(
    Ref _) {
  return (ref, oin) async {
    final appSessions = await ref.watch(appSessionRepositoryProvider.future);

    if (appSessions.isEmpty) {
      ref
          .read(loggingProvider)
          .addLog(DeviceEvent.uu_95, "App session not found.");
      throw Exception("app session not found");
    }

    final organizationAppSessions =
        appSessions.where((session) => session.oin == oin);
    if (organizationAppSessions.isEmpty) {
      ref.read(loggingProvider).addLog(DeviceEvent.uu_101,
          "App session not found for organization with oid: $oin.");
      throw Exception("App session not found for organization with oid: $oin.");
    }

    return organizationAppSessions.last;
  };
}
