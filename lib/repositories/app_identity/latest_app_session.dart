// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';

part 'latest_app_session.g.dart';

@riverpod
Future<AppSession> latestAppSession(Ref ref, {required String oin}) async =>
    ref.watch(latestAppSessionDelegateProvider)(ref, oin);
