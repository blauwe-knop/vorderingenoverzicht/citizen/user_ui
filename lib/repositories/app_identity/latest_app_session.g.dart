// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_app_session.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestAppSessionHash() => r'8b9f4ef427c3efdac026e94b649dba48fdefbc57';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [latestAppSession].
@ProviderFor(latestAppSession)
const latestAppSessionProvider = LatestAppSessionFamily();

/// See also [latestAppSession].
class LatestAppSessionFamily extends Family<AsyncValue<AppSession>> {
  /// See also [latestAppSession].
  const LatestAppSessionFamily();

  /// See also [latestAppSession].
  LatestAppSessionProvider call({
    required String oin,
  }) {
    return LatestAppSessionProvider(
      oin: oin,
    );
  }

  @override
  LatestAppSessionProvider getProviderOverride(
    covariant LatestAppSessionProvider provider,
  ) {
    return call(
      oin: provider.oin,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'latestAppSessionProvider';
}

/// See also [latestAppSession].
class LatestAppSessionProvider extends AutoDisposeFutureProvider<AppSession> {
  /// See also [latestAppSession].
  LatestAppSessionProvider({
    required String oin,
  }) : this._internal(
          (ref) => latestAppSession(
            ref as LatestAppSessionRef,
            oin: oin,
          ),
          from: latestAppSessionProvider,
          name: r'latestAppSessionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$latestAppSessionHash,
          dependencies: LatestAppSessionFamily._dependencies,
          allTransitiveDependencies:
              LatestAppSessionFamily._allTransitiveDependencies,
          oin: oin,
        );

  LatestAppSessionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.oin,
  }) : super.internal();

  final String oin;

  @override
  Override overrideWith(
    FutureOr<AppSession> Function(LatestAppSessionRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: LatestAppSessionProvider._internal(
        (ref) => create(ref as LatestAppSessionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        oin: oin,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<AppSession> createElement() {
    return _LatestAppSessionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is LatestAppSessionProvider && other.oin == oin;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, oin.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin LatestAppSessionRef on AutoDisposeFutureProviderRef<AppSession> {
  /// The parameter `oin` of this provider.
  String get oin;
}

class _LatestAppSessionProviderElement
    extends AutoDisposeFutureProviderElement<AppSession>
    with LatestAppSessionRef {
  _LatestAppSessionProviderElement(super.provider);

  @override
  String get oin => (origin as LatestAppSessionProvider).oin;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
