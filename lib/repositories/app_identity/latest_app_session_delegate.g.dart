// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'latest_app_session_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$latestAppSessionDelegateHash() =>
    r'f1c4dcdc31b67f19400e6f2e180d126edd14ff6a';

/// See also [latestAppSessionDelegate].
@ProviderFor(latestAppSessionDelegate)
final latestAppSessionDelegateProvider = AutoDisposeProvider<
    Future<AppSession> Function(Ref ref, String oin)>.internal(
  latestAppSessionDelegate,
  name: r'latestAppSessionDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$latestAppSessionDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef LatestAppSessionDelegateRef
    = AutoDisposeProviderRef<Future<AppSession> Function(Ref ref, String oin)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
