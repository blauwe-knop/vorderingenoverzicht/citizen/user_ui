// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'logs_sorted_by_time_stamp_notifier.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$logsSortedByTimeStampHash() =>
    r'bb1410ddcf1d2198f547dc6b267884aa4e2d4e43';

/// See also [LogsSortedByTimeStamp].
@ProviderFor(LogsSortedByTimeStamp)
final logsSortedByTimeStampProvider = AutoDisposeAsyncNotifierProvider<
    LogsSortedByTimeStamp, List<LogRecord>>.internal(
  LogsSortedByTimeStamp.new,
  name: r'logsSortedByTimeStampProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$logsSortedByTimeStampHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$LogsSortedByTimeStamp = AutoDisposeAsyncNotifier<List<LogRecord>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
