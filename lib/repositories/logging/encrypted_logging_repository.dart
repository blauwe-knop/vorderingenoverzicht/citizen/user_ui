import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';
import 'package:user_ui/entities/log_record.dart';

import 'logging_repository.dart';

class EncryptedLoggingRepository extends StateNotifier<List<LogRecord>>
    implements LoggingRepository {
  final EncryptedDatabase database;

  EncryptedLoggingRepository(this.database) : super([]) {
    logs.then((logs) => state = logs);
  }

  @override
  Future<void> add(LogRecord log) async {
    await database.into(database.logRecordTable).insert(
          LogRecordTableCompanion.insert(
            timestamp: log.timestamp,
            host: log.host,
            cef: log.cef,
            deviceVendor: log.deviceVendor,
            deviceProduct: log.deviceProduct,
            deviceVersion: log.deviceVersion,
            deviceEventClassId: log.deviceEventClassId,
            name: log.name,
            severity: log.severity,
            flexString1Label: log.flexString1Label,
            flexString1: log.flexString1,
            flexString2Label: log.flexString2Label,
            flexString2: log.flexString2,
            act: log.act,
            app: log.app,
            request: log.request,
            requestMethod: log.requestMethod,
          ),
        );

    state = await logs;
  }

  @override
  Future<List<LogRecord>> get logs async {
    List<LogRecord> entries = [];
    await database
        .select(database.logRecordTable)
        .get()
        .then((logRecordsList) => {
              for (var logRecord in logRecordsList)
                {
                  entries.add(LogRecord(
                      id: logRecord.id,
                      timestamp: logRecord.timestamp,
                      host: logRecord.host,
                      cef: logRecord.cef,
                      deviceVendor: logRecord.deviceVendor,
                      deviceProduct: logRecord.deviceProduct,
                      deviceVersion: logRecord.deviceVersion,
                      deviceEventClassId: logRecord.deviceEventClassId,
                      severity: logRecord.severity,
                      flexString1Label: logRecord.flexString1Label,
                      flexString1: logRecord.flexString1,
                      flexString2Label: logRecord.flexString2Label,
                      flexString2: logRecord.flexString2,
                      act: logRecord.act,
                      app: logRecord.app,
                      request: logRecord.request,
                      requestMethod: logRecord.requestMethod,
                      name: logRecord.name))
                }
            });
    entries.sort((a, b) => b.timestamp.compareTo(a.timestamp));
    return entries;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.logRecordTable).go();

    state = await logs;
  }

  Future<void> addBulk(List<LogRecord> logRecords) async {
    await database.transaction(() async {
      for (var log in logRecords) {
        await database.into(database.logRecordTable).insert(
              LogRecordTableCompanion.insert(
                timestamp: log.timestamp,
                host: log.host,
                cef: log.cef,
                deviceVendor: log.deviceVendor,
                deviceProduct: log.deviceProduct,
                deviceVersion: log.deviceVersion,
                deviceEventClassId: log.deviceEventClassId,
                name: log.name,
                severity: log.severity,
                flexString1Label: log.flexString1Label,
                flexString1: log.flexString1,
                flexString2Label: log.flexString2Label,
                flexString2: log.flexString2,
                act: log.act,
                app: log.app,
                request: log.request,
                requestMethod: log.requestMethod,
              ),
            );
      }
    });

    state = await logs;
  }
}

final encryptedLoggingRepositoryProvider =
    StateNotifierProvider<EncryptedLoggingRepository, List<LogRecord>>((ref) {
  final encryptedDatabase = ref.watch(encryptedDatabaseProvider);
  return EncryptedLoggingRepository(encryptedDatabase);
});
