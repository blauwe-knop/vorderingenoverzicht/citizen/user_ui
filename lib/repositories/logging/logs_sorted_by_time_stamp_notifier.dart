import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/log_record.dart';
import 'package:user_ui/repositories/logging/encrypted_logging_repository.dart';
import 'package:user_ui/repositories/logging/event_severity_level.dart';

part 'logs_sorted_by_time_stamp_notifier.g.dart';

@riverpod
class LogsSortedByTimeStamp extends _$LogsSortedByTimeStamp {
  final Map<int, bool> _filters = {};

  Future<List<LogRecord>> get _list async {
    final logs = ref.read(encryptedLoggingRepositoryProvider);

    // Apply filters
    final filteredLogs = logs.where((log) {
      return _filters[log.severity] ?? false;
    }).toList();

    filteredLogs.sort((a, b) => b.timestamp.compareTo(a.timestamp));
    return filteredLogs;
  }

  @override
  Future<List<LogRecord>> build() async {
    initFilters();
    return await _list;
  }

  Future<void> initFilters() async {
    for (var level in EventSeverityLevel.values) {
      _filters[level.toInt()] = true;
    }
    state = AsyncValue.data(await _list);
  }

  Future<void> toggleFilter(EventSeverityLevel level) async {
    _filters[level.toInt()] = !_filters[level.toInt()]!;
    state = AsyncValue.data(await _list);
  }

  Future<void> resetFilters() async {
    _filters.forEach((key, _) => _filters[key] = true);
    state = AsyncValue.data(await _list);
  }

  bool filterEnabled(EventSeverityLevel severity) {
    int severityInt = severity.toInt();
    return _filters.containsKey(severityInt) ? _filters[severityInt]! : false;
  }
}
