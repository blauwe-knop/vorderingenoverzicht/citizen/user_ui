import 'package:drift/drift.dart';

@DataClassName('SchemeAppManagerDriftModel')
class SchemeAppManagerTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  TextColumn get name => text()();
  TextColumn get publicKey => text()();
  TextColumn get discoveryUrl => text()();
  BoolColumn get available => boolean()();
}
