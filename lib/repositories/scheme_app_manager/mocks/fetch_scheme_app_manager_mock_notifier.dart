// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/scheme_app_manager.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';

part 'fetch_scheme_app_manager_mock_notifier.g.dart';

@riverpod
class FetchSchemeAppManagerMock extends _$FetchSchemeAppManagerMock
    implements FetchSchemeAppManager {
  @override
  Future<SchemeAppManager> build() async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));

    return SchemeAppManager(
      id: 1,
      oin: 'oin',
      name: 'name',
      publicKey: keys["publicKey"],
      discoveryUrl: 'discoveryUrl',
      available: true,
    );
  }
}
