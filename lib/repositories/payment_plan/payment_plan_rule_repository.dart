import 'package:drift/drift.dart';
import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';

abstract class PaymentPlanRuleRepository
    implements StateNotifier<List<PaymentPlanRule>> {
  Future<void> addRule(PaymentPlanRule paymentPlanRule);
  Future<List<PaymentPlanRule>> get rules;
  Future<void> clear();
  Future<void> addRules(List<PaymentPlanRule> paymentPlanRules);
}

class DriftPaymentPlanRuleRepository
    extends StateNotifier<List<PaymentPlanRule>>
    implements PaymentPlanRuleRepository {
  final EncryptedDatabase database;

  DriftPaymentPlanRuleRepository(this.database) : super([]) {
    rules.then((rules) => state = rules);
  }

  @override
  Future<void> addRule(PaymentPlanRule paymentPlanRule) async {
    await database.into(database.paymentPlanRulesStorageTable).insert(
        PaymentPlanRulesStorageTableCompanion.insert(
            id: paymentPlanRule.id,
            organisaties: paymentPlanRule.organisaties,
            minimumBedrag: paymentPlanRule.maximumBedrag,
            maximumBedrag: paymentPlanRule.maximumBedrag,
            type: paymentPlanRule.type));
  }

  @override
  Future<void> clear() async {
    await database.delete(database.paymentPlanRulesStorageTable).go();
    state = await rules;
  }

  @override
  Future<List<PaymentPlanRule>> get rules async {
    var rulesList =
        await database.select(database.paymentPlanRulesStorageTable).get();
    return rulesList
        .map(
          (rule) => PaymentPlanRule(
            id: rule.id,
            organisaties: rule.organisaties,
            minimumBedrag: rule.minimumBedrag,
            maximumBedrag: rule.maximumBedrag,
            type: rule.type,
          ),
        )
        .toList();
  }

  @override
  Future<void> addRules(List<PaymentPlanRule> paymentPlanRules) async {
    for (PaymentPlanRule rule in paymentPlanRules) {
      await database.into(database.paymentPlanRulesStorageTable).insert(
          PaymentPlanRulesStorageTableCompanion.insert(
              id: rule.id,
              organisaties: rule.organisaties,
              minimumBedrag: rule.minimumBedrag,
              maximumBedrag: rule.maximumBedrag,
              type: rule.type),
          mode: InsertMode.insertOrReplace);
    }
  }
}

final paymentPlanRuleRepositoryProvider =
    StateNotifierProvider<PaymentPlanRuleRepository, List<PaymentPlanRule>>(
        (ref) {
  final database = ref.watch(encryptedDatabaseProvider);
  return DriftPaymentPlanRuleRepository(database);
});
