import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/database/encrypted_database.dart';

abstract class PaymentPlanGroupRepository
    implements StateNotifier<List<PaymentPlanGroup>> {
  Future<void> addGroup(PaymentPlanGroup paymentPlanGroup);
  Future<List<PaymentPlanGroup>> get groups;
  Future<void> clear();
}

class DriftPaymentPlanGroupRepository
    extends StateNotifier<List<PaymentPlanGroup>>
    implements PaymentPlanGroupRepository {
  final EncryptedDatabase database;

  DriftPaymentPlanGroupRepository(this.database) : super([]) {
    groups.then((groups) => state = groups);
  }

  @override
  Future<void> addGroup(PaymentPlanGroup paymentPlanGroup) async {
    await database.into(database.paymentPlanGroupsTable).insert(
        PaymentPlanGroupsTableCompanion.insert(
            zaakKenmerken: paymentPlanGroup.zaakkenmerk.join(','),
            saldo: paymentPlanGroup.saldo,
            termijnen: paymentPlanGroup.termijnen,
            paymentRules: paymentPlanGroup.betaalRegels));
    state = await groups;
  }

  @override
  Future<void> clear() async {
    await database.delete(database.paymentPlanGroupsTable).go();
    state = await groups;
  }

  @override
  Future<List<PaymentPlanGroup>> get groups async {
    var paymentPlanGroupsList =
        await database.select(database.paymentPlanGroupsTable).get();

    return paymentPlanGroupsList
        .map(
          (PaymentPlanGroupsModel group) => PaymentPlanGroup(
            id: group.id,
            zaakkenmerk: group.zaakKenmerken.split(','),
            saldo: group.saldo,
            termijnen: group.termijnen,
            betaalRegels: group.paymentRules,
          ),
        )
        .toList();
  }
}

final paymentPlanGroupRepositoryProvider =
    StateNotifierProvider<PaymentPlanGroupRepository, List<PaymentPlanGroup>>(
        (ref) {
  final database = ref.watch(encryptedDatabaseProvider);
  return DriftPaymentPlanGroupRepository(database);
});
