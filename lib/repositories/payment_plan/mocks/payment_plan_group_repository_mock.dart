import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/repositories/payment_plan/payment_plan_group_repository.dart';

class DriftPaymentPlanGroupRepositoryMock
    extends StateNotifier<List<PaymentPlanGroup>>
    implements PaymentPlanGroupRepository {
  List<PaymentPlanGroup> listOfGroups = [];
  DriftPaymentPlanGroupRepositoryMock() : super([]) {
    groups.then((groups) => state = groups);
  }

  @override
  Future<void> addGroup(PaymentPlanGroup paymentPlanGroup) async {
    listOfGroups.add(paymentPlanGroup);
    state = listOfGroups;
  }

  @override
  Future<void> clear() async {
    listOfGroups = [];
    state = listOfGroups;
  }

  @override
  Future<List<PaymentPlanGroup>> get groups async {
    return listOfGroups;
  }
}

final paymentPlanGroupRepositoryMockProvider =
    StateNotifierProvider<PaymentPlanGroupRepository, List<PaymentPlanGroup>>(
        (ref) {
  return DriftPaymentPlanGroupRepositoryMock();
});
