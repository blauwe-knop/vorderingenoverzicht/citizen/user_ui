import 'package:drift/drift.dart';

@DataClassName('FinancialClaimsInformationConfigurationDriftModel')
class FinancialClaimsInformationConfigurationTable extends Table {
  IntColumn get id => integer().autoIncrement()();
  TextColumn get oin => text()();
  TextColumn get document => text().nullable()();
  TextColumn get documentSignature => text().nullable()();
  TextColumn get envelope => text().nullable()();
  TextColumn get encryptedEnvelope => text().nullable()();
  TextColumn get configuration => text().nullable()();
  BoolColumn get expired => boolean()();
}
