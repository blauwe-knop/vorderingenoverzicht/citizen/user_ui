// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'scheme_document_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SchemeDocumentType {
  int get id => throw _privateConstructorUsedError;
  String get name => throw _privateConstructorUsedError;
  bool get available => throw _privateConstructorUsedError;

  /// Create a copy of SchemeDocumentType
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $SchemeDocumentTypeCopyWith<SchemeDocumentType> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SchemeDocumentTypeCopyWith<$Res> {
  factory $SchemeDocumentTypeCopyWith(
          SchemeDocumentType value, $Res Function(SchemeDocumentType) then) =
      _$SchemeDocumentTypeCopyWithImpl<$Res, SchemeDocumentType>;
  @useResult
  $Res call({int id, String name, bool available});
}

/// @nodoc
class _$SchemeDocumentTypeCopyWithImpl<$Res, $Val extends SchemeDocumentType>
    implements $SchemeDocumentTypeCopyWith<$Res> {
  _$SchemeDocumentTypeCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of SchemeDocumentType
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? available = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      available: null == available
          ? _value.available
          : available // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SchemeDocumentTypeImplCopyWith<$Res>
    implements $SchemeDocumentTypeCopyWith<$Res> {
  factory _$$SchemeDocumentTypeImplCopyWith(_$SchemeDocumentTypeImpl value,
          $Res Function(_$SchemeDocumentTypeImpl) then) =
      __$$SchemeDocumentTypeImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String name, bool available});
}

/// @nodoc
class __$$SchemeDocumentTypeImplCopyWithImpl<$Res>
    extends _$SchemeDocumentTypeCopyWithImpl<$Res, _$SchemeDocumentTypeImpl>
    implements _$$SchemeDocumentTypeImplCopyWith<$Res> {
  __$$SchemeDocumentTypeImplCopyWithImpl(_$SchemeDocumentTypeImpl _value,
      $Res Function(_$SchemeDocumentTypeImpl) _then)
      : super(_value, _then);

  /// Create a copy of SchemeDocumentType
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? name = null,
    Object? available = null,
  }) {
    return _then(_$SchemeDocumentTypeImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      available: null == available
          ? _value.available
          : available // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SchemeDocumentTypeImpl
    with DiagnosticableTreeMixin
    implements _SchemeDocumentType {
  const _$SchemeDocumentTypeImpl(
      {required this.id, required this.name, required this.available});

  @override
  final int id;
  @override
  final String name;
  @override
  final bool available;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'SchemeDocumentType(id: $id, name: $name, available: $available)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'SchemeDocumentType'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('name', name))
      ..add(DiagnosticsProperty('available', available));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SchemeDocumentTypeImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.available, available) ||
                other.available == available));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, name, available);

  /// Create a copy of SchemeDocumentType
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$SchemeDocumentTypeImplCopyWith<_$SchemeDocumentTypeImpl> get copyWith =>
      __$$SchemeDocumentTypeImplCopyWithImpl<_$SchemeDocumentTypeImpl>(
          this, _$identity);
}

abstract class _SchemeDocumentType implements SchemeDocumentType {
  const factory _SchemeDocumentType(
      {required final int id,
      required final String name,
      required final bool available}) = _$SchemeDocumentTypeImpl;

  @override
  int get id;
  @override
  String get name;
  @override
  bool get available;

  /// Create a copy of SchemeDocumentType
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$SchemeDocumentTypeImplCopyWith<_$SchemeDocumentTypeImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
