// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_key_pair.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppKeyPair {
  int get id => throw _privateConstructorUsedError;
  String get publicKey => throw _privateConstructorUsedError;
  String get privateKey => throw _privateConstructorUsedError;

  /// Create a copy of AppKeyPair
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AppKeyPairCopyWith<AppKeyPair> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppKeyPairCopyWith<$Res> {
  factory $AppKeyPairCopyWith(
          AppKeyPair value, $Res Function(AppKeyPair) then) =
      _$AppKeyPairCopyWithImpl<$Res, AppKeyPair>;
  @useResult
  $Res call({int id, String publicKey, String privateKey});
}

/// @nodoc
class _$AppKeyPairCopyWithImpl<$Res, $Val extends AppKeyPair>
    implements $AppKeyPairCopyWith<$Res> {
  _$AppKeyPairCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of AppKeyPair
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? publicKey = null,
    Object? privateKey = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      publicKey: null == publicKey
          ? _value.publicKey
          : publicKey // ignore: cast_nullable_to_non_nullable
              as String,
      privateKey: null == privateKey
          ? _value.privateKey
          : privateKey // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppKeyPairImplCopyWith<$Res>
    implements $AppKeyPairCopyWith<$Res> {
  factory _$$AppKeyPairImplCopyWith(
          _$AppKeyPairImpl value, $Res Function(_$AppKeyPairImpl) then) =
      __$$AppKeyPairImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String publicKey, String privateKey});
}

/// @nodoc
class __$$AppKeyPairImplCopyWithImpl<$Res>
    extends _$AppKeyPairCopyWithImpl<$Res, _$AppKeyPairImpl>
    implements _$$AppKeyPairImplCopyWith<$Res> {
  __$$AppKeyPairImplCopyWithImpl(
      _$AppKeyPairImpl _value, $Res Function(_$AppKeyPairImpl) _then)
      : super(_value, _then);

  /// Create a copy of AppKeyPair
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? publicKey = null,
    Object? privateKey = null,
  }) {
    return _then(_$AppKeyPairImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      publicKey: null == publicKey
          ? _value.publicKey
          : publicKey // ignore: cast_nullable_to_non_nullable
              as String,
      privateKey: null == privateKey
          ? _value.privateKey
          : privateKey // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AppKeyPairImpl with DiagnosticableTreeMixin implements _AppKeyPair {
  const _$AppKeyPairImpl(
      {required this.id, required this.publicKey, required this.privateKey});

  @override
  final int id;
  @override
  final String publicKey;
  @override
  final String privateKey;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'AppKeyPair(id: $id, publicKey: $publicKey, privateKey: $privateKey)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'AppKeyPair'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('publicKey', publicKey))
      ..add(DiagnosticsProperty('privateKey', privateKey));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppKeyPairImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.publicKey, publicKey) ||
                other.publicKey == publicKey) &&
            (identical(other.privateKey, privateKey) ||
                other.privateKey == privateKey));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, publicKey, privateKey);

  /// Create a copy of AppKeyPair
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AppKeyPairImplCopyWith<_$AppKeyPairImpl> get copyWith =>
      __$$AppKeyPairImplCopyWithImpl<_$AppKeyPairImpl>(this, _$identity);
}

abstract class _AppKeyPair implements AppKeyPair {
  const factory _AppKeyPair(
      {required final int id,
      required final String publicKey,
      required final String privateKey}) = _$AppKeyPairImpl;

  @override
  int get id;
  @override
  String get publicKey;
  @override
  String get privateKey;

  /// Create a copy of AppKeyPair
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AppKeyPairImplCopyWith<_$AppKeyPairImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
