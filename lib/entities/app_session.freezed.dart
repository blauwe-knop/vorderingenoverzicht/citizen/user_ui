// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'app_session.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$AppSession {
  int get id => throw _privateConstructorUsedError;
  String get key => throw _privateConstructorUsedError;
  String get token => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;

  /// Create a copy of AppSession
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $AppSessionCopyWith<AppSession> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AppSessionCopyWith<$Res> {
  factory $AppSessionCopyWith(
          AppSession value, $Res Function(AppSession) then) =
      _$AppSessionCopyWithImpl<$Res, AppSession>;
  @useResult
  $Res call({int id, String key, String token, String oin});
}

/// @nodoc
class _$AppSessionCopyWithImpl<$Res, $Val extends AppSession>
    implements $AppSessionCopyWith<$Res> {
  _$AppSessionCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of AppSession
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? token = null,
    Object? oin = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$AppSessionImplCopyWith<$Res>
    implements $AppSessionCopyWith<$Res> {
  factory _$$AppSessionImplCopyWith(
          _$AppSessionImpl value, $Res Function(_$AppSessionImpl) then) =
      __$$AppSessionImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int id, String key, String token, String oin});
}

/// @nodoc
class __$$AppSessionImplCopyWithImpl<$Res>
    extends _$AppSessionCopyWithImpl<$Res, _$AppSessionImpl>
    implements _$$AppSessionImplCopyWith<$Res> {
  __$$AppSessionImplCopyWithImpl(
      _$AppSessionImpl _value, $Res Function(_$AppSessionImpl) _then)
      : super(_value, _then);

  /// Create a copy of AppSession
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? key = null,
    Object? token = null,
    Object? oin = null,
  }) {
    return _then(_$AppSessionImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      key: null == key
          ? _value.key
          : key // ignore: cast_nullable_to_non_nullable
              as String,
      token: null == token
          ? _value.token
          : token // ignore: cast_nullable_to_non_nullable
              as String,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$AppSessionImpl implements _AppSession {
  const _$AppSessionImpl(
      {required this.id,
      required this.key,
      required this.token,
      required this.oin});

  @override
  final int id;
  @override
  final String key;
  @override
  final String token;
  @override
  final String oin;

  @override
  String toString() {
    return 'AppSession(id: $id, key: $key, token: $token, oin: $oin)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$AppSessionImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.key, key) || other.key == key) &&
            (identical(other.token, token) || other.token == token) &&
            (identical(other.oin, oin) || other.oin == oin));
  }

  @override
  int get hashCode => Object.hash(runtimeType, id, key, token, oin);

  /// Create a copy of AppSession
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$AppSessionImplCopyWith<_$AppSessionImpl> get copyWith =>
      __$$AppSessionImplCopyWithImpl<_$AppSessionImpl>(this, _$identity);
}

abstract class _AppSession implements AppSession {
  const factory _AppSession(
      {required final int id,
      required final String key,
      required final String token,
      required final String oin}) = _$AppSessionImpl;

  @override
  int get id;
  @override
  String get key;
  @override
  String get token;
  @override
  String get oin;

  /// Create a copy of AppSession
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$AppSessionImplCopyWith<_$AppSessionImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
