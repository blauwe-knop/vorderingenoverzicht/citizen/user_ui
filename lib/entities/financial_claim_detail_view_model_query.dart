// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:freezed_annotation/freezed_annotation.dart';

part 'financial_claim_detail_view_model_query.freezed.dart';

@freezed
class FinancialClaimDetailViewModelQuery
    with _$FinancialClaimDetailViewModelQuery {
  const factory FinancialClaimDetailViewModelQuery({
    required int financialClaimsInformationStorageId,
    required String zaakkenmerk,
  }) = _FinancialClaimDetailViewModelQuery;
}
