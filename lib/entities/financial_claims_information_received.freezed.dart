// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claims_information_received.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$FinancialClaimsInformationReceived {
  int get id => throw _privateConstructorUsedError;
  String get oin => throw _privateConstructorUsedError;
  DateTime get dateTimeRequested => throw _privateConstructorUsedError;
  DateTime get dateTimeReceived => throw _privateConstructorUsedError;
  String get encryptedFinancialClaimsInformationDocument =>
      throw _privateConstructorUsedError;
  String? get financialClaimsInformationDocument =>
      throw _privateConstructorUsedError;
  bool get copiedToInbox => throw _privateConstructorUsedError;
  bool? get changed => throw _privateConstructorUsedError;

  /// Create a copy of FinancialClaimsInformationReceived
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $FinancialClaimsInformationReceivedCopyWith<
          FinancialClaimsInformationReceived>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimsInformationReceivedCopyWith<$Res> {
  factory $FinancialClaimsInformationReceivedCopyWith(
          FinancialClaimsInformationReceived value,
          $Res Function(FinancialClaimsInformationReceived) then) =
      _$FinancialClaimsInformationReceivedCopyWithImpl<$Res,
          FinancialClaimsInformationReceived>;
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String encryptedFinancialClaimsInformationDocument,
      String? financialClaimsInformationDocument,
      bool copiedToInbox,
      bool? changed});
}

/// @nodoc
class _$FinancialClaimsInformationReceivedCopyWithImpl<$Res,
        $Val extends FinancialClaimsInformationReceived>
    implements $FinancialClaimsInformationReceivedCopyWith<$Res> {
  _$FinancialClaimsInformationReceivedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of FinancialClaimsInformationReceived
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? encryptedFinancialClaimsInformationDocument = null,
    Object? financialClaimsInformationDocument = freezed,
    Object? copiedToInbox = null,
    Object? changed = freezed,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      encryptedFinancialClaimsInformationDocument: null ==
              encryptedFinancialClaimsInformationDocument
          ? _value.encryptedFinancialClaimsInformationDocument
          : encryptedFinancialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
      financialClaimsInformationDocument: freezed ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String?,
      copiedToInbox: null == copiedToInbox
          ? _value.copiedToInbox
          : copiedToInbox // ignore: cast_nullable_to_non_nullable
              as bool,
      changed: freezed == changed
          ? _value.changed
          : changed // ignore: cast_nullable_to_non_nullable
              as bool?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimsInformationReceivedImplCopyWith<$Res>
    implements $FinancialClaimsInformationReceivedCopyWith<$Res> {
  factory _$$FinancialClaimsInformationReceivedImplCopyWith(
          _$FinancialClaimsInformationReceivedImpl value,
          $Res Function(_$FinancialClaimsInformationReceivedImpl) then) =
      __$$FinancialClaimsInformationReceivedImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      String oin,
      DateTime dateTimeRequested,
      DateTime dateTimeReceived,
      String encryptedFinancialClaimsInformationDocument,
      String? financialClaimsInformationDocument,
      bool copiedToInbox,
      bool? changed});
}

/// @nodoc
class __$$FinancialClaimsInformationReceivedImplCopyWithImpl<$Res>
    extends _$FinancialClaimsInformationReceivedCopyWithImpl<$Res,
        _$FinancialClaimsInformationReceivedImpl>
    implements _$$FinancialClaimsInformationReceivedImplCopyWith<$Res> {
  __$$FinancialClaimsInformationReceivedImplCopyWithImpl(
      _$FinancialClaimsInformationReceivedImpl _value,
      $Res Function(_$FinancialClaimsInformationReceivedImpl) _then)
      : super(_value, _then);

  /// Create a copy of FinancialClaimsInformationReceived
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? oin = null,
    Object? dateTimeRequested = null,
    Object? dateTimeReceived = null,
    Object? encryptedFinancialClaimsInformationDocument = null,
    Object? financialClaimsInformationDocument = freezed,
    Object? copiedToInbox = null,
    Object? changed = freezed,
  }) {
    return _then(_$FinancialClaimsInformationReceivedImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      oin: null == oin
          ? _value.oin
          : oin // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeRequested: null == dateTimeRequested
          ? _value.dateTimeRequested
          : dateTimeRequested // ignore: cast_nullable_to_non_nullable
              as DateTime,
      dateTimeReceived: null == dateTimeReceived
          ? _value.dateTimeReceived
          : dateTimeReceived // ignore: cast_nullable_to_non_nullable
              as DateTime,
      encryptedFinancialClaimsInformationDocument: null ==
              encryptedFinancialClaimsInformationDocument
          ? _value.encryptedFinancialClaimsInformationDocument
          : encryptedFinancialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String,
      financialClaimsInformationDocument: freezed ==
              financialClaimsInformationDocument
          ? _value.financialClaimsInformationDocument
          : financialClaimsInformationDocument // ignore: cast_nullable_to_non_nullable
              as String?,
      copiedToInbox: null == copiedToInbox
          ? _value.copiedToInbox
          : copiedToInbox // ignore: cast_nullable_to_non_nullable
              as bool,
      changed: freezed == changed
          ? _value.changed
          : changed // ignore: cast_nullable_to_non_nullable
              as bool?,
    ));
  }
}

/// @nodoc

class _$FinancialClaimsInformationReceivedImpl
    with DiagnosticableTreeMixin
    implements _FinancialClaimsInformationReceived {
  const _$FinancialClaimsInformationReceivedImpl(
      {required this.id,
      required this.oin,
      required this.dateTimeRequested,
      required this.dateTimeReceived,
      required this.encryptedFinancialClaimsInformationDocument,
      required this.financialClaimsInformationDocument,
      required this.copiedToInbox,
      required this.changed});

  @override
  final int id;
  @override
  final String oin;
  @override
  final DateTime dateTimeRequested;
  @override
  final DateTime dateTimeReceived;
  @override
  final String encryptedFinancialClaimsInformationDocument;
  @override
  final String? financialClaimsInformationDocument;
  @override
  final bool copiedToInbox;
  @override
  final bool? changed;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'FinancialClaimsInformationReceived(id: $id, oin: $oin, dateTimeRequested: $dateTimeRequested, dateTimeReceived: $dateTimeReceived, encryptedFinancialClaimsInformationDocument: $encryptedFinancialClaimsInformationDocument, financialClaimsInformationDocument: $financialClaimsInformationDocument, copiedToInbox: $copiedToInbox, changed: $changed)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'FinancialClaimsInformationReceived'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('oin', oin))
      ..add(DiagnosticsProperty('dateTimeRequested', dateTimeRequested))
      ..add(DiagnosticsProperty('dateTimeReceived', dateTimeReceived))
      ..add(DiagnosticsProperty('encryptedFinancialClaimsInformationDocument',
          encryptedFinancialClaimsInformationDocument))
      ..add(DiagnosticsProperty('financialClaimsInformationDocument',
          financialClaimsInformationDocument))
      ..add(DiagnosticsProperty('copiedToInbox', copiedToInbox))
      ..add(DiagnosticsProperty('changed', changed));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimsInformationReceivedImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.oin, oin) || other.oin == oin) &&
            (identical(other.dateTimeRequested, dateTimeRequested) ||
                other.dateTimeRequested == dateTimeRequested) &&
            (identical(other.dateTimeReceived, dateTimeReceived) ||
                other.dateTimeReceived == dateTimeReceived) &&
            (identical(other.encryptedFinancialClaimsInformationDocument,
                    encryptedFinancialClaimsInformationDocument) ||
                other.encryptedFinancialClaimsInformationDocument ==
                    encryptedFinancialClaimsInformationDocument) &&
            (identical(other.financialClaimsInformationDocument,
                    financialClaimsInformationDocument) ||
                other.financialClaimsInformationDocument ==
                    financialClaimsInformationDocument) &&
            (identical(other.copiedToInbox, copiedToInbox) ||
                other.copiedToInbox == copiedToInbox) &&
            (identical(other.changed, changed) || other.changed == changed));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      oin,
      dateTimeRequested,
      dateTimeReceived,
      encryptedFinancialClaimsInformationDocument,
      financialClaimsInformationDocument,
      copiedToInbox,
      changed);

  /// Create a copy of FinancialClaimsInformationReceived
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimsInformationReceivedImplCopyWith<
          _$FinancialClaimsInformationReceivedImpl>
      get copyWith => __$$FinancialClaimsInformationReceivedImplCopyWithImpl<
          _$FinancialClaimsInformationReceivedImpl>(this, _$identity);
}

abstract class _FinancialClaimsInformationReceived
    implements FinancialClaimsInformationReceived {
  const factory _FinancialClaimsInformationReceived(
      {required final int id,
      required final String oin,
      required final DateTime dateTimeRequested,
      required final DateTime dateTimeReceived,
      required final String encryptedFinancialClaimsInformationDocument,
      required final String? financialClaimsInformationDocument,
      required final bool copiedToInbox,
      required final bool? changed}) = _$FinancialClaimsInformationReceivedImpl;

  @override
  int get id;
  @override
  String get oin;
  @override
  DateTime get dateTimeRequested;
  @override
  DateTime get dateTimeReceived;
  @override
  String get encryptedFinancialClaimsInformationDocument;
  @override
  String? get financialClaimsInformationDocument;
  @override
  bool get copiedToInbox;
  @override
  bool? get changed;

  /// Create a copy of FinancialClaimsInformationReceived
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$FinancialClaimsInformationReceivedImplCopyWith<
          _$FinancialClaimsInformationReceivedImpl>
      get copyWith => throw _privateConstructorUsedError;
}
