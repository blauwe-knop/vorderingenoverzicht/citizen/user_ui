// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_key_pair.freezed.dart';

@freezed
class AppKeyPair with _$AppKeyPair {
  const factory AppKeyPair({
    required int id,
    required String publicKey,
    required String privateKey,
  }) = _AppKeyPair;
}
