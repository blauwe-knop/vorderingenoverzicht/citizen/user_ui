// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'user_settings.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$UserSettings {
  bool get hasSeenOnboarding => throw _privateConstructorUsedError;
  bool get autoSelectAllOrganizations => throw _privateConstructorUsedError;
  bool get hasCompletedRegistration => throw _privateConstructorUsedError;
  bool get hasCompletedOrganizationSelection =>
      throw _privateConstructorUsedError;
  bool get delay => throw _privateConstructorUsedError;
  bool get showDevelopmentOverlay => throw _privateConstructorUsedError;
  bool get showCriticalErrorAlerts => throw _privateConstructorUsedError;
  String get pincode => throw _privateConstructorUsedError;
  bool get betalingsregelingRijkFeature => throw _privateConstructorUsedError;

  /// Create a copy of UserSettings
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UserSettingsCopyWith<UserSettings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserSettingsCopyWith<$Res> {
  factory $UserSettingsCopyWith(
          UserSettings value, $Res Function(UserSettings) then) =
      _$UserSettingsCopyWithImpl<$Res, UserSettings>;
  @useResult
  $Res call(
      {bool hasSeenOnboarding,
      bool autoSelectAllOrganizations,
      bool hasCompletedRegistration,
      bool hasCompletedOrganizationSelection,
      bool delay,
      bool showDevelopmentOverlay,
      bool showCriticalErrorAlerts,
      String pincode,
      bool betalingsregelingRijkFeature});
}

/// @nodoc
class _$UserSettingsCopyWithImpl<$Res, $Val extends UserSettings>
    implements $UserSettingsCopyWith<$Res> {
  _$UserSettingsCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of UserSettings
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hasSeenOnboarding = null,
    Object? autoSelectAllOrganizations = null,
    Object? hasCompletedRegistration = null,
    Object? hasCompletedOrganizationSelection = null,
    Object? delay = null,
    Object? showDevelopmentOverlay = null,
    Object? showCriticalErrorAlerts = null,
    Object? pincode = null,
    Object? betalingsregelingRijkFeature = null,
  }) {
    return _then(_value.copyWith(
      hasSeenOnboarding: null == hasSeenOnboarding
          ? _value.hasSeenOnboarding
          : hasSeenOnboarding // ignore: cast_nullable_to_non_nullable
              as bool,
      autoSelectAllOrganizations: null == autoSelectAllOrganizations
          ? _value.autoSelectAllOrganizations
          : autoSelectAllOrganizations // ignore: cast_nullable_to_non_nullable
              as bool,
      hasCompletedRegistration: null == hasCompletedRegistration
          ? _value.hasCompletedRegistration
          : hasCompletedRegistration // ignore: cast_nullable_to_non_nullable
              as bool,
      hasCompletedOrganizationSelection: null ==
              hasCompletedOrganizationSelection
          ? _value.hasCompletedOrganizationSelection
          : hasCompletedOrganizationSelection // ignore: cast_nullable_to_non_nullable
              as bool,
      delay: null == delay
          ? _value.delay
          : delay // ignore: cast_nullable_to_non_nullable
              as bool,
      showDevelopmentOverlay: null == showDevelopmentOverlay
          ? _value.showDevelopmentOverlay
          : showDevelopmentOverlay // ignore: cast_nullable_to_non_nullable
              as bool,
      showCriticalErrorAlerts: null == showCriticalErrorAlerts
          ? _value.showCriticalErrorAlerts
          : showCriticalErrorAlerts // ignore: cast_nullable_to_non_nullable
              as bool,
      pincode: null == pincode
          ? _value.pincode
          : pincode // ignore: cast_nullable_to_non_nullable
              as String,
      betalingsregelingRijkFeature: null == betalingsregelingRijkFeature
          ? _value.betalingsregelingRijkFeature
          : betalingsregelingRijkFeature // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UserSettingsImplCopyWith<$Res>
    implements $UserSettingsCopyWith<$Res> {
  factory _$$UserSettingsImplCopyWith(
          _$UserSettingsImpl value, $Res Function(_$UserSettingsImpl) then) =
      __$$UserSettingsImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {bool hasSeenOnboarding,
      bool autoSelectAllOrganizations,
      bool hasCompletedRegistration,
      bool hasCompletedOrganizationSelection,
      bool delay,
      bool showDevelopmentOverlay,
      bool showCriticalErrorAlerts,
      String pincode,
      bool betalingsregelingRijkFeature});
}

/// @nodoc
class __$$UserSettingsImplCopyWithImpl<$Res>
    extends _$UserSettingsCopyWithImpl<$Res, _$UserSettingsImpl>
    implements _$$UserSettingsImplCopyWith<$Res> {
  __$$UserSettingsImplCopyWithImpl(
      _$UserSettingsImpl _value, $Res Function(_$UserSettingsImpl) _then)
      : super(_value, _then);

  /// Create a copy of UserSettings
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? hasSeenOnboarding = null,
    Object? autoSelectAllOrganizations = null,
    Object? hasCompletedRegistration = null,
    Object? hasCompletedOrganizationSelection = null,
    Object? delay = null,
    Object? showDevelopmentOverlay = null,
    Object? showCriticalErrorAlerts = null,
    Object? pincode = null,
    Object? betalingsregelingRijkFeature = null,
  }) {
    return _then(_$UserSettingsImpl(
      hasSeenOnboarding: null == hasSeenOnboarding
          ? _value.hasSeenOnboarding
          : hasSeenOnboarding // ignore: cast_nullable_to_non_nullable
              as bool,
      autoSelectAllOrganizations: null == autoSelectAllOrganizations
          ? _value.autoSelectAllOrganizations
          : autoSelectAllOrganizations // ignore: cast_nullable_to_non_nullable
              as bool,
      hasCompletedRegistration: null == hasCompletedRegistration
          ? _value.hasCompletedRegistration
          : hasCompletedRegistration // ignore: cast_nullable_to_non_nullable
              as bool,
      hasCompletedOrganizationSelection: null ==
              hasCompletedOrganizationSelection
          ? _value.hasCompletedOrganizationSelection
          : hasCompletedOrganizationSelection // ignore: cast_nullable_to_non_nullable
              as bool,
      delay: null == delay
          ? _value.delay
          : delay // ignore: cast_nullable_to_non_nullable
              as bool,
      showDevelopmentOverlay: null == showDevelopmentOverlay
          ? _value.showDevelopmentOverlay
          : showDevelopmentOverlay // ignore: cast_nullable_to_non_nullable
              as bool,
      showCriticalErrorAlerts: null == showCriticalErrorAlerts
          ? _value.showCriticalErrorAlerts
          : showCriticalErrorAlerts // ignore: cast_nullable_to_non_nullable
              as bool,
      pincode: null == pincode
          ? _value.pincode
          : pincode // ignore: cast_nullable_to_non_nullable
              as String,
      betalingsregelingRijkFeature: null == betalingsregelingRijkFeature
          ? _value.betalingsregelingRijkFeature
          : betalingsregelingRijkFeature // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$UserSettingsImpl with DiagnosticableTreeMixin implements _UserSettings {
  const _$UserSettingsImpl(
      {required this.hasSeenOnboarding,
      required this.autoSelectAllOrganizations,
      required this.hasCompletedRegistration,
      required this.hasCompletedOrganizationSelection,
      required this.delay,
      required this.showDevelopmentOverlay,
      required this.showCriticalErrorAlerts,
      required this.pincode,
      required this.betalingsregelingRijkFeature});

  @override
  final bool hasSeenOnboarding;
  @override
  final bool autoSelectAllOrganizations;
  @override
  final bool hasCompletedRegistration;
  @override
  final bool hasCompletedOrganizationSelection;
  @override
  final bool delay;
  @override
  final bool showDevelopmentOverlay;
  @override
  final bool showCriticalErrorAlerts;
  @override
  final String pincode;
  @override
  final bool betalingsregelingRijkFeature;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UserSettings(hasSeenOnboarding: $hasSeenOnboarding, autoSelectAllOrganizations: $autoSelectAllOrganizations, hasCompletedRegistration: $hasCompletedRegistration, hasCompletedOrganizationSelection: $hasCompletedOrganizationSelection, delay: $delay, showDevelopmentOverlay: $showDevelopmentOverlay, showCriticalErrorAlerts: $showCriticalErrorAlerts, pincode: $pincode, betalingsregelingRijkFeature: $betalingsregelingRijkFeature)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'UserSettings'))
      ..add(DiagnosticsProperty('hasSeenOnboarding', hasSeenOnboarding))
      ..add(DiagnosticsProperty(
          'autoSelectAllOrganizations', autoSelectAllOrganizations))
      ..add(DiagnosticsProperty(
          'hasCompletedRegistration', hasCompletedRegistration))
      ..add(DiagnosticsProperty('hasCompletedOrganizationSelection',
          hasCompletedOrganizationSelection))
      ..add(DiagnosticsProperty('delay', delay))
      ..add(
          DiagnosticsProperty('showDevelopmentOverlay', showDevelopmentOverlay))
      ..add(DiagnosticsProperty(
          'showCriticalErrorAlerts', showCriticalErrorAlerts))
      ..add(DiagnosticsProperty('pincode', pincode))
      ..add(DiagnosticsProperty(
          'betalingsregelingRijkFeature', betalingsregelingRijkFeature));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UserSettingsImpl &&
            (identical(other.hasSeenOnboarding, hasSeenOnboarding) ||
                other.hasSeenOnboarding == hasSeenOnboarding) &&
            (identical(other.autoSelectAllOrganizations,
                    autoSelectAllOrganizations) ||
                other.autoSelectAllOrganizations ==
                    autoSelectAllOrganizations) &&
            (identical(
                    other.hasCompletedRegistration, hasCompletedRegistration) ||
                other.hasCompletedRegistration == hasCompletedRegistration) &&
            (identical(other.hasCompletedOrganizationSelection,
                    hasCompletedOrganizationSelection) ||
                other.hasCompletedOrganizationSelection ==
                    hasCompletedOrganizationSelection) &&
            (identical(other.delay, delay) || other.delay == delay) &&
            (identical(other.showDevelopmentOverlay, showDevelopmentOverlay) ||
                other.showDevelopmentOverlay == showDevelopmentOverlay) &&
            (identical(
                    other.showCriticalErrorAlerts, showCriticalErrorAlerts) ||
                other.showCriticalErrorAlerts == showCriticalErrorAlerts) &&
            (identical(other.pincode, pincode) || other.pincode == pincode) &&
            (identical(other.betalingsregelingRijkFeature,
                    betalingsregelingRijkFeature) ||
                other.betalingsregelingRijkFeature ==
                    betalingsregelingRijkFeature));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      hasSeenOnboarding,
      autoSelectAllOrganizations,
      hasCompletedRegistration,
      hasCompletedOrganizationSelection,
      delay,
      showDevelopmentOverlay,
      showCriticalErrorAlerts,
      pincode,
      betalingsregelingRijkFeature);

  /// Create a copy of UserSettings
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UserSettingsImplCopyWith<_$UserSettingsImpl> get copyWith =>
      __$$UserSettingsImplCopyWithImpl<_$UserSettingsImpl>(this, _$identity);
}

abstract class _UserSettings implements UserSettings {
  const factory _UserSettings(
      {required final bool hasSeenOnboarding,
      required final bool autoSelectAllOrganizations,
      required final bool hasCompletedRegistration,
      required final bool hasCompletedOrganizationSelection,
      required final bool delay,
      required final bool showDevelopmentOverlay,
      required final bool showCriticalErrorAlerts,
      required final String pincode,
      required final bool betalingsregelingRijkFeature}) = _$UserSettingsImpl;

  @override
  bool get hasSeenOnboarding;
  @override
  bool get autoSelectAllOrganizations;
  @override
  bool get hasCompletedRegistration;
  @override
  bool get hasCompletedOrganizationSelection;
  @override
  bool get delay;
  @override
  bool get showDevelopmentOverlay;
  @override
  bool get showCriticalErrorAlerts;
  @override
  String get pincode;
  @override
  bool get betalingsregelingRijkFeature;

  /// Create a copy of UserSettings
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UserSettingsImplCopyWith<_$UserSettingsImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
