import 'package:freezed_annotation/freezed_annotation.dart';

part 'pincode_error.freezed.dart';

@freezed
class PincodeError with _$PincodeError {
  const factory PincodeError({
    required String title,
    required String message,
  }) = _PincodeError;
}
