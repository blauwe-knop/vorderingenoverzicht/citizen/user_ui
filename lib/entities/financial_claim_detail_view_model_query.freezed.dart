// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'financial_claim_detail_view_model_query.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$FinancialClaimDetailViewModelQuery {
  int get financialClaimsInformationStorageId =>
      throw _privateConstructorUsedError;
  String get zaakkenmerk => throw _privateConstructorUsedError;

  /// Create a copy of FinancialClaimDetailViewModelQuery
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $FinancialClaimDetailViewModelQueryCopyWith<
          FinancialClaimDetailViewModelQuery>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FinancialClaimDetailViewModelQueryCopyWith<$Res> {
  factory $FinancialClaimDetailViewModelQueryCopyWith(
          FinancialClaimDetailViewModelQuery value,
          $Res Function(FinancialClaimDetailViewModelQuery) then) =
      _$FinancialClaimDetailViewModelQueryCopyWithImpl<$Res,
          FinancialClaimDetailViewModelQuery>;
  @useResult
  $Res call({int financialClaimsInformationStorageId, String zaakkenmerk});
}

/// @nodoc
class _$FinancialClaimDetailViewModelQueryCopyWithImpl<$Res,
        $Val extends FinancialClaimDetailViewModelQuery>
    implements $FinancialClaimDetailViewModelQueryCopyWith<$Res> {
  _$FinancialClaimDetailViewModelQueryCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of FinancialClaimDetailViewModelQuery
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? financialClaimsInformationStorageId = null,
    Object? zaakkenmerk = null,
  }) {
    return _then(_value.copyWith(
      financialClaimsInformationStorageId: null ==
              financialClaimsInformationStorageId
          ? _value.financialClaimsInformationStorageId
          : financialClaimsInformationStorageId // ignore: cast_nullable_to_non_nullable
              as int,
      zaakkenmerk: null == zaakkenmerk
          ? _value.zaakkenmerk
          : zaakkenmerk // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FinancialClaimDetailViewModelQueryImplCopyWith<$Res>
    implements $FinancialClaimDetailViewModelQueryCopyWith<$Res> {
  factory _$$FinancialClaimDetailViewModelQueryImplCopyWith(
          _$FinancialClaimDetailViewModelQueryImpl value,
          $Res Function(_$FinancialClaimDetailViewModelQueryImpl) then) =
      __$$FinancialClaimDetailViewModelQueryImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int financialClaimsInformationStorageId, String zaakkenmerk});
}

/// @nodoc
class __$$FinancialClaimDetailViewModelQueryImplCopyWithImpl<$Res>
    extends _$FinancialClaimDetailViewModelQueryCopyWithImpl<$Res,
        _$FinancialClaimDetailViewModelQueryImpl>
    implements _$$FinancialClaimDetailViewModelQueryImplCopyWith<$Res> {
  __$$FinancialClaimDetailViewModelQueryImplCopyWithImpl(
      _$FinancialClaimDetailViewModelQueryImpl _value,
      $Res Function(_$FinancialClaimDetailViewModelQueryImpl) _then)
      : super(_value, _then);

  /// Create a copy of FinancialClaimDetailViewModelQuery
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? financialClaimsInformationStorageId = null,
    Object? zaakkenmerk = null,
  }) {
    return _then(_$FinancialClaimDetailViewModelQueryImpl(
      financialClaimsInformationStorageId: null ==
              financialClaimsInformationStorageId
          ? _value.financialClaimsInformationStorageId
          : financialClaimsInformationStorageId // ignore: cast_nullable_to_non_nullable
              as int,
      zaakkenmerk: null == zaakkenmerk
          ? _value.zaakkenmerk
          : zaakkenmerk // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FinancialClaimDetailViewModelQueryImpl
    implements _FinancialClaimDetailViewModelQuery {
  const _$FinancialClaimDetailViewModelQueryImpl(
      {required this.financialClaimsInformationStorageId,
      required this.zaakkenmerk});

  @override
  final int financialClaimsInformationStorageId;
  @override
  final String zaakkenmerk;

  @override
  String toString() {
    return 'FinancialClaimDetailViewModelQuery(financialClaimsInformationStorageId: $financialClaimsInformationStorageId, zaakkenmerk: $zaakkenmerk)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FinancialClaimDetailViewModelQueryImpl &&
            (identical(other.financialClaimsInformationStorageId,
                    financialClaimsInformationStorageId) ||
                other.financialClaimsInformationStorageId ==
                    financialClaimsInformationStorageId) &&
            (identical(other.zaakkenmerk, zaakkenmerk) ||
                other.zaakkenmerk == zaakkenmerk));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, financialClaimsInformationStorageId, zaakkenmerk);

  /// Create a copy of FinancialClaimDetailViewModelQuery
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$FinancialClaimDetailViewModelQueryImplCopyWith<
          _$FinancialClaimDetailViewModelQueryImpl>
      get copyWith => __$$FinancialClaimDetailViewModelQueryImplCopyWithImpl<
          _$FinancialClaimDetailViewModelQueryImpl>(this, _$identity);
}

abstract class _FinancialClaimDetailViewModelQuery
    implements FinancialClaimDetailViewModelQuery {
  const factory _FinancialClaimDetailViewModelQuery(
          {required final int financialClaimsInformationStorageId,
          required final String zaakkenmerk}) =
      _$FinancialClaimDetailViewModelQueryImpl;

  @override
  int get financialClaimsInformationStorageId;
  @override
  String get zaakkenmerk;

  /// Create a copy of FinancialClaimDetailViewModelQuery
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$FinancialClaimDetailViewModelQueryImplCopyWith<
          _$FinancialClaimDetailViewModelQueryImpl>
      get copyWith => throw _privateConstructorUsedError;
}
