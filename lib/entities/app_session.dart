// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:freezed_annotation/freezed_annotation.dart';

part 'app_session.freezed.dart';

@freezed
class AppSession with _$AppSession {
  const factory AppSession({
    required int id,
    required String key,
    required String token,
    required String oin,
  }) = _AppSession;
}
