// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'registration.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Registration {
  int get id => throw _privateConstructorUsedError;
  DateTime get dateTimeStarted => throw _privateConstructorUsedError;
  String get appPublicKey => throw _privateConstructorUsedError;
  String get appManagerOin => throw _privateConstructorUsedError;
  String get appManagerPublicKey => throw _privateConstructorUsedError;
  String get registrationToken => throw _privateConstructorUsedError;
  DateTime? get dateTimeCompleted => throw _privateConstructorUsedError;
  String? get givenName => throw _privateConstructorUsedError;
  bool get expired => throw _privateConstructorUsedError;
  bool get revoked => throw _privateConstructorUsedError;

  /// Create a copy of Registration
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $RegistrationCopyWith<Registration> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RegistrationCopyWith<$Res> {
  factory $RegistrationCopyWith(
          Registration value, $Res Function(Registration) then) =
      _$RegistrationCopyWithImpl<$Res, Registration>;
  @useResult
  $Res call(
      {int id,
      DateTime dateTimeStarted,
      String appPublicKey,
      String appManagerOin,
      String appManagerPublicKey,
      String registrationToken,
      DateTime? dateTimeCompleted,
      String? givenName,
      bool expired,
      bool revoked});
}

/// @nodoc
class _$RegistrationCopyWithImpl<$Res, $Val extends Registration>
    implements $RegistrationCopyWith<$Res> {
  _$RegistrationCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of Registration
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? dateTimeStarted = null,
    Object? appPublicKey = null,
    Object? appManagerOin = null,
    Object? appManagerPublicKey = null,
    Object? registrationToken = null,
    Object? dateTimeCompleted = freezed,
    Object? givenName = freezed,
    Object? expired = null,
    Object? revoked = null,
  }) {
    return _then(_value.copyWith(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      dateTimeStarted: null == dateTimeStarted
          ? _value.dateTimeStarted
          : dateTimeStarted // ignore: cast_nullable_to_non_nullable
              as DateTime,
      appPublicKey: null == appPublicKey
          ? _value.appPublicKey
          : appPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      appManagerOin: null == appManagerOin
          ? _value.appManagerOin
          : appManagerOin // ignore: cast_nullable_to_non_nullable
              as String,
      appManagerPublicKey: null == appManagerPublicKey
          ? _value.appManagerPublicKey
          : appManagerPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      registrationToken: null == registrationToken
          ? _value.registrationToken
          : registrationToken // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeCompleted: freezed == dateTimeCompleted
          ? _value.dateTimeCompleted
          : dateTimeCompleted // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      givenName: freezed == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String?,
      expired: null == expired
          ? _value.expired
          : expired // ignore: cast_nullable_to_non_nullable
              as bool,
      revoked: null == revoked
          ? _value.revoked
          : revoked // ignore: cast_nullable_to_non_nullable
              as bool,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$RegistrationImplCopyWith<$Res>
    implements $RegistrationCopyWith<$Res> {
  factory _$$RegistrationImplCopyWith(
          _$RegistrationImpl value, $Res Function(_$RegistrationImpl) then) =
      __$$RegistrationImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int id,
      DateTime dateTimeStarted,
      String appPublicKey,
      String appManagerOin,
      String appManagerPublicKey,
      String registrationToken,
      DateTime? dateTimeCompleted,
      String? givenName,
      bool expired,
      bool revoked});
}

/// @nodoc
class __$$RegistrationImplCopyWithImpl<$Res>
    extends _$RegistrationCopyWithImpl<$Res, _$RegistrationImpl>
    implements _$$RegistrationImplCopyWith<$Res> {
  __$$RegistrationImplCopyWithImpl(
      _$RegistrationImpl _value, $Res Function(_$RegistrationImpl) _then)
      : super(_value, _then);

  /// Create a copy of Registration
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = null,
    Object? dateTimeStarted = null,
    Object? appPublicKey = null,
    Object? appManagerOin = null,
    Object? appManagerPublicKey = null,
    Object? registrationToken = null,
    Object? dateTimeCompleted = freezed,
    Object? givenName = freezed,
    Object? expired = null,
    Object? revoked = null,
  }) {
    return _then(_$RegistrationImpl(
      id: null == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int,
      dateTimeStarted: null == dateTimeStarted
          ? _value.dateTimeStarted
          : dateTimeStarted // ignore: cast_nullable_to_non_nullable
              as DateTime,
      appPublicKey: null == appPublicKey
          ? _value.appPublicKey
          : appPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      appManagerOin: null == appManagerOin
          ? _value.appManagerOin
          : appManagerOin // ignore: cast_nullable_to_non_nullable
              as String,
      appManagerPublicKey: null == appManagerPublicKey
          ? _value.appManagerPublicKey
          : appManagerPublicKey // ignore: cast_nullable_to_non_nullable
              as String,
      registrationToken: null == registrationToken
          ? _value.registrationToken
          : registrationToken // ignore: cast_nullable_to_non_nullable
              as String,
      dateTimeCompleted: freezed == dateTimeCompleted
          ? _value.dateTimeCompleted
          : dateTimeCompleted // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      givenName: freezed == givenName
          ? _value.givenName
          : givenName // ignore: cast_nullable_to_non_nullable
              as String?,
      expired: null == expired
          ? _value.expired
          : expired // ignore: cast_nullable_to_non_nullable
              as bool,
      revoked: null == revoked
          ? _value.revoked
          : revoked // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$RegistrationImpl with DiagnosticableTreeMixin implements _Registration {
  const _$RegistrationImpl(
      {required this.id,
      required this.dateTimeStarted,
      required this.appPublicKey,
      required this.appManagerOin,
      required this.appManagerPublicKey,
      required this.registrationToken,
      required this.dateTimeCompleted,
      required this.givenName,
      required this.expired,
      required this.revoked});

  @override
  final int id;
  @override
  final DateTime dateTimeStarted;
  @override
  final String appPublicKey;
  @override
  final String appManagerOin;
  @override
  final String appManagerPublicKey;
  @override
  final String registrationToken;
  @override
  final DateTime? dateTimeCompleted;
  @override
  final String? givenName;
  @override
  final bool expired;
  @override
  final bool revoked;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'Registration(id: $id, dateTimeStarted: $dateTimeStarted, appPublicKey: $appPublicKey, appManagerOin: $appManagerOin, appManagerPublicKey: $appManagerPublicKey, registrationToken: $registrationToken, dateTimeCompleted: $dateTimeCompleted, givenName: $givenName, expired: $expired, revoked: $revoked)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'Registration'))
      ..add(DiagnosticsProperty('id', id))
      ..add(DiagnosticsProperty('dateTimeStarted', dateTimeStarted))
      ..add(DiagnosticsProperty('appPublicKey', appPublicKey))
      ..add(DiagnosticsProperty('appManagerOin', appManagerOin))
      ..add(DiagnosticsProperty('appManagerPublicKey', appManagerPublicKey))
      ..add(DiagnosticsProperty('registrationToken', registrationToken))
      ..add(DiagnosticsProperty('dateTimeCompleted', dateTimeCompleted))
      ..add(DiagnosticsProperty('givenName', givenName))
      ..add(DiagnosticsProperty('expired', expired))
      ..add(DiagnosticsProperty('revoked', revoked));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RegistrationImpl &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.dateTimeStarted, dateTimeStarted) ||
                other.dateTimeStarted == dateTimeStarted) &&
            (identical(other.appPublicKey, appPublicKey) ||
                other.appPublicKey == appPublicKey) &&
            (identical(other.appManagerOin, appManagerOin) ||
                other.appManagerOin == appManagerOin) &&
            (identical(other.appManagerPublicKey, appManagerPublicKey) ||
                other.appManagerPublicKey == appManagerPublicKey) &&
            (identical(other.registrationToken, registrationToken) ||
                other.registrationToken == registrationToken) &&
            (identical(other.dateTimeCompleted, dateTimeCompleted) ||
                other.dateTimeCompleted == dateTimeCompleted) &&
            (identical(other.givenName, givenName) ||
                other.givenName == givenName) &&
            (identical(other.expired, expired) || other.expired == expired) &&
            (identical(other.revoked, revoked) || other.revoked == revoked));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      id,
      dateTimeStarted,
      appPublicKey,
      appManagerOin,
      appManagerPublicKey,
      registrationToken,
      dateTimeCompleted,
      givenName,
      expired,
      revoked);

  /// Create a copy of Registration
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$RegistrationImplCopyWith<_$RegistrationImpl> get copyWith =>
      __$$RegistrationImplCopyWithImpl<_$RegistrationImpl>(this, _$identity);
}

abstract class _Registration implements Registration {
  const factory _Registration(
      {required final int id,
      required final DateTime dateTimeStarted,
      required final String appPublicKey,
      required final String appManagerOin,
      required final String appManagerPublicKey,
      required final String registrationToken,
      required final DateTime? dateTimeCompleted,
      required final String? givenName,
      required final bool expired,
      required final bool revoked}) = _$RegistrationImpl;

  @override
  int get id;
  @override
  DateTime get dateTimeStarted;
  @override
  String get appPublicKey;
  @override
  String get appManagerOin;
  @override
  String get appManagerPublicKey;
  @override
  String get registrationToken;
  @override
  DateTime? get dateTimeCompleted;
  @override
  String? get givenName;
  @override
  bool get expired;
  @override
  bool get revoked;

  /// Create a copy of Registration
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$RegistrationImplCopyWith<_$RegistrationImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
