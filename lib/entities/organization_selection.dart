import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'organization_selection.freezed.dart';

@freezed
class OrganizationSelection with _$OrganizationSelection {
  const factory OrganizationSelection({
    required int id,
    required String oin,
    required bool selected,
  }) = _OrganizationSelection;
}
