// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

enum CertificateType {
  appManagerJWTCertificate("AppManagerJWTCertificate"),
  certificateTypeNone("CertificateTypeNone");

  const CertificateType(this.value);

  final String value;
}
