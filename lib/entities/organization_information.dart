import 'package:freezed_annotation/freezed_annotation.dart';

part 'organization_information.freezed.dart';
part 'organization_information.g.dart';

@freezed
class OrganizationInformation with _$OrganizationInformation {
  const factory OrganizationInformation({
    required String oin,
    required String organization,
    required String introText,
    required List<String> availableClaims,
    required List<String> nonAvailableClaims,
  }) = _OrganizationInformation;

  factory OrganizationInformation.fromJson(Map<String, dynamic> json) =>
      _$OrganizationInformationFromJson(json);
}
