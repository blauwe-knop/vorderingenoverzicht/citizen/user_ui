// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

class RSAKeypair {
  String privateKey;
  String publicKey;

  RSAKeypair(this.privateKey, this.publicKey);
}
