// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'certificate.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$CertificateImpl _$$CertificateImplFromJson(Map<String, dynamic> json) =>
    _$CertificateImpl(
      type: json['type'] as String,
      value: const Uint8ListJsonConverter().fromJson(json['value'] as String),
    );

Map<String, dynamic> _$$CertificateImplToJson(_$CertificateImpl instance) =>
    <String, dynamic>{
      'type': instance.type,
      'value': const Uint8ListJsonConverter().toJson(instance.value),
    };
