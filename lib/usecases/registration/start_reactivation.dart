// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/registration/latest_expired_and_unrevoked_registration_notifier.dart';
import 'package:user_ui/usecases/helpers/revoke_registration_helper.dart';

part 'start_reactivation.g.dart';

@riverpod
Future<void> startReactivation(Ref ref) async {
  final registration =
      await ref.read(latestExpiredAndUnrevokedRegistrationProvider.future);

  await ref.read(revokeRegistrationHelperProvider(registration).future);

  await ref.read(appRootKeypairProvider.notifier).generateAndStoreAppKeyPair();
}
