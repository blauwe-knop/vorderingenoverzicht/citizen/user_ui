// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'undo_latest_registration.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$undoLatestRegistrationHash() =>
    r'8f3955f605c569dc6f45708dbdb214180315b782';

/// See also [undoLatestRegistration].
@ProviderFor(undoLatestRegistration)
final undoLatestRegistrationProvider = AutoDisposeFutureProvider<void>.internal(
  undoLatestRegistration,
  name: r'undoLatestRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$undoLatestRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UndoLatestRegistrationRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
