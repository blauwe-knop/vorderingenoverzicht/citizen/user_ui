// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/latest_started_registration_notifier.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/usecases/helpers/revoke_registration_helper.dart';

part 'undo_latest_registration.g.dart';

@riverpod
Future<void> undoLatestRegistration(Ref ref) async {
  final registration = await ref.read(latestStartedRegistrationProvider.future);

  try {
    await ref.read(revokeRegistrationHelperProvider(registration).future);
  } catch (_) {
    final loggingHelper = ref.read(loggingProvider);

    loggingHelper.addLog(DeviceEvent.uu_78, "revoking registrations error");
  }

  await ref
      .read(registrationRepositoryProvider.notifier)
      .deleteRegistration(registration.id);
}
