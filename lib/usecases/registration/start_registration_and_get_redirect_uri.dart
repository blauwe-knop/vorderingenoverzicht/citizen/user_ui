// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL
import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/register_app.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/env_provider.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';
import 'package:user_ui/usecases/registration/models/app_identity.dart';

part 'start_registration_and_get_redirect_uri.g.dart';

@riverpod
Future<Uri> startRegistrationAndGetRedirectUri(Ref ref) async {
  final loggingHelper = ref.read(loggingProvider);

  if (ref.read(userSettingsRepositoryProvider).delay) {
    await Future.delayed(const Duration(milliseconds: 2000));
  }

  final selectedSchemeAppManager =
      await ref.read(fetchSchemeAppManagerProvider.future);

  final discoveredServices = await ref
      .read(serviceDiscoveryClientProvider)
      .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

  final sessionToken = await ref.read(
    startSessionProvider(
      sessionApiUrl: discoveredServices.getSessionApiV1(loggingHelper),
      rootOrganizationEcPublicKey: selectedSchemeAppManager.publicKey,
      organizationOin: selectedSchemeAppManager.oin,
      contactType: OrganizationType.appManager,
    ).future,
  );

  final appSession = await ref
      .read(latestAppSessionProvider(oin: selectedSchemeAppManager.oin).future);

  String clientId;

  final onLocalHost =
      await ref.read(environmentProvider).isRunningOnWebLocalhost();

  if (!onLocalHost) {
    if (!kIsWeb) {
      clientId = "native_app";
    } else if (ref.read(environmentProvider).type == EnvironmentType.test) {
      clientId = "test_web";
    } else if (ref.read(environmentProvider).type == EnvironmentType.demo) {
      clientId = "demo_web";
    } else if (ref.read(environmentProvider).type == EnvironmentType.local) {
      clientId = "local_web";
    } else {
      loggingHelper.addLog(DeviceEvent.uu_124,
          "Failed to load client ID: Invalid environment type");
      throw Exception("Failed to load client ID: Invalid environment type");
    }
  } else {
    clientId = "development_web";
  }

  final rootAppPublicKey =
      await ref.read(appRootKeypairProvider.notifier).getPublicKey();

  if (rootAppPublicKey == null) {
    loggingHelper.addLog(DeviceEvent.uu_110, "App root public key is null");
    throw Exception('App root public key should not be null');
  }

  final appIdentity = AppIdentity(
    appPublicKey: rootAppPublicKey,
    clientId: clientId,
  );

  String? encryptedAppIdentity;
  final aesKeyBytes = Base64.toBytes(appSession.key);

  try {
    encryptedAppIdentity = Base64.parseFromBytes(
      Aes.encrypt(
        aesKeyBytes,
        utf8.encode(
          json.encode(appIdentity),
        ),
      ),
    );
  } catch (ex) {
    // TODO: Add logging
    debugPrint('CRYPTO ERROR: Failed to encrypt app identity. $ex');
    throw Exception('Failed to encrypt app identity');
  }

  final encodedEcryptedRegistrationToken = await ref.read(registerAppProvider(
          serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
          sessionToken: sessionToken,
          encryptedAppIdentity: encryptedAppIdentity)
      .future);

  String? registrationToken;
  final ecryptedRegistrationToken =
      Base64.toBytes(encodedEcryptedRegistrationToken);

  try {
    registrationToken = utf8.decode(
      Aes.decrypt(
        aesKeyBytes,
        ecryptedRegistrationToken,
      ),
    );
  } catch (ex) {
    // TODO: Add logging
    debugPrint('CRYPTO ERROR: Failed to decrypt registration token. $ex');
    throw Exception('Failed to decrypt registration token. $ex');
  }

  final dateTimeStarted = DateTime.now();

  await ref.read(registrationRepositoryProvider.notifier).addRegistration(
        dateTimeStarted: dateTimeStarted,
        appPublicKey: rootAppPublicKey,
        appManagerOin: selectedSchemeAppManager.oin,
        appManagerPublicKey: selectedSchemeAppManager.publicKey,
        registrationToken: registrationToken,
      );

  // NOTE: skip app manager login when using mock
  // TODO: improve skip appManager
  if (ref.read(environmentProvider).type == EnvironmentType.mock) {
    return Uri.parse("app://vorijk.nl/activatie-voltooien");
  }

  final appManagerUiUri =
      Uri.parse(discoveredServices.getAppManagerUi(loggingHelper));

  final uri = appManagerUiUri
      .replace(queryParameters: {"registrationToken": registrationToken});

  return uri;
}
