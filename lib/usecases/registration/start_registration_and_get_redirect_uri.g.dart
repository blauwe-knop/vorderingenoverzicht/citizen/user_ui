// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_registration_and_get_redirect_uri.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startRegistrationAndGetRedirectUriHash() =>
    r'5d8cddf350e90cf7ac85b9bda411b026db90f182';

/// See also [startRegistrationAndGetRedirectUri].
@ProviderFor(startRegistrationAndGetRedirectUri)
final startRegistrationAndGetRedirectUriProvider =
    AutoDisposeFutureProvider<Uri>.internal(
  startRegistrationAndGetRedirectUri,
  name: r'startRegistrationAndGetRedirectUriProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$startRegistrationAndGetRedirectUriHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef StartRegistrationAndGetRedirectUriRef
    = AutoDisposeFutureProviderRef<Uri>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
