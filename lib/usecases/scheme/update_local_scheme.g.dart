// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'update_local_scheme.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$updateLocalSchemeHash() => r'361606dea7bc6ddba1d7798a74f1112d80e8c05b';

/// See also [updateLocalScheme].
@ProviderFor(updateLocalScheme)
final updateLocalSchemeProvider = AutoDisposeFutureProvider<void>.internal(
  updateLocalScheme,
  name: r'updateLocalSchemeProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$updateLocalSchemeHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UpdateLocalSchemeRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
