// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'copy_financial_claims_information_inbox_to_storage.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$copyFinancialClaimsInformationInboxToStorageHash() =>
    r'180eed3eba29093ad94c9026f685e1b3a4be7f34';

/// See also [copyFinancialClaimsInformationInboxToStorage].
@ProviderFor(copyFinancialClaimsInformationInboxToStorage)
final copyFinancialClaimsInformationInboxToStorageProvider =
    AutoDisposeFutureProvider<void>.internal(
  copyFinancialClaimsInformationInboxToStorage,
  name: r'copyFinancialClaimsInformationInboxToStorageProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$copyFinancialClaimsInformationInboxToStorageHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CopyFinancialClaimsInformationInboxToStorageRef
    = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
