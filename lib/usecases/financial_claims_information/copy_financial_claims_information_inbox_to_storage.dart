import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';

part 'copy_financial_claims_information_inbox_to_storage.g.dart';

@riverpod
Future<void> copyFinancialClaimsInformationInboxToStorage(Ref ref) async {
  final financialClaimsInformationInboxRepository =
      ref.watch(financialClaimsInformationInboxRepositoryProvider.notifier);

  final financialClaimsInformationStorageRepository =
      ref.watch(financialClaimsInformationStorageRepositoryProvider.notifier);

  var financialClaimsInformationInbox =
      await financialClaimsInformationInboxRepository
          .latestFinancialClaimsInformationInboxPerOrganization;

  for (var inboxItem
      in financialClaimsInformationInbox.where((e) => !e.copiedToStorage)) {
    financialClaimsInformationStorageRepository.add(
      inboxItem.oin,
      inboxItem.dateTimeRequested,
      inboxItem.dateTimeReceived,
      inboxItem.financialClaimsInformationDocument,
    );

    inboxItem = inboxItem.copyWith(copiedToStorage: true);
    financialClaimsInformationInboxRepository.update(inboxItem);
  }
}
