// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_session_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startSessionDelegateHash() =>
    r'0902d24b311e866eeea91cb47e7498edd8e3f46b';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
///
/// Copied from [startSessionDelegate].
@ProviderFor(startSessionDelegate)
final startSessionDelegateProvider = AutoDisposeProvider<
    Future<String> Function(
        Ref ref,
        String sessionApiUrl,
        String rootOrganizationEcPublicKey,
        String organizationOin,
        OrganizationType contactType)>.internal(
  startSessionDelegate,
  name: r'startSessionDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$startSessionDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef StartSessionDelegateRef = AutoDisposeProviderRef<
    Future<String> Function(
        Ref ref,
        String sessionApiUrl,
        String rootOrganizationEcPublicKey,
        String organizationOin,
        OrganizationType contactType)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
