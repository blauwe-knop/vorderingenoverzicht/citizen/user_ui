// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/models/challenge_result.dart';
import 'package:user_ui/clients/session/session_client_live.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/app_session_repository.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/certificate/latest_certificate_with_scope_notifier.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/usecases/app_identity/generate_and_store_session_key.dart';

part 'start_session_delegate.g.dart';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
@riverpod
Future<String> Function(
  Ref ref,
  String sessionApiUrl,
  String rootOrganizationEcPublicKey,
  String organizationOin,
  OrganizationType contactType,
) startSessionDelegate(Ref _) {
  return (
    ref,
    sessionApiUrl,
    rootOrganizationEcPublicKey,
    organizationOin,
    contactType,
  ) async {
    final loggingHelper = ref.read(loggingProvider);

    final challenge =
        await ref.read(sessionClientProvider).requestChallenge(sessionApiUrl);

    PublicKey rootOrganizationPublicKey;

    try {
      rootOrganizationPublicKey =
          Ec.parsePublicKeyFromPem(utf8.encode(rootOrganizationEcPublicKey));
      Ec.verifyEcPublicKey(rootOrganizationPublicKey);
    } catch (ex) {
      loggingHelper.addLog(
          DeviceEvent.uu_94, "invalid root organization EC public key");
      throw Exception('invalid root organization EC public key');
    }

    bool? orgNanceSignatureIsVerified;
    try {
      orgNanceSignatureIsVerified = Ecdsa.verify(
        rootOrganizationPublicKey,
        Base64.toBytes(challenge.organizationNonceSignature),
        utf8.encode(challenge.nonce),
      );
    } catch (ex) {
      loggingHelper.addLog(
          DeviceEvent.uu_96, "Failed to verify signature. $ex");
      throw Exception('Failed to verify signature');
    }

    if (!orgNanceSignatureIsVerified) {
      loggingHelper.addLog(DeviceEvent.uu_26, "invalid challenge signature");
      throw Exception('invalid challenge signature');
    }

    await ref
        .read(generateAndStoreSessionKeyProvider(oin: organizationOin).future);

    final appNonceSignature =
        await ref.read(appRootKeypairProvider.notifier).sign(challenge.nonce);

    if (appNonceSignature == null) {
      loggingHelper.addLog(DeviceEvent.uu_115, "Signature is null");
      throw Exception('Signature cannot be null');
    }

    CertificateType? type;
    Certificate? certificate;

    if (contactType == OrganizationType.organization) {
      type = CertificateType.appManagerJWTCertificate;

      const String scope = 'nl.vorijk.oauth_scope.blauwe_knop';
      certificate =
          await ref.read(latestCertificateWithScopeProvider(scope).future);

      if (certificate == null ||
          certificate.deemedExpiredBySourceOrganization ||
          DateTime.now().isAfter(certificate.expiresAt)) {
        loggingHelper.addLog(DeviceEvent.uu_98, "Certificate expired");
        throw AppCertificateExpiredException('Certificate expired');
      }
    } else {
      type = CertificateType.certificateTypeNone;
    }

    final appSession =
        await ref.read(latestAppSessionProvider(oin: organizationOin).future);

    final rootAppPublicKey =
        await ref.read(appRootKeypairProvider.notifier).getPublicKey();

    if (rootAppPublicKey == null) {
      loggingHelper.addLog(DeviceEvent.uu_112, "App root public key is null");
      throw Exception('App root public key should not be null');
    }

    final challengeResult = ChallengeResult(
      appPublicKey: rootAppPublicKey,
      appNonceSignature: appNonceSignature,
      certificateType: type.value,
      certificate: certificate == null ? null : utf8.decode(certificate.value),
      sessionAesKey: appSession.key,
    );

    String? encryptedChallengeResult;

    try {
      final publicKey = Ec.parsePublicKeyFromPem(
          utf8.encode(challenge.ephemeralOrganizationEcPublicKey));
      encryptedChallengeResult = Base64.parseFromBytes(
        Ecies.encrypt(
          publicKey,
          utf8.encode(
            json.encode(challengeResult.toJson()),
          ),
        ),
      );
    } catch (ex) {
      loggingHelper.addLog(
          DeviceEvent.uu_113, "Failed to encrypt challenge result. $ex");
      throw Exception('Failed to encrypt challenge result');
    }

    final challengeResponse = ChallengeResponse(
      nonce: challenge.nonce,
      encryptedChallengeResult: encryptedChallengeResult,
    );

    final encodedEncryptedSessionToken =
        await ref.read(sessionClientProvider).completeChallenge(
              sessionApiUrl,
              challengeResponse,
            );

    String? sessionToken;
    final encryptedSessionToken = Base64.toBytes(encodedEncryptedSessionToken);
    final aesKeyBytes = Base64.toBytes(appSession.key);

    try {
      sessionToken = utf8.decode(
        Aes.decrypt(
          aesKeyBytes,
          encryptedSessionToken,
        ),
      );
    } catch (ex) {
      loggingHelper.addLog(
          DeviceEvent.uu_114, "Failed to decrypt session token. $ex");
      throw Exception('Failed to decrypt session token. $ex');
    }

    final appSessionUpdate = appSession.copyWith(token: sessionToken);
    await ref
        .read(appSessionRepositoryProvider.notifier)
        .updateAppSession(appSessionUpdate);

    return sessionToken;
  };
}

class AppCertificateExpiredException implements Exception {
  String cause;
  AppCertificateExpiredException(this.cause);
}
