// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/fetch_certificate.dart';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/router/router.dart';
import 'package:user_ui/screens/features/activation/different_bsn_screen.dart';
import 'package:user_ui/usecases/registration/models/certificate.dart';

part 'fetch_and_store_certificate_helper_delegate.g.dart';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
@riverpod
Future<void> Function(
  Ref ref,
  Registration registration,
) fetchAndStoreCertificateHelperDelegate(Ref _) {
  return (
    ref,
    registration,
  ) async {
    final loggingHelper = ref.read(loggingProvider);

    if (ref.read(userSettingsRepositoryProvider).delay) {
      Future.delayed(const Duration(milliseconds: 2000));
    }

    if (registration.registrationToken.isEmpty) {
      loggingHelper.addLog(DeviceEvent.uu_24, "registrationToken not found.");
      throw Exception("registrationToken not found");
    }

    final appRootPublicKey =
        await ref.read(appRootKeypairProvider.notifier).getPublicKey();

    if (registration.appPublicKey != appRootPublicKey) {
      loggingHelper.addLog(DeviceEvent.uu_29,
          "appPublicKey changed, cannot complete registration");
      throw Exception("appPublicKey changed, cannot complete registration");
    }

    final selectedSchemeAppManager =
        await ref.read(fetchSchemeAppManagerProvider.future);

    final discoveredServices = await ref
        .read(serviceDiscoveryClientProvider)
        .fetchDiscoveredServices(selectedSchemeAppManager.discoveryUrl);

    final appSession = await ref.read(
        latestAppSessionProvider(oin: selectedSchemeAppManager.oin).future);

    String encodedEncryptedCertificate;

    try {
      encodedEncryptedCertificate = await ref.read(fetchCertificateProvider(
        serviceUrl: discoveredServices.getRegistrationApiV1(loggingHelper),
        sessionToken: appSession.token,
        registrationToken: registration.registrationToken,
      ).future);
    } on RegistrationExpiredException catch (_) {
      final registrationExpired = registration.copyWith(expired: true);

      await ref
          .read(registrationRepositoryProvider.notifier)
          .updateRegistration(registrationExpired);

      await ref
          .read(financialClaimsInformationRequestRepositoryProvider.notifier)
          .cancelAllUncompletedRequests();

      await ref
          .read(financialClaimsInformationConfigurationRepositoryProvider
              .notifier)
          .expireAll();

      rethrow;
    }

    final Uint8List encryptedCertificate =
        Base64.toBytes(encodedEncryptedCertificate);

    final aesKeyBytes = Base64.toBytes(appSession.key);

    String? decryptedCertificate;

    try {
      decryptedCertificate = utf8.decode(
        Aes.decrypt(
          aesKeyBytes,
          encryptedCertificate,
        ),
      );
    } catch (ex) {
      // TODO: Add logging
      debugPrint('CRYPTO ERROR: Failed to decrypt certificate. $ex');
      throw Exception('Failed to decrypt certificate. $ex');
    }

    final decryptedCertificateJson = json.decode(decryptedCertificate);

    final certificate = Certificate.fromJson(decryptedCertificateJson);
    final certificateType = CertificateType.values.firstWhere(
      (type) => type.value == certificate.type,
    );

    String bsn;
    String givenName;
    DateTime expiresAt;
    String scope;

    if (certificateType == CertificateType.appManagerJWTCertificate) {
      String jwtAppPublicKey;

      try {
        final jwt = JWT.verify(
          String.fromCharCodes(certificate.value),
          ECPublicKey(selectedSchemeAppManager.publicKey),
        );

        bsn = jwt.payload["bsn"];

        givenName = jwt.payload["given_name"];

        scope = jwt.payload["scope"];

        jwtAppPublicKey = jwt.payload["app_public_key"];

        expiresAt =
            DateTime.fromMillisecondsSinceEpoch(jwt.payload["exp"] * 1000);
      } on JWTExpiredException {
        loggingHelper.addLog(DeviceEvent.uu_30, "certificate expired");
        throw Exception("certificate expired");
      } on JWTException catch (ex) {
        loggingHelper.addLog(DeviceEvent.uu_31,
            "Invalid signature on registration result: ${ex.message}");
        throw Exception(
            "Invalid signature on registration result: ${ex.message}");
      }

      if (registration.appPublicKey != jwtAppPublicKey) {
        loggingHelper.addLog(DeviceEvent.uu_32,
            "Invalid started registration result: wrong appPublicKey");
        throw Exception(
            "Invalid started registration result: wrong appPublicKey");
      }
    } else {
      loggingHelper.addLog(DeviceEvent.uu_33,
          "not implemented exception certificate type: ${certificate.type}");
      throw Exception(
          "not implemented exception certificate type: ${certificate.type}");
    }

    final certificates = await ref.read(certificateRepositoryProvider.future);
    if (certificates.any((certificate) => certificate.bsn != bsn)) {
      await ref.read(goRouterProvider).pushNamed(DifferentBsnScreen.routeName);

      return;
    }

    await ref.read(certificateRepositoryProvider.notifier).addCertificate(
          type: certificateType,
          value: certificate.value,
          bsn: bsn,
          givenName: givenName,
          expiresAt: expiresAt,
          scope: scope,
        );

    final registrationUpdate = registration.copyWith(
      givenName: givenName,
      dateTimeCompleted: DateTime.now(),
    );

    await ref
        .read(registrationRepositoryProvider.notifier)
        .updateRegistration(registrationUpdate);
  };
}
