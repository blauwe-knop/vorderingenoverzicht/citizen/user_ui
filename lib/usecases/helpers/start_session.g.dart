// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'start_session.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$startSessionHash() => r'4a3d420b83492a58d6615e475f7da8131f22a1f2';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [startSession].
@ProviderFor(startSession)
const startSessionProvider = StartSessionFamily();

/// See also [startSession].
class StartSessionFamily extends Family<AsyncValue<String>> {
  /// See also [startSession].
  const StartSessionFamily();

  /// See also [startSession].
  StartSessionProvider call({
    required String sessionApiUrl,
    required String rootOrganizationEcPublicKey,
    required String organizationOin,
    required OrganizationType contactType,
  }) {
    return StartSessionProvider(
      sessionApiUrl: sessionApiUrl,
      rootOrganizationEcPublicKey: rootOrganizationEcPublicKey,
      organizationOin: organizationOin,
      contactType: contactType,
    );
  }

  @override
  StartSessionProvider getProviderOverride(
    covariant StartSessionProvider provider,
  ) {
    return call(
      sessionApiUrl: provider.sessionApiUrl,
      rootOrganizationEcPublicKey: provider.rootOrganizationEcPublicKey,
      organizationOin: provider.organizationOin,
      contactType: provider.contactType,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'startSessionProvider';
}

/// See also [startSession].
class StartSessionProvider extends AutoDisposeFutureProvider<String> {
  /// See also [startSession].
  StartSessionProvider({
    required String sessionApiUrl,
    required String rootOrganizationEcPublicKey,
    required String organizationOin,
    required OrganizationType contactType,
  }) : this._internal(
          (ref) => startSession(
            ref as StartSessionRef,
            sessionApiUrl: sessionApiUrl,
            rootOrganizationEcPublicKey: rootOrganizationEcPublicKey,
            organizationOin: organizationOin,
            contactType: contactType,
          ),
          from: startSessionProvider,
          name: r'startSessionProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$startSessionHash,
          dependencies: StartSessionFamily._dependencies,
          allTransitiveDependencies:
              StartSessionFamily._allTransitiveDependencies,
          sessionApiUrl: sessionApiUrl,
          rootOrganizationEcPublicKey: rootOrganizationEcPublicKey,
          organizationOin: organizationOin,
          contactType: contactType,
        );

  StartSessionProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.sessionApiUrl,
    required this.rootOrganizationEcPublicKey,
    required this.organizationOin,
    required this.contactType,
  }) : super.internal();

  final String sessionApiUrl;
  final String rootOrganizationEcPublicKey;
  final String organizationOin;
  final OrganizationType contactType;

  @override
  Override overrideWith(
    FutureOr<String> Function(StartSessionRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: StartSessionProvider._internal(
        (ref) => create(ref as StartSessionRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        sessionApiUrl: sessionApiUrl,
        rootOrganizationEcPublicKey: rootOrganizationEcPublicKey,
        organizationOin: organizationOin,
        contactType: contactType,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<String> createElement() {
    return _StartSessionProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is StartSessionProvider &&
        other.sessionApiUrl == sessionApiUrl &&
        other.rootOrganizationEcPublicKey == rootOrganizationEcPublicKey &&
        other.organizationOin == organizationOin &&
        other.contactType == contactType;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, sessionApiUrl.hashCode);
    hash = _SystemHash.combine(hash, rootOrganizationEcPublicKey.hashCode);
    hash = _SystemHash.combine(hash, organizationOin.hashCode);
    hash = _SystemHash.combine(hash, contactType.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin StartSessionRef on AutoDisposeFutureProviderRef<String> {
  /// The parameter `sessionApiUrl` of this provider.
  String get sessionApiUrl;

  /// The parameter `rootOrganizationEcPublicKey` of this provider.
  String get rootOrganizationEcPublicKey;

  /// The parameter `organizationOin` of this provider.
  String get organizationOin;

  /// The parameter `contactType` of this provider.
  OrganizationType get contactType;
}

class _StartSessionProviderElement
    extends AutoDisposeFutureProviderElement<String> with StartSessionRef {
  _StartSessionProviderElement(super.provider);

  @override
  String get sessionApiUrl => (origin as StartSessionProvider).sessionApiUrl;
  @override
  String get rootOrganizationEcPublicKey =>
      (origin as StartSessionProvider).rootOrganizationEcPublicKey;
  @override
  String get organizationOin =>
      (origin as StartSessionProvider).organizationOin;
  @override
  OrganizationType get contactType =>
      (origin as StartSessionProvider).contactType;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
