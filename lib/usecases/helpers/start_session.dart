// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/usecases/helpers/start_session_delegate.dart';

part 'start_session.g.dart';

@riverpod
Future<String> startSession(
  Ref ref, {
  required String sessionApiUrl,
  required String rootOrganizationEcPublicKey,
  required String organizationOin,
  required OrganizationType contactType,
}) async =>
    ref.watch(startSessionDelegateProvider)(
      ref,
      sessionApiUrl,
      rootOrganizationEcPublicKey,
      organizationOin,
      contactType,
    );
