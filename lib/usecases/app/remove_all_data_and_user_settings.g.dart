// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_all_data_and_user_settings.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$removeAllDataAndUserSettingsHash() =>
    r'f890469ff6ff1def8e3d23a896e2c22ad202acb2';

/// See also [removeAllDataAndUserSettings].
@ProviderFor(removeAllDataAndUserSettings)
final removeAllDataAndUserSettingsProvider =
    AutoDisposeFutureProvider<void>.internal(
  removeAllDataAndUserSettings,
  name: r'removeAllDataAndUserSettingsProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$removeAllDataAndUserSettingsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RemoveAllDataAndUserSettingsRef = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
