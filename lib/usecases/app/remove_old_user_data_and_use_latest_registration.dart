// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_inbox/financial_claims_information_inbox_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_storage/financial_claims_information_storage_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/organization_selection/organization_selection_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/revoke_registration_helper.dart';

part 'remove_old_user_data_and_use_latest_registration.g.dart';

@riverpod
Future<void> removeOldUserDataAndUseLatestRegistration(Ref ref) async {
  final registrations = await ref.read(registrationRepositoryProvider.future);

  final latestRegistration = registrations.last;
  try {
    for (var registration in registrations.where((registration) =>
        registration.dateTimeCompleted != null &&
        !registration.revoked &&
        registration.id != latestRegistration.id)) {
      await ref.read(revokeRegistrationHelperProvider(registration).future);
    }
  } catch (_) {
    final loggingHelper = ref.read(loggingProvider);

    loggingHelper.addLog(DeviceEvent.uu_77, "revoking registrations error");
  }

  for (var registration in registrations) {
    if (registration.id == latestRegistration.id) {
      continue;
    }

    await ref
        .read(registrationRepositoryProvider.notifier)
        .deleteRegistration(registration.id);
  }

  await ref
      .read(financialClaimsInformationStorageRepositoryProvider.notifier)
      .clear();

  await ref
      .read(financialClaimsInformationInboxRepositoryProvider.notifier)
      .clear();

  await ref
      .read(financialClaimsInformationRequestRepositoryProvider.notifier)
      .clear();

  await ref.read(organizationSelectionRepositoryProvider.notifier).clear();

  await ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier)
      .clear();

  await ref.read(certificateRepositoryProvider.notifier).clear();

  await ref
      .read(userSettingsRepositoryProvider.notifier)
      .setHasCompletedOrganizationSelection(false);
}
