// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'remove_old_user_data_and_use_latest_registration.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$removeOldUserDataAndUseLatestRegistrationHash() =>
    r'd5f420ec0266ad8096a18b53e9303846ce480da7';

/// See also [removeOldUserDataAndUseLatestRegistration].
@ProviderFor(removeOldUserDataAndUseLatestRegistration)
final removeOldUserDataAndUseLatestRegistrationProvider =
    AutoDisposeFutureProvider<void>.internal(
  removeOldUserDataAndUseLatestRegistration,
  name: r'removeOldUserDataAndUseLatestRegistrationProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$removeOldUserDataAndUseLatestRegistrationHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RemoveOldUserDataAndUseLatestRegistrationRef
    = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
