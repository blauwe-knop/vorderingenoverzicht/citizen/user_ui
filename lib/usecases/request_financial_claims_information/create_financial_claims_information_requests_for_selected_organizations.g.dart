// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_financial_claims_information_requests_for_selected_organizations.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String
    _$createFinancialClaimsInformationRequestsForSelectedOrganizationsHash() =>
        r'3821f0f2802e5212f224e9e3620cd646c74dcd45';

/// See also [createFinancialClaimsInformationRequestsForSelectedOrganizations].
@ProviderFor(createFinancialClaimsInformationRequestsForSelectedOrganizations)
final createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider =
    AutoDisposeFutureProvider<void>.internal(
  createFinancialClaimsInformationRequestsForSelectedOrganizations,
  name:
      r'createFinancialClaimsInformationRequestsForSelectedOrganizationsProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$createFinancialClaimsInformationRequestsForSelectedOrganizationsHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef CreateFinancialClaimsInformationRequestsForSelectedOrganizationsRef
    = AutoDisposeFutureProviderRef<void>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
