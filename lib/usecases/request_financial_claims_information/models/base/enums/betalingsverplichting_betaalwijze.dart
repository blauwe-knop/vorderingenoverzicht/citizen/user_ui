enum BetalingsverplichtingBetaalwijze {
  handmatig("Handmatig"),
  automatischIncasso("Automatische incasso");

  const BetalingsverplichtingBetaalwijze(this.value);

  final String value;
}
