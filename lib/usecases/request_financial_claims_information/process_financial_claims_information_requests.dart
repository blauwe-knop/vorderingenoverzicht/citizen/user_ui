import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/usecases/payment_plan_rules/fetch_and_store_payment_plan_rules.dart';
import 'package:user_ui/usecases/payment_plan_rules/process_payment_plan_rules.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/compare_financial_claims_received_with_latest_storage.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/configure_claims_request.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/copy_financial_claims_information_to_inbox.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_document.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_document_signature.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_encrypted_envelope.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_envelope.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_session_for_app_manager.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/create_session_for_source_organization.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/decrypt_financial_claims_information_document.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/fetch_new_certificate.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/request_financial_claims_information.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/reuse_or_create_configuration.dart';

part 'process_financial_claims_information_requests.g.dart';

@riverpod
Future<void> processFinancialClaimsInformationRequests(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  debugPrint(
      "processFinancialClaimsInformationRequest: ${financialClaimsInformationRequest.id}, ${financialClaimsInformationRequest.status}, ${financialClaimsInformationRequest.lock};");

  final loggingHelper = ref.watch(loggingProvider);

  switch (financialClaimsInformationRequest.status) {
    case FinancialClaimsInformationRequestStatus.queue:
      await ref.read(
          reuseOrCreateConfigurationProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.configurationCreated:
      await ref.read(
          createDocumentProvider(financialClaimsInformationRequest).future);
      break;
    case FinancialClaimsInformationRequestStatus.documentCreated:
      await ref.read(
          createDocumentSignatureProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.documentSigned:
      await ref.read(
          createEnvelopeProvider(financialClaimsInformationRequest).future);
      break;
    case FinancialClaimsInformationRequestStatus.envelopeCreated:
      await ref.read(createSessionForSourceOrganizationProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus.certificateInvalid:
      await ref.read(
          fetchNewCertificateProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.appManagerSessionInvalid:
      await ref.read(
          createSessionForAppManagerProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.sessionCreated:
      await ref.read(
          createEncryptedEnvelopeProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.envelopeEncrypted:
      await ref.read(
          configureClaimsRequestProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.encryptedEnvelopeMailed:
      await ref.read(requestFinancialClaimsInformationProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus
          .financialClaimsInformationReceived:
      await ref.read(decryptFinancialClaimsInformationDocumentProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus
          .financialClaimsInformationDecrypted:
      await ref.read(compareFinancialClaimsReceivedWithLatestStorageProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus
          .financialClaimsInformationChanged:
      await ref.read(copyFinancialClaimsInformationToInboxProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus
          .financialClaimsInformationUnchanged:
    case FinancialClaimsInformationRequestStatus
          .financialClaimsInformationCopiedToInbox:
      await ref.read(fetchAndStorePaymentPlanRulesProvider(
              financialClaimsInformationRequest)
          .future);
      break;
    case FinancialClaimsInformationRequestStatus
          .paymentPlanRulesFetchedAndStored:
      await ref.read(
          processPaymentPlanRulesProvider(financialClaimsInformationRequest)
              .future);
      break;
    case FinancialClaimsInformationRequestStatus.paymentPlanRulesProcessed:
    case FinancialClaimsInformationRequestStatus.cancelled:
      loggingHelper.addLog(DeviceEvent.uu_76,
          "financial claims request with status '${financialClaimsInformationRequest.status.value}' should not be processed.");
      throw Exception(
          "financial claims request with status '${financialClaimsInformationRequest.status.value}' should not be processed.");
  }
}
