// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'process_financial_claims_information_requests.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$processFinancialClaimsInformationRequestsHash() =>
    r'52c56790d23f7f4f8ea4add02bdda885ab12e0c0';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [processFinancialClaimsInformationRequests].
@ProviderFor(processFinancialClaimsInformationRequests)
const processFinancialClaimsInformationRequestsProvider =
    ProcessFinancialClaimsInformationRequestsFamily();

/// See also [processFinancialClaimsInformationRequests].
class ProcessFinancialClaimsInformationRequestsFamily
    extends Family<AsyncValue<void>> {
  /// See also [processFinancialClaimsInformationRequests].
  const ProcessFinancialClaimsInformationRequestsFamily();

  /// See also [processFinancialClaimsInformationRequests].
  ProcessFinancialClaimsInformationRequestsProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return ProcessFinancialClaimsInformationRequestsProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  ProcessFinancialClaimsInformationRequestsProvider getProviderOverride(
    covariant ProcessFinancialClaimsInformationRequestsProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'processFinancialClaimsInformationRequestsProvider';
}

/// See also [processFinancialClaimsInformationRequests].
class ProcessFinancialClaimsInformationRequestsProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [processFinancialClaimsInformationRequests].
  ProcessFinancialClaimsInformationRequestsProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => processFinancialClaimsInformationRequests(
            ref as ProcessFinancialClaimsInformationRequestsRef,
            financialClaimsInformationRequest,
          ),
          from: processFinancialClaimsInformationRequestsProvider,
          name: r'processFinancialClaimsInformationRequestsProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$processFinancialClaimsInformationRequestsHash,
          dependencies:
              ProcessFinancialClaimsInformationRequestsFamily._dependencies,
          allTransitiveDependencies:
              ProcessFinancialClaimsInformationRequestsFamily
                  ._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  ProcessFinancialClaimsInformationRequestsProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(
            ProcessFinancialClaimsInformationRequestsRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ProcessFinancialClaimsInformationRequestsProvider._internal(
        (ref) => create(ref as ProcessFinancialClaimsInformationRequestsRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _ProcessFinancialClaimsInformationRequestsProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ProcessFinancialClaimsInformationRequestsProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ProcessFinancialClaimsInformationRequestsRef
    on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _ProcessFinancialClaimsInformationRequestsProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with ProcessFinancialClaimsInformationRequestsRef {
  _ProcessFinancialClaimsInformationRequestsProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as ProcessFinancialClaimsInformationRequestsProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
