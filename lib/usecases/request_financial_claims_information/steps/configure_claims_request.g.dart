// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'configure_claims_request.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$configureClaimsRequestHash() =>
    r'1831a229a768429c78580f4e90bf158396e15f48';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [configureClaimsRequest].
@ProviderFor(configureClaimsRequest)
const configureClaimsRequestProvider = ConfigureClaimsRequestFamily();

/// See also [configureClaimsRequest].
class ConfigureClaimsRequestFamily extends Family<AsyncValue<void>> {
  /// See also [configureClaimsRequest].
  const ConfigureClaimsRequestFamily();

  /// See also [configureClaimsRequest].
  ConfigureClaimsRequestProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return ConfigureClaimsRequestProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  ConfigureClaimsRequestProvider getProviderOverride(
    covariant ConfigureClaimsRequestProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'configureClaimsRequestProvider';
}

/// See also [configureClaimsRequest].
class ConfigureClaimsRequestProvider extends AutoDisposeFutureProvider<void> {
  /// See also [configureClaimsRequest].
  ConfigureClaimsRequestProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => configureClaimsRequest(
            ref as ConfigureClaimsRequestRef,
            financialClaimsInformationRequest,
          ),
          from: configureClaimsRequestProvider,
          name: r'configureClaimsRequestProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$configureClaimsRequestHash,
          dependencies: ConfigureClaimsRequestFamily._dependencies,
          allTransitiveDependencies:
              ConfigureClaimsRequestFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  ConfigureClaimsRequestProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(ConfigureClaimsRequestRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: ConfigureClaimsRequestProvider._internal(
        (ref) => create(ref as ConfigureClaimsRequestRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _ConfigureClaimsRequestProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is ConfigureClaimsRequestProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin ConfigureClaimsRequestRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _ConfigureClaimsRequestProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with ConfigureClaimsRequestRef {
  _ConfigureClaimsRequestProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as ConfigureClaimsRequestProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
