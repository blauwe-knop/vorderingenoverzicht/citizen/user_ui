import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'decrypt_financial_claims_information_document.g.dart';

@riverpod
Future<void> decryptFinancialClaimsInformationDocument(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 11 decryptFinancialClaimsInformationDocument");
  final financialClaimsInformationReceivedList = await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.future);

  final financialClaimsInformationReceived =
      financialClaimsInformationReceivedList.lastWhere(
    (financialClaimsInformationReceived) =>
        financialClaimsInformationReceived.oin ==
            financialClaimsInformationRequest.oin &&
        financialClaimsInformationReceived.changed == null,
  );

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_60, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  final appSession = await ref.read(
      latestAppSessionProvider(oin: financialClaimsInformationConfiguration.oin)
          .future);

  final aesBytes = Base64.toBytes(appSession.key);

  String? financialClaimsInformationDocument;

  try {
    financialClaimsInformationDocument = utf8.decode(
      Aes.decrypt(
        aesBytes,
        Base64.toBytes(
          financialClaimsInformationReceived
              .encryptedFinancialClaimsInformationDocument,
        ),
      ),
    );
  } catch (ex) {
    // TODO: Add logging
    debugPrint(
        'CRYPTO ERROR: Failed to decrypt financial claims information document. $ex');
    throw Exception('Failed to decrypt financial claims information document');
  }

  final financialClaimsInformationReceivedUpdate =
      financialClaimsInformationReceived.copyWith(
          financialClaimsInformationDocument:
              financialClaimsInformationDocument);

  await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.notifier)
      .updateFinancialClaimsInformationReceived(
          financialClaimsInformationReceivedUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus
              .financialClaimsInformationDecrypted);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
