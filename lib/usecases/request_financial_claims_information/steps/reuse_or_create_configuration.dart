import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'reuse_or_create_configuration.g.dart';

@riverpod
Future<void> reuseOrCreateConfiguration(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 1 createConfiguration");

  final userSettings = await userSettingsRepository.userSettings;
  if (userSettings.delay) {
    await Future.delayed(const Duration(seconds: 3));
  }

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  var status = FinancialClaimsInformationRequestStatus.configurationCreated;

  if (financialClaimsInformationConfiguration != null) {
    if (financialClaimsInformationConfiguration.configuration != null) {
      status = FinancialClaimsInformationRequestStatus.encryptedEnvelopeMailed;
    } else {
      loggingHelper.addLog(
          DeviceEvent.uu_40, "previous request is in progress");
      throw Exception("previous request is in progress");
    }
  } else {
    await financialClaimsInformationConfigurationRepository.add(
      oin: financialClaimsInformationRequest.oin,
    );
  }

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(status: status);

  await financialClaimsInformationRequestRepository
      .update(financialClaimsInformationRequestUpdate);
}
