// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'copy_financial_claims_information_to_inbox.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$copyFinancialClaimsInformationToInboxHash() =>
    r'b971eb0e78ac4f1e0818376fead36129d50fdd02';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [copyFinancialClaimsInformationToInbox].
@ProviderFor(copyFinancialClaimsInformationToInbox)
const copyFinancialClaimsInformationToInboxProvider =
    CopyFinancialClaimsInformationToInboxFamily();

/// See also [copyFinancialClaimsInformationToInbox].
class CopyFinancialClaimsInformationToInboxFamily
    extends Family<AsyncValue<void>> {
  /// See also [copyFinancialClaimsInformationToInbox].
  const CopyFinancialClaimsInformationToInboxFamily();

  /// See also [copyFinancialClaimsInformationToInbox].
  CopyFinancialClaimsInformationToInboxProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return CopyFinancialClaimsInformationToInboxProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  CopyFinancialClaimsInformationToInboxProvider getProviderOverride(
    covariant CopyFinancialClaimsInformationToInboxProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'copyFinancialClaimsInformationToInboxProvider';
}

/// See also [copyFinancialClaimsInformationToInbox].
class CopyFinancialClaimsInformationToInboxProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [copyFinancialClaimsInformationToInbox].
  CopyFinancialClaimsInformationToInboxProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => copyFinancialClaimsInformationToInbox(
            ref as CopyFinancialClaimsInformationToInboxRef,
            financialClaimsInformationRequest,
          ),
          from: copyFinancialClaimsInformationToInboxProvider,
          name: r'copyFinancialClaimsInformationToInboxProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$copyFinancialClaimsInformationToInboxHash,
          dependencies:
              CopyFinancialClaimsInformationToInboxFamily._dependencies,
          allTransitiveDependencies: CopyFinancialClaimsInformationToInboxFamily
              ._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  CopyFinancialClaimsInformationToInboxProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(CopyFinancialClaimsInformationToInboxRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CopyFinancialClaimsInformationToInboxProvider._internal(
        (ref) => create(ref as CopyFinancialClaimsInformationToInboxRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _CopyFinancialClaimsInformationToInboxProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CopyFinancialClaimsInformationToInboxProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CopyFinancialClaimsInformationToInboxRef
    on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _CopyFinancialClaimsInformationToInboxProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with CopyFinancialClaimsInformationToInboxRef {
  _CopyFinancialClaimsInformationToInboxProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as CopyFinancialClaimsInformationToInboxProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
