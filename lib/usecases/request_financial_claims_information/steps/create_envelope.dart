import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'create_envelope.g.dart';

@riverpod
Future<void> createEnvelope(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 4 CreateEnvelope");

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_50, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  if (financialClaimsInformationConfiguration.document == null) {
    loggingHelper.addLog(DeviceEvent.uu_51, "document not found");
    throw Exception("document not found");
  }

  final documentJson =
      json.decode(financialClaimsInformationConfiguration.document!);

  final document = Document.fromJson(documentJson);

  if (financialClaimsInformationConfiguration.documentSignature == null) {
    loggingHelper.addLog(DeviceEvent.uu_52, "documentSignature not found");
    throw Exception("documentSignature not found");
  }

  final envelope = Envelope(
    document: document,
    documentSignature:
        financialClaimsInformationConfiguration.documentSignature!,
  );

  final financialClaimsInformationConfigurationUpdate =
      financialClaimsInformationConfiguration.copyWith(
          envelope: json.encode(envelope));

  await financialClaimsInformationConfigurationRepository
      .update(financialClaimsInformationConfigurationUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.envelopeCreated);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
