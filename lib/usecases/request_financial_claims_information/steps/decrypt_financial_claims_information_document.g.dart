// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'decrypt_financial_claims_information_document.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$decryptFinancialClaimsInformationDocumentHash() =>
    r'8262bba3b09aaccc3392d632f43b96605fe300fa';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [decryptFinancialClaimsInformationDocument].
@ProviderFor(decryptFinancialClaimsInformationDocument)
const decryptFinancialClaimsInformationDocumentProvider =
    DecryptFinancialClaimsInformationDocumentFamily();

/// See also [decryptFinancialClaimsInformationDocument].
class DecryptFinancialClaimsInformationDocumentFamily
    extends Family<AsyncValue<void>> {
  /// See also [decryptFinancialClaimsInformationDocument].
  const DecryptFinancialClaimsInformationDocumentFamily();

  /// See also [decryptFinancialClaimsInformationDocument].
  DecryptFinancialClaimsInformationDocumentProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return DecryptFinancialClaimsInformationDocumentProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  DecryptFinancialClaimsInformationDocumentProvider getProviderOverride(
    covariant DecryptFinancialClaimsInformationDocumentProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'decryptFinancialClaimsInformationDocumentProvider';
}

/// See also [decryptFinancialClaimsInformationDocument].
class DecryptFinancialClaimsInformationDocumentProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [decryptFinancialClaimsInformationDocument].
  DecryptFinancialClaimsInformationDocumentProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => decryptFinancialClaimsInformationDocument(
            ref as DecryptFinancialClaimsInformationDocumentRef,
            financialClaimsInformationRequest,
          ),
          from: decryptFinancialClaimsInformationDocumentProvider,
          name: r'decryptFinancialClaimsInformationDocumentProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$decryptFinancialClaimsInformationDocumentHash,
          dependencies:
              DecryptFinancialClaimsInformationDocumentFamily._dependencies,
          allTransitiveDependencies:
              DecryptFinancialClaimsInformationDocumentFamily
                  ._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  DecryptFinancialClaimsInformationDocumentProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(
            DecryptFinancialClaimsInformationDocumentRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: DecryptFinancialClaimsInformationDocumentProvider._internal(
        (ref) => create(ref as DecryptFinancialClaimsInformationDocumentRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _DecryptFinancialClaimsInformationDocumentProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is DecryptFinancialClaimsInformationDocumentProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin DecryptFinancialClaimsInformationDocumentRef
    on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _DecryptFinancialClaimsInformationDocumentProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with DecryptFinancialClaimsInformationDocumentRef {
  _DecryptFinancialClaimsInformationDocumentProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as DecryptFinancialClaimsInformationDocumentProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
