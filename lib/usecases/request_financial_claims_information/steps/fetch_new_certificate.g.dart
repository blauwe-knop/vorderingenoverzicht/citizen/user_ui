// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_new_certificate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchNewCertificateHash() =>
    r'7ac4f0097db75bbdbd6651c2127d595723da462e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [fetchNewCertificate].
@ProviderFor(fetchNewCertificate)
const fetchNewCertificateProvider = FetchNewCertificateFamily();

/// See also [fetchNewCertificate].
class FetchNewCertificateFamily extends Family<AsyncValue<void>> {
  /// See also [fetchNewCertificate].
  const FetchNewCertificateFamily();

  /// See also [fetchNewCertificate].
  FetchNewCertificateProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return FetchNewCertificateProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  FetchNewCertificateProvider getProviderOverride(
    covariant FetchNewCertificateProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'fetchNewCertificateProvider';
}

/// See also [fetchNewCertificate].
class FetchNewCertificateProvider extends AutoDisposeFutureProvider<void> {
  /// See also [fetchNewCertificate].
  FetchNewCertificateProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => fetchNewCertificate(
            ref as FetchNewCertificateRef,
            financialClaimsInformationRequest,
          ),
          from: fetchNewCertificateProvider,
          name: r'fetchNewCertificateProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$fetchNewCertificateHash,
          dependencies: FetchNewCertificateFamily._dependencies,
          allTransitiveDependencies:
              FetchNewCertificateFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  FetchNewCertificateProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(FetchNewCertificateRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: FetchNewCertificateProvider._internal(
        (ref) => create(ref as FetchNewCertificateRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _FetchNewCertificateProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is FetchNewCertificateProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin FetchNewCertificateRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _FetchNewCertificateProviderElement
    extends AutoDisposeFutureProviderElement<void> with FetchNewCertificateRef {
  _FetchNewCertificateProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as FetchNewCertificateProvider).financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
