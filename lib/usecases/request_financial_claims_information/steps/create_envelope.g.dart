// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_envelope.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$createEnvelopeHash() => r'5f0d5674bbf87c707d03dfc3a399a24b9db88b04';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [createEnvelope].
@ProviderFor(createEnvelope)
const createEnvelopeProvider = CreateEnvelopeFamily();

/// See also [createEnvelope].
class CreateEnvelopeFamily extends Family<AsyncValue<void>> {
  /// See also [createEnvelope].
  const CreateEnvelopeFamily();

  /// See also [createEnvelope].
  CreateEnvelopeProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return CreateEnvelopeProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  CreateEnvelopeProvider getProviderOverride(
    covariant CreateEnvelopeProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createEnvelopeProvider';
}

/// See also [createEnvelope].
class CreateEnvelopeProvider extends AutoDisposeFutureProvider<void> {
  /// See also [createEnvelope].
  CreateEnvelopeProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => createEnvelope(
            ref as CreateEnvelopeRef,
            financialClaimsInformationRequest,
          ),
          from: createEnvelopeProvider,
          name: r'createEnvelopeProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createEnvelopeHash,
          dependencies: CreateEnvelopeFamily._dependencies,
          allTransitiveDependencies:
              CreateEnvelopeFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  CreateEnvelopeProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(CreateEnvelopeRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CreateEnvelopeProvider._internal(
        (ref) => create(ref as CreateEnvelopeRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _CreateEnvelopeProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateEnvelopeProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CreateEnvelopeRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _CreateEnvelopeProviderElement
    extends AutoDisposeFutureProviderElement<void> with CreateEnvelopeRef {
  _CreateEnvelopeProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as CreateEnvelopeProvider).financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
