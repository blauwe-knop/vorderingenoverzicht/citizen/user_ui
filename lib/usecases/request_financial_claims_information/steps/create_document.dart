import 'dart:convert';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'create_document.g.dart';

@riverpod
Future<void> createDocument(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);

  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 2 CreateDocument");

  final userSettings = await userSettingsRepository.userSettings;
  if (userSettings.delay) {
    await Future.delayed(const Duration(seconds: 3));
  }

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_41, "Financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  const document = Document(
    type: DocumentType(
      name: "FINANCIAL_CLAIMS_INFORMATION_REQUEST",
    ),
  );

  final financialClaimsInformationConfigurationUpdate =
      financialClaimsInformationConfiguration.copyWith(
          document: json.encode(document));

  await financialClaimsInformationConfigurationRepository
      .update(financialClaimsInformationConfigurationUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.documentCreated);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
