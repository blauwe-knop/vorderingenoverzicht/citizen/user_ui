// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'request_financial_claims_information.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$requestFinancialClaimsInformationHash() =>
    r'49b0481f66c5723bf28b62767300c2d5ac532e9e';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [requestFinancialClaimsInformation].
@ProviderFor(requestFinancialClaimsInformation)
const requestFinancialClaimsInformationProvider =
    RequestFinancialClaimsInformationFamily();

/// See also [requestFinancialClaimsInformation].
class RequestFinancialClaimsInformationFamily extends Family<AsyncValue<void>> {
  /// See also [requestFinancialClaimsInformation].
  const RequestFinancialClaimsInformationFamily();

  /// See also [requestFinancialClaimsInformation].
  RequestFinancialClaimsInformationProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return RequestFinancialClaimsInformationProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  RequestFinancialClaimsInformationProvider getProviderOverride(
    covariant RequestFinancialClaimsInformationProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'requestFinancialClaimsInformationProvider';
}

/// See also [requestFinancialClaimsInformation].
class RequestFinancialClaimsInformationProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [requestFinancialClaimsInformation].
  RequestFinancialClaimsInformationProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => requestFinancialClaimsInformation(
            ref as RequestFinancialClaimsInformationRef,
            financialClaimsInformationRequest,
          ),
          from: requestFinancialClaimsInformationProvider,
          name: r'requestFinancialClaimsInformationProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$requestFinancialClaimsInformationHash,
          dependencies: RequestFinancialClaimsInformationFamily._dependencies,
          allTransitiveDependencies: RequestFinancialClaimsInformationFamily
              ._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  RequestFinancialClaimsInformationProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(RequestFinancialClaimsInformationRef provider)
        create,
  ) {
    return ProviderOverride(
      origin: this,
      override: RequestFinancialClaimsInformationProvider._internal(
        (ref) => create(ref as RequestFinancialClaimsInformationRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _RequestFinancialClaimsInformationProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is RequestFinancialClaimsInformationProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin RequestFinancialClaimsInformationRef
    on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _RequestFinancialClaimsInformationProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with RequestFinancialClaimsInformationRef {
  _RequestFinancialClaimsInformationProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as RequestFinancialClaimsInformationProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
