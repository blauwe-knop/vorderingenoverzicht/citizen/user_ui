import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/latest_usable_registration_notifier.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper.dart';

part 'fetch_new_certificate.g.dart';

@riverpod
Future<void> fetchNewCertificate(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 6 fetchNewCertificate");

  final latestUsableRegistration =
      await ref.read(latestUsableRegistrationProvider.future);

  try {
    await ref.read(fetchAndStoreCertificateHelperProvider(
            registration: latestUsableRegistration)
        .future);
  } on RegistrationExpiredException catch (_) {
    loggingHelper.addLog(DeviceEvent.uu_80,
        "registration expired when fetching new certificate");
    return;
  } on SessionExpiredException catch (_) {
    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .appManagerSessionInvalid);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );

    return;
  }

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.envelopeCreated);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
