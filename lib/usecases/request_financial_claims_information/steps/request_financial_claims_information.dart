import 'dart:math';

import 'package:fcid_library/fcid_library.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_received/financial_claims_information_received_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';

part 'request_financial_claims_information.g.dart';

@riverpod
Future<void> requestFinancialClaimsInformation(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final serviceDiscoveryClient = ref.read(serviceDiscoveryClientProvider);
  final citizenFinancialClaimClient =
      ref.read(citizenFinancialClaimClientProvider);

  final schemeOrganizationRepository =
      ref.read(schemeOrganizationRepositoryProvider.notifier);

  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final userSettingsRepository =
      ref.read(userSettingsRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 10 RequestFinancialClaimsInformation");

  final userSettings = await userSettingsRepository.userSettings;
  if (userSettings.delay) {
    final random = Random();
    await Future.delayed(Duration(seconds: 1 + random.nextInt(6)));
  }

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_60, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  if (financialClaimsInformationConfiguration.configuration == null) {
    loggingHelper.addLog(DeviceEvent.uu_61, "configuration not found");
    throw Exception("configuration not found");
  }

  final organizations = await schemeOrganizationRepository.organizations;

  // TODO: what if the organization is no longer in the scheme
  final organization = organizations.firstWhere(
      (SchemeOrganization l) => l.oin == financialClaimsInformationRequest.oin);

  final discoveredServices = await serviceDiscoveryClient
      .fetchDiscoveredServices(organization.discoveryUrl);

  final appSession =
      await ref.read(latestAppSessionProvider(oin: organization.oin).future);

  String encryptedFinancialClaimsInformationDocument;
  try {
    encryptedFinancialClaimsInformationDocument =
        await citizenFinancialClaimClient.requestFinancialClaimsInformation(
      discoveredServices.getFinancialClaimRequestApiV3(loggingHelper),
      appSession.token,
      financialClaimsInformationConfiguration.configuration!,
    );
  } on SessionExpiredException catch (_) {
    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus
                .appManagerSessionInvalid);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );

    return;
  } catch (ex) {
    final financialClaimsInformationRequestUpdate =
        financialClaimsInformationRequest.copyWith(
            status: FinancialClaimsInformationRequestStatus.envelopeEncrypted);

    await financialClaimsInformationRequestRepository.update(
      financialClaimsInformationRequestUpdate,
    );
    return;
  }

  final dateTimeReceived = DateTime.now();

  await ref
      .read(financialClaimsInformationReceivedRepositoryProvider.notifier)
      .add(
        oin: financialClaimsInformationRequest.oin,
        dateTimeRequested: financialClaimsInformationRequest.dateTimeRequested,
        dateTimeReceived: dateTimeReceived,
        encryptedFinancialClaimsInformationDocument:
            encryptedFinancialClaimsInformationDocument,
      );

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus
              .financialClaimsInformationReceived);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
