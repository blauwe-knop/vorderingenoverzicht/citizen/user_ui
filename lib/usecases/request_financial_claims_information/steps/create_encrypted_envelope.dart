import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'create_encrypted_envelope.g.dart';

@riverpod
Future<void> createEncryptedEnvelope(Ref ref,
    FinancialClaimsInformationRequest financialClaimsInformationRequest) async {
  final financialClaimsInformationRequestRepository =
      ref.read(financialClaimsInformationRequestRepositoryProvider.notifier);

  final financialClaimsInformationConfigurationRepository = ref
      .read(financialClaimsInformationConfigurationRepositoryProvider.notifier);

  final loggingHelper = ref.watch(loggingProvider);
  debugPrint(
      "     ${financialClaimsInformationRequest.id} Step: 8 createEncryptedEnvelope");

  final financialClaimsInformationConfiguration =
      await financialClaimsInformationConfigurationRepository
          .getUnexpiredConfigurationForOin(
              financialClaimsInformationRequest.oin);

  if (financialClaimsInformationConfiguration == null) {
    loggingHelper.addLog(
        DeviceEvent.uu_55, "financial claims configuration not found");
    throw Exception("financial claims configuration not found");
  }

  if (financialClaimsInformationConfiguration.envelope == null) {
    loggingHelper.addLog(DeviceEvent.uu_56, "envelope not found");
    throw Exception("envelope not found");
  }

  final appSession = await ref.read(
      latestAppSessionProvider(oin: financialClaimsInformationConfiguration.oin)
          .future);

  String? encryptedEnvelope;
  final aesKeyBytes = Base64.toBytes(appSession.key);

  try {
    encryptedEnvelope = Base64.parseFromBytes(
      Aes.encrypt(
        aesKeyBytes,
        utf8.encode(
          financialClaimsInformationConfiguration.envelope!,
        ),
      ),
    );
  } catch (ex) {
    // TODO: Add logging
    debugPrint('CRYPTO ERROR: Failed to encrypt envelope. $ex');
    throw Exception('Failed to encrypt envelope');
  }

  final financialClaimsInformationConfigurationUpdate =
      financialClaimsInformationConfiguration.copyWith(
          encryptedEnvelope: encryptedEnvelope);

  await financialClaimsInformationConfigurationRepository
      .update(financialClaimsInformationConfigurationUpdate);

  final financialClaimsInformationRequestUpdate =
      financialClaimsInformationRequest.copyWith(
          status: FinancialClaimsInformationRequestStatus.envelopeEncrypted);

  await financialClaimsInformationRequestRepository.update(
    financialClaimsInformationRequestUpdate,
  );
}
