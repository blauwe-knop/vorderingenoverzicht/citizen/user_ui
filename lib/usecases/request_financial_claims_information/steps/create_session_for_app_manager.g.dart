// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_session_for_app_manager.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$createSessionForAppManagerHash() =>
    r'93fff0c42cf856f06d1a8414789e33c88698b453';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [createSessionForAppManager].
@ProviderFor(createSessionForAppManager)
const createSessionForAppManagerProvider = CreateSessionForAppManagerFamily();

/// See also [createSessionForAppManager].
class CreateSessionForAppManagerFamily extends Family<AsyncValue<void>> {
  /// See also [createSessionForAppManager].
  const CreateSessionForAppManagerFamily();

  /// See also [createSessionForAppManager].
  CreateSessionForAppManagerProvider call(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) {
    return CreateSessionForAppManagerProvider(
      financialClaimsInformationRequest,
    );
  }

  @override
  CreateSessionForAppManagerProvider getProviderOverride(
    covariant CreateSessionForAppManagerProvider provider,
  ) {
    return call(
      provider.financialClaimsInformationRequest,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'createSessionForAppManagerProvider';
}

/// See also [createSessionForAppManager].
class CreateSessionForAppManagerProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [createSessionForAppManager].
  CreateSessionForAppManagerProvider(
    FinancialClaimsInformationRequest financialClaimsInformationRequest,
  ) : this._internal(
          (ref) => createSessionForAppManager(
            ref as CreateSessionForAppManagerRef,
            financialClaimsInformationRequest,
          ),
          from: createSessionForAppManagerProvider,
          name: r'createSessionForAppManagerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$createSessionForAppManagerHash,
          dependencies: CreateSessionForAppManagerFamily._dependencies,
          allTransitiveDependencies:
              CreateSessionForAppManagerFamily._allTransitiveDependencies,
          financialClaimsInformationRequest: financialClaimsInformationRequest,
        );

  CreateSessionForAppManagerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.financialClaimsInformationRequest,
  }) : super.internal();

  final FinancialClaimsInformationRequest financialClaimsInformationRequest;

  @override
  Override overrideWith(
    FutureOr<void> Function(CreateSessionForAppManagerRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: CreateSessionForAppManagerProvider._internal(
        (ref) => create(ref as CreateSessionForAppManagerRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        financialClaimsInformationRequest: financialClaimsInformationRequest,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _CreateSessionForAppManagerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is CreateSessionForAppManagerProvider &&
        other.financialClaimsInformationRequest ==
            financialClaimsInformationRequest;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash =
        _SystemHash.combine(hash, financialClaimsInformationRequest.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin CreateSessionForAppManagerRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `financialClaimsInformationRequest` of this provider.
  FinancialClaimsInformationRequest get financialClaimsInformationRequest;
}

class _CreateSessionForAppManagerProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with CreateSessionForAppManagerRef {
  _CreateSessionForAppManagerProviderElement(super.provider);

  @override
  FinancialClaimsInformationRequest get financialClaimsInformationRequest =>
      (origin as CreateSessionForAppManagerProvider)
          .financialClaimsInformationRequest;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
