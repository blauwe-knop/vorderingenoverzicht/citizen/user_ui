// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generate_and_store_session_key.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$generateAndStoreSessionKeyHash() =>
    r'cd20f9f2773b37350e26868b164a63817fd00693';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

/// See also [generateAndStoreSessionKey].
@ProviderFor(generateAndStoreSessionKey)
const generateAndStoreSessionKeyProvider = GenerateAndStoreSessionKeyFamily();

/// See also [generateAndStoreSessionKey].
class GenerateAndStoreSessionKeyFamily extends Family<AsyncValue<void>> {
  /// See also [generateAndStoreSessionKey].
  const GenerateAndStoreSessionKeyFamily();

  /// See also [generateAndStoreSessionKey].
  GenerateAndStoreSessionKeyProvider call({
    required String oin,
  }) {
    return GenerateAndStoreSessionKeyProvider(
      oin: oin,
    );
  }

  @override
  GenerateAndStoreSessionKeyProvider getProviderOverride(
    covariant GenerateAndStoreSessionKeyProvider provider,
  ) {
    return call(
      oin: provider.oin,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'generateAndStoreSessionKeyProvider';
}

/// See also [generateAndStoreSessionKey].
class GenerateAndStoreSessionKeyProvider
    extends AutoDisposeFutureProvider<void> {
  /// See also [generateAndStoreSessionKey].
  GenerateAndStoreSessionKeyProvider({
    required String oin,
  }) : this._internal(
          (ref) => generateAndStoreSessionKey(
            ref as GenerateAndStoreSessionKeyRef,
            oin: oin,
          ),
          from: generateAndStoreSessionKeyProvider,
          name: r'generateAndStoreSessionKeyProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$generateAndStoreSessionKeyHash,
          dependencies: GenerateAndStoreSessionKeyFamily._dependencies,
          allTransitiveDependencies:
              GenerateAndStoreSessionKeyFamily._allTransitiveDependencies,
          oin: oin,
        );

  GenerateAndStoreSessionKeyProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.oin,
  }) : super.internal();

  final String oin;

  @override
  Override overrideWith(
    FutureOr<void> Function(GenerateAndStoreSessionKeyRef provider) create,
  ) {
    return ProviderOverride(
      origin: this,
      override: GenerateAndStoreSessionKeyProvider._internal(
        (ref) => create(ref as GenerateAndStoreSessionKeyRef),
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        oin: oin,
      ),
    );
  }

  @override
  AutoDisposeFutureProviderElement<void> createElement() {
    return _GenerateAndStoreSessionKeyProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is GenerateAndStoreSessionKeyProvider && other.oin == oin;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, oin.hashCode);

    return _SystemHash.finish(hash);
  }
}

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
mixin GenerateAndStoreSessionKeyRef on AutoDisposeFutureProviderRef<void> {
  /// The parameter `oin` of this provider.
  String get oin;
}

class _GenerateAndStoreSessionKeyProviderElement
    extends AutoDisposeFutureProviderElement<void>
    with GenerateAndStoreSessionKeyRef {
  _GenerateAndStoreSessionKeyProviderElement(super.provider);

  @override
  String get oin => (origin as GenerateAndStoreSessionKeyProvider).oin;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
