import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/usecases/app_identity/generate_and_store_session_key_delegate.dart';

part 'generate_and_store_session_key.g.dart';

@riverpod
Future<void> generateAndStoreSessionKey(
  Ref ref, {
  required String oin,
}) async =>
    ref.watch(generateAndStoreSessionKeyDelegateProvider)(
      ref,
      oin,
    );
