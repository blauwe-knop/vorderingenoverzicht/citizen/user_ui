// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:is_lock_screen/is_lock_screen.dart';
import 'package:universal_html/html.dart' show Event, window;
import 'package:user_ui/repositories/logging/encrypted_logging_repository.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/router/router.dart';
import 'package:user_ui/screens/common/development_overlay_widget.dart';
import 'package:user_ui/screens/common/themed_snack_bar.dart';
import 'package:user_ui/screens/features/error/error_handler_widget.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen.dart';
import 'package:user_ui/screens/features/pincode/pincode_screen_state.dart';
import 'package:user_ui/theme/responsive_grid.dart';
import 'package:user_ui/theme/theme_manager.dart';
import 'package:user_ui/utils/custom_scroll_behavior.dart';

class App extends ConsumerStatefulWidget {
  const App({super.key});

  @override
  ConsumerState<App> createState() => _AppState();
}

class _AppState extends ConsumerState<App> with WidgetsBindingObserver {
  Timer? _timer;
  final int _timerDurationInSeconds = 10;

  @override
  void initState() {
    super.initState();
    if (kIsWeb) {
      window.addEventListener('focus', _onFocus);
      window.addEventListener('blur', _onBlur);
    } else {
      WidgetsBinding.instance.addObserver(this);
    }
  }

  @override
  void dispose() {
    if (kIsWeb) {
      window.removeEventListener('focus', _onFocus);
      window.removeEventListener('blur', _onBlur);
    } else {
      WidgetsBinding.instance.removeObserver(this);
    }
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    final pincodeState =
        await ref.read(pincodeScreenStateNotifierProvider.future);
    final userSettings = ref.read(userSettingsRepositoryProvider);
    final isUnlocked = pincodeState == PincodeScreenState.unlocked;
    final hasCompletedActivation = userSettings.hasCompletedRegistration;

    debugPrint('App lifecycle state: ${state.name}');

    switch (state) {
      case AppLifecycleState.hidden:
      case AppLifecycleState.paused:
      case AppLifecycleState.inactive:
        if (isUnlocked && hasCompletedActivation) {
          final bool? screenLocked = await isLockScreen();
          if (screenLocked != null && screenLocked) {
            _lockTheApp();
          } else {
            _startTimer();
          }
        }
      case AppLifecycleState.detached:
        if (isUnlocked && hasCompletedActivation) {
          _lockTheApp();
        }
      case AppLifecycleState.resumed:
        _cancelTimer();
    }
  }

  void _cancelTimer() {
    _timer?.cancel();
    _timer = null;
  }

  void _startTimer() {
    _timer?.cancel();
    _timer = Timer(Duration(seconds: _timerDurationInSeconds), () {
      _lockTheApp();
    });
  }

  void _lockTheApp() {
    debugPrint('Locked the app');
    ref
        .read(pincodeScreenStateNotifierProvider.notifier)
        .setState(PincodeScreenState.locked);

    final goRouter = ref.watch(goRouterProvider);
    if (mounted) {
      goRouter.goNamed(PincodeScreen.routeName);
    }
  }

  void _onFocus(Event e) {
    didChangeAppLifecycleState(AppLifecycleState.resumed);
  }

  void _onBlur(Event e) {
    didChangeAppLifecycleState(AppLifecycleState.paused);
  }

  @override
  Widget build(BuildContext context) {
    initializeDateFormatting();

    final goRouter = ref.watch(goRouterProvider);

    final bool isSmallScreenSize =
        responsiveValue(context, xs: true, sm: false);

    final scaffoldMessengerKey = GlobalKey<ScaffoldMessengerState>();

    ref.listen(encryptedLoggingRepositoryProvider, (previous, next) {
      if (next.length > previous!.length &&
          ref.read(userSettingsRepositoryProvider).showCriticalErrorAlerts) {
        final ScaffoldMessengerState? scaffold =
            scaffoldMessengerKey.currentState;

        final snackBar =
            createSnackBar(next.first, context, goRouter, scaffold);

        scaffold!.showSnackBar(snackBar);
      }
    });

    return MaterialApp.router(
      title: 'Financiële verplichtingen',
      scaffoldMessengerKey: scaffoldMessengerKey,
      debugShowCheckedModeBanner: false,
      theme: ThemeManager.light(isSmallScreenSize),
      routerConfig: goRouter,
      scrollBehavior: CustomScrollBehavior(),
      builder: (context, child) => ErrorHandlerWidget(
        child: ref.watch(userSettingsRepositoryProvider).showDevelopmentOverlay
            ? DevelopmentOverlayWidget(
                children: [child!],
              )
            : child!,
      ),
    );
  }
}
