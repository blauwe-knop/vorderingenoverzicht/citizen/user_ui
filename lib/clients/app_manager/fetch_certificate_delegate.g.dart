// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fetch_certificate_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$fetchCertificateDelegateHash() =>
    r'6a41356f18d2a444051228d24d5b5d5348091390';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
///
/// Copied from [fetchCertificateDelegate].
@ProviderFor(fetchCertificateDelegate)
final fetchCertificateDelegateProvider = AutoDisposeProvider<
    Future<String> Function(Ref ref, String serviceUrl, String sessionToken,
        String registrationToken)>.internal(
  fetchCertificateDelegate,
  name: r'fetchCertificateDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$fetchCertificateDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef FetchCertificateDelegateRef = AutoDisposeProviderRef<
    Future<String> Function(Ref ref, String serviceUrl, String sessionToken,
        String registrationToken)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
