// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'dart:async';

import 'package:user_ui/clients/app_manager/fetch_certificate_delegate.dart';

part 'fetch_certificate.g.dart';

@riverpod
Future<String> fetchCertificate(
  Ref ref, {
  required String serviceUrl,
  required String sessionToken,
  required String registrationToken,
}) async =>
    ref.watch(fetchCertificateDelegateProvider)(
      ref,
      serviceUrl,
      sessionToken,
      registrationToken,
    );
