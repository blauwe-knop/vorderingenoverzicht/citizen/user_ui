// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'dart:async';
import 'dart:convert';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/clients/session_expired_exception.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';

part 'fetch_certificate_delegate.g.dart';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
@riverpod
Future<String> Function(
  Ref ref,
  String serviceUrl,
  String sessionToken,
  String registrationToken,
) fetchCertificateDelegate(Ref _) {
  return (
    ref,
    serviceUrl,
    sessionToken,
    registrationToken,
  ) async {
    final loggingHelper = ref.read(loggingProvider);
    final fetchRegistrationUri = Uri.parse("$serviceUrl/fetch_certificate");

    final uri = fetchRegistrationUri
        .replace(queryParameters: {"registrationToken": registrationToken});

    final response = await http.post(
      uri,
      headers: {
        "Authorization": sessionToken,
      },
    );

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      if (response.body.contains("session expired")) {
        loggingHelper.addLog(
            DeviceEvent.uu_2, "Failed to fetch certificate - sessions expired");
        throw SessionExpiredException(
            'Failed to fetch certificate - session expired');
      }

      if (response.body.contains("registration expired")) {
        loggingHelper.addLog(DeviceEvent.uu_3,
            "Failed to fetch certificate - registration expired");
        throw RegistrationExpiredException(
            'Failed to fetch certificate - registration expired');
      }
      loggingHelper.addLog(DeviceEvent.uu_4, "Failed to fetch certificate");
      throw Exception('Failed to fetch certificate');
    }
  };
}
