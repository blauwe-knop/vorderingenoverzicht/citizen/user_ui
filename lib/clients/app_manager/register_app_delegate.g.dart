// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'register_app_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$registerAppDelegateHash() =>
    r'56cdcd4b525d49e8ef01eb961697ed87e3463562';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
///
/// Copied from [registerAppDelegate].
@ProviderFor(registerAppDelegate)
final registerAppDelegateProvider = AutoDisposeProvider<
    Future<String> Function(Ref ref, String serviceUrl, String sessionToken,
        Base64String encryptedAppIdentity)>.internal(
  registerAppDelegate,
  name: r'registerAppDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$registerAppDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef RegisterAppDelegateRef = AutoDisposeProviderRef<
    Future<String> Function(Ref ref, String serviceUrl, String sessionToken,
        Base64String encryptedAppIdentity)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
