// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/register_app_delegate.dart';

part 'register_app.g.dart';

@riverpod
Future<String> registerApp(
  Ref ref, {
  required String serviceUrl,
  required String sessionToken,
  required Base64String encryptedAppIdentity,
}) async =>
    ref.watch(registerAppDelegateProvider)(
      ref,
      serviceUrl,
      sessionToken,
      encryptedAppIdentity,
    );
