// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class RegistrationExpiredException implements Exception {
  String cause;
  RegistrationExpiredException(this.cause);
}
