// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unregister_app_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$UnregisterAppRequestImpl _$$UnregisterAppRequestImplFromJson(
        Map<String, dynamic> json) =>
    _$UnregisterAppRequestImpl(
      registrationToken: json['registrationToken'] as String,
      signature: json['signature'] as String,
    );

Map<String, dynamic> _$$UnregisterAppRequestImplToJson(
        _$UnregisterAppRequestImpl instance) =>
    <String, dynamic>{
      'registrationToken': instance.registrationToken,
      'signature': instance.signature,
    };
