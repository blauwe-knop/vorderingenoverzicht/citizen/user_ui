// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'unregister_app_request.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

UnregisterAppRequest _$UnregisterAppRequestFromJson(Map<String, dynamic> json) {
  return _UnregisterAppRequest.fromJson(json);
}

/// @nodoc
mixin _$UnregisterAppRequest {
  String get registrationToken => throw _privateConstructorUsedError;
  String get signature => throw _privateConstructorUsedError;

  /// Serializes this UnregisterAppRequest to a JSON map.
  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;

  /// Create a copy of UnregisterAppRequest
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  $UnregisterAppRequestCopyWith<UnregisterAppRequest> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnregisterAppRequestCopyWith<$Res> {
  factory $UnregisterAppRequestCopyWith(UnregisterAppRequest value,
          $Res Function(UnregisterAppRequest) then) =
      _$UnregisterAppRequestCopyWithImpl<$Res, UnregisterAppRequest>;
  @useResult
  $Res call({String registrationToken, String signature});
}

/// @nodoc
class _$UnregisterAppRequestCopyWithImpl<$Res,
        $Val extends UnregisterAppRequest>
    implements $UnregisterAppRequestCopyWith<$Res> {
  _$UnregisterAppRequestCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  /// Create a copy of UnregisterAppRequest
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? registrationToken = null,
    Object? signature = null,
  }) {
    return _then(_value.copyWith(
      registrationToken: null == registrationToken
          ? _value.registrationToken
          : registrationToken // ignore: cast_nullable_to_non_nullable
              as String,
      signature: null == signature
          ? _value.signature
          : signature // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$UnregisterAppRequestImplCopyWith<$Res>
    implements $UnregisterAppRequestCopyWith<$Res> {
  factory _$$UnregisterAppRequestImplCopyWith(_$UnregisterAppRequestImpl value,
          $Res Function(_$UnregisterAppRequestImpl) then) =
      __$$UnregisterAppRequestImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String registrationToken, String signature});
}

/// @nodoc
class __$$UnregisterAppRequestImplCopyWithImpl<$Res>
    extends _$UnregisterAppRequestCopyWithImpl<$Res, _$UnregisterAppRequestImpl>
    implements _$$UnregisterAppRequestImplCopyWith<$Res> {
  __$$UnregisterAppRequestImplCopyWithImpl(_$UnregisterAppRequestImpl _value,
      $Res Function(_$UnregisterAppRequestImpl) _then)
      : super(_value, _then);

  /// Create a copy of UnregisterAppRequest
  /// with the given fields replaced by the non-null parameter values.
  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? registrationToken = null,
    Object? signature = null,
  }) {
    return _then(_$UnregisterAppRequestImpl(
      registrationToken: null == registrationToken
          ? _value.registrationToken
          : registrationToken // ignore: cast_nullable_to_non_nullable
              as String,
      signature: null == signature
          ? _value.signature
          : signature // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$UnregisterAppRequestImpl
    with DiagnosticableTreeMixin
    implements _UnregisterAppRequest {
  const _$UnregisterAppRequestImpl(
      {required this.registrationToken, required this.signature});

  factory _$UnregisterAppRequestImpl.fromJson(Map<String, dynamic> json) =>
      _$$UnregisterAppRequestImplFromJson(json);

  @override
  final String registrationToken;
  @override
  final String signature;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'UnregisterAppRequest(registrationToken: $registrationToken, signature: $signature)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'UnregisterAppRequest'))
      ..add(DiagnosticsProperty('registrationToken', registrationToken))
      ..add(DiagnosticsProperty('signature', signature));
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$UnregisterAppRequestImpl &&
            (identical(other.registrationToken, registrationToken) ||
                other.registrationToken == registrationToken) &&
            (identical(other.signature, signature) ||
                other.signature == signature));
  }

  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  int get hashCode => Object.hash(runtimeType, registrationToken, signature);

  /// Create a copy of UnregisterAppRequest
  /// with the given fields replaced by the non-null parameter values.
  @JsonKey(includeFromJson: false, includeToJson: false)
  @override
  @pragma('vm:prefer-inline')
  _$$UnregisterAppRequestImplCopyWith<_$UnregisterAppRequestImpl>
      get copyWith =>
          __$$UnregisterAppRequestImplCopyWithImpl<_$UnregisterAppRequestImpl>(
              this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$UnregisterAppRequestImplToJson(
      this,
    );
  }
}

abstract class _UnregisterAppRequest implements UnregisterAppRequest {
  const factory _UnregisterAppRequest(
      {required final String registrationToken,
      required final String signature}) = _$UnregisterAppRequestImpl;

  factory _UnregisterAppRequest.fromJson(Map<String, dynamic> json) =
      _$UnregisterAppRequestImpl.fromJson;

  @override
  String get registrationToken;
  @override
  String get signature;

  /// Create a copy of UnregisterAppRequest
  /// with the given fields replaced by the non-null parameter values.
  @override
  @JsonKey(includeFromJson: false, includeToJson: false)
  _$$UnregisterAppRequestImplCopyWith<_$UnregisterAppRequestImpl>
      get copyWith => throw _privateConstructorUsedError;
}
