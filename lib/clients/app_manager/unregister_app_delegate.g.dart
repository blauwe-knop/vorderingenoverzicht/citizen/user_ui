// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unregister_app_delegate.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$unregisterAppDelegateHash() =>
    r'404338ff97e4461b3a7d9c13e39a27aedafdf0d0';

/// This delegate is needed because family providers are not mockable in Riverpod 2.0.
/// see: https://github.com/rrousselGit/riverpod/discussions/2510
/// and also: https://github.com/rrousselGit/riverpod/issues/3009
///
/// Copied from [unregisterAppDelegate].
@ProviderFor(unregisterAppDelegate)
final unregisterAppDelegateProvider = AutoDisposeProvider<
    Future<void> Function(Ref ref, String serviceUrl, String sessionToken,
        UnregisterAppRequest unregisterAppRequest)>.internal(
  unregisterAppDelegate,
  name: r'unregisterAppDelegateProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$unregisterAppDelegateHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

@Deprecated('Will be removed in 3.0. Use Ref instead')
// ignore: unused_element
typedef UnregisterAppDelegateRef = AutoDisposeProviderRef<
    Future<void> Function(Ref ref, String serviceUrl, String sessionToken,
        UnregisterAppRequest unregisterAppRequest)>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member, deprecated_member_use_from_same_package
