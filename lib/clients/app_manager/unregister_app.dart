// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/app_manager/unregister_app_delegate.dart';

part 'unregister_app.g.dart';

@riverpod
Future<void> unregisterApp(
  Ref ref, {
  required String serviceUrl,
  required String sessionToken,
  required UnregisterAppRequest unregisterAppRequest,
}) async =>
    ref.watch(unregisterAppDelegateProvider)(
      ref,
      serviceUrl,
      sessionToken,
      unregisterAppRequest,
    );
