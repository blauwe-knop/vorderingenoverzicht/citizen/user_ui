// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

import 'dart:async';
import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart' as jwt;
import 'package:flutter/services.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:user_ui/clients/app_manager/fetch_certificate.dart';
import 'package:user_ui/clients/app_manager/models/unregister_app_request.dart';
import 'package:user_ui/clients/app_manager/register_app_delegate.dart';
import 'package:user_ui/clients/app_manager/unregister_app.dart';

Future<List<Override>> appManagerClientMockOverrides() async {
  final appKeyJson =
      jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));

  final appManagerKeyJson = jsonDecode(
      await rootBundle.loadString('assets/mock_app_manager_keys.json'));

  final fetchCertificateResponse =
      await fakeFetchCertificate(appKeyJson, appManagerKeyJson);

  return [
    registerAppDelegateProvider.overrideWith(
      (ref) => (
        ref,
        serviceUrl,
        sessionToken,
        encryptedAppIdentity,
      ) async {
        final aesKey = appKeyJson["aesKey"];
        final aesKeyBytes = Base64.toBytes(aesKey);

        final encryptedToken = Base64.parseFromBytes(
          Aes.encrypt(
            aesKeyBytes,
            utf8.encode('token'),
          ),
        );
        return Future.value(encryptedToken);
      },
    ),
    fetchCertificateProvider(
      serviceUrl: "http://session-api.external/v1",
      sessionToken: "token",
      registrationToken: "registrationToken",
    ).overrideWith(
      (_) => fetchCertificateResponse,
    ),
    unregisterAppProvider(
      serviceUrl: "serviceUrl",
      sessionToken: "sessionToken",
      unregisterAppRequest: const UnregisterAppRequest(
        registrationToken: "registrationToken",
        signature: "signature",
      ),
    ).overrideWith(
      (_) => Future.value(),
    ),
  ];
}

FutureOr<String> fakeFetchCertificate(
    dynamic appKeyJson, dynamic appManagerKeyJson) async {
  final jsonWebToken = jwt.JWT(
    {
      "bsn": "814859094",
      "given_name": "Piet",
      "app_public_key": appKeyJson["publicKey"],
      "nbf": DateTime.now().millisecondsSinceEpoch ~/ 1000,
      "exp":
          DateTime.now().add(const Duration(days: 2)).millisecondsSinceEpoch ~/
              1000,
    },
    issuer: 'https://github.com/jonasroussel/dart_jsonwebtoken',
  );

  final jwtToken = jsonWebToken.sign(jwt.ECPrivateKey(appKeyJson["privateKey"]),
      algorithm: jwt.JWTAlgorithm.ES256);
  final encodedJwtToken = base64Encode(const Utf8Encoder().convert(jwtToken));

  final certificate = {
    "id": 1,
    "type": "AppManagerJWTCertificate",
    "value": encodedJwtToken,
    "bsn": "814859094",
    "givenName": "Piet",
    "expiresAt": DateTime.now().add(const Duration(days: 1)).toIso8601String(),
    "deemedExpiredBySourceOrganization": "true"
  };

  final certificateAsJson = jsonEncode(certificate);

  final aesKeyBytes = Base64.toBytes(appKeyJson["aesKey"]);

  final encryptedCertificate = Base64.parseFromBytes(
    Aes.encrypt(
      aesKeyBytes,
      utf8.encode(certificateAsJson),
    ),
  );

  return encryptedCertificate;
}
