// Copyright © Centraal Justitieel Incassobureau (CJIB) 2023
// Licensed under the EUPL

import 'dart:convert';

import 'package:flutter/services.dart';
import 'package:user_ui/clients/scheme/model/scheme.dart';
import 'package:user_ui/clients/scheme/model/scheme_app_manager.dart';
import 'package:user_ui/clients/scheme/model/scheme_document_type.dart';
import 'package:user_ui/clients/scheme/model/scheme_organization.dart';
import 'package:user_ui/clients/scheme/scheme_client.dart';

class SchemeClientMock implements SchemeClient {
  SchemeClientMock();

  @override
  Future<Scheme> fetchScheme() async {
    var organizations = [
      SchemeOrganization(
        oin: '00000000000000000001',
        name: 'mock-org 1',
        discoveryUrl: "http://mock1-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
        MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
        AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
        -----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000002',
        name: 'mock-org 2',
        discoveryUrl: "http://mock2-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
        MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
        AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
        -----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000003',
        name: 'mock-org 3',
        discoveryUrl: "http://mock3-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
        MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
        AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
        -----END PUBLIC KEY-----""",
      ),
      SchemeOrganization(
        oin: '00000000000000000004',
        name: 'mock-org 4',
        discoveryUrl: "http://mock4-session-api.mock/v1",
        publicKey: """-----BEGIN PUBLIC KEY-----
        MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80
        AoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==
        -----END PUBLIC KEY-----""",
      ),
    ];

    var documentTypes = [
      SchemeDocumentType(
        name: 'mock-document-type',
      ),
    ];

    final appManagerKeyJson = jsonDecode(
        await rootBundle.loadString('assets/mock_app_manager_keys.json'));

    var appManagers = [
      SchemeAppManager(
        oin: '00000000000000000004',
        name: 'mock-app-manager',
        discoveryUrl: "http://mock5-session-api.mock/v1",
        publicKey: appManagerKeyJson["publicKey"],
      ),
    ];

    return Scheme(
      organizations: organizations,
      documentTypes: documentTypes,
      appManagers: appManagers,
    );
  }
}
