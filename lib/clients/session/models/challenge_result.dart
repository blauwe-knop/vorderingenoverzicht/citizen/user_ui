// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeResult {
  String appPublicKey;
  String appNonceSignature;
  String? certificateType;
  String? certificate;
  String sessionAesKey;

  ChallengeResult({
    required this.appPublicKey,
    required this.appNonceSignature,
    required this.certificateType,
    required this.certificate,
    required this.sessionAesKey,
  });

  factory ChallengeResult.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeResult(
      appNonceSignature: jsonObject["appNonceSignature"],
      appPublicKey: jsonObject["appPublicKey"],
      certificateType: jsonObject["certificateType"],
      certificate: jsonObject["certificate"],
      sessionAesKey: jsonObject["sessionAesKey"],
    );
  }

  Map<String, dynamic> toJson() => {
        'appNonceSignature': appNonceSignature,
        'appPublicKey': appPublicKey,
        'certificateType': certificateType,
        'certificate': certificate,
        'sessionAesKey': sessionAesKey,
      };
}
