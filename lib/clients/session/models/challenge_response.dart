// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL

class ChallengeResponse {
  String nonce;
  String encryptedChallengeResult;

  ChallengeResponse({
    required this.nonce,
    required this.encryptedChallengeResult,
  });

  factory ChallengeResponse.fromJson(Map<String, dynamic> jsonObject) {
    return ChallengeResponse(
      nonce: jsonObject["nonce"],
      encryptedChallengeResult: jsonObject["encryptedChallengeResult"],
    );
  }

  Map<String, dynamic> toJson() => {
        'nonce': nonce,
        'encryptedChallengeResult': encryptedChallengeResult,
      };
}
