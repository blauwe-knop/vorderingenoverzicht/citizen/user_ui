// Copyright © Centraal Justitieel Incassobureau (CJIB) 2022
// Licensed under the EUPL
import 'package:dart_connect/dart_connect.dart';
import 'package:user_ui/clients/citizen_financial_claim/citizen_financial_claim_client.dart';

class CitizenFinancialClaimClientInvalidMock
    extends CitizenFinancialClaimClient {
  @override
  Future<String> configureClaimsRequest(
    String serviceUrl,
    String sessionToken,
    Base64String encryptedEnvelope,
  ) async {
    throw Exception('failed to configure claims request');
  }

  @override
  Future<String> requestFinancialClaimsInformation(
      String serviceUrl, String sessionToken, String configurationToken) async {
    throw Exception('failed to request financial claims information');
  }
}
