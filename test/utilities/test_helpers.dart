import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'listener.dart';

/// Set up a listener and checks if the called provider runs as expected
Future<void> handleErrorTests<T>(
    {required ProviderContainer container,
    required ProviderBase<AsyncValue<T>> sut,
    required Future<void> Function() callSut}) async {
  // Create a listener
  final listener = Listener<AsyncValue<T>>();

  // Subscribe to the future provider so it stays alive and doesn't dispose
  container.listen(
    sut,
    listener.call,
    fireImmediately: true,
  );

  // Verify that future returns the loading state at first
  verify(() => listener(null, AsyncLoading<T>()));

  // Call the provided function
  await callSut();

  // Verify that future returns the error state after loading state
  verify(() => listener(AsyncLoading<T>(), any(that: isA<AsyncError<T>>())));

  // Verify that the function has finished
  verifyNoMoreInteractions(listener);
}

/// Set up a listener and checks if the called provider runs as expected
Future<void> handleSuccesTests<T>(
    {required ProviderContainer container,
    required ProviderBase<AsyncValue<T>> sut,
    required Future<void> Function() callSut}) async {
  // Create a listener
  final listener = Listener<AsyncValue<T>>();

  // Subscribe to the future provider so it stays alive and doesn't dispose
  container.listen(
    sut,
    listener.call,
    fireImmediately: true,
  );

  // Verify that future returns the loading state at first
  verify(() => listener(null, AsyncLoading<T>()));

  // Call the provided function
  await callSut();

  // Verify that future returns data succesfully after loading state
  verify(() => listener(AsyncLoading<T>(), any(that: isA<AsyncData<T>>())));

  // Verify that the function has finished
  verifyNoMoreInteractions(listener);
}
