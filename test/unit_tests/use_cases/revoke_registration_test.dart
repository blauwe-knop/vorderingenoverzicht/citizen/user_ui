import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:user_ui/clients/app_manager/unregister_app_delegate.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/entities/app_manager_selection.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/entities/scheme_app_manager.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';
import 'package:user_ui/repositories/app_identity/mocks/app_root_keypair_mock.dart';
import 'package:user_ui/repositories/app_manager_selection/app_manager_selection_repository.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository_mock.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/repositories/scheme_app_manager/mocks/fetch_scheme_app_manager_mock_notifier.dart';
import 'package:user_ui/usecases/helpers/revoke_registration_helper.dart';

import '../../utilities/create_container.dart';
import '../../utilities/mocks.dart';
import '../../utilities/test_helpers.dart';

void main() {
  late MockLoggingHelper mockLoggingRepository;
  late MockDriftAppManagerSelectionRepository mockAppManagerSelectionRepository;
  late MockFetchSchemeAppManager mockFetchSchemeAppManager;

  final registration = Registration(
    id: 1,
    dateTimeStarted: DateTime.now(),
    appPublicKey: 'appPublicKey',
    appManagerOin: 'appManagerOin',
    appManagerPublicKey: 'appManagerPublicKey',
    registrationToken: 'registrationToken',
    dateTimeCompleted: DateTime.now(),
    givenName: 'givenName',
    expired: false,
    revoked: false,
  );

  setUp(() {
    mockLoggingRepository = MockLoggingHelper();
    mockAppManagerSelectionRepository = MockDriftAppManagerSelectionRepository(
        const AppManagerSelection(id: 1, oin: ''));
    mockFetchSchemeAppManager = MockFetchSchemeAppManager();

    registerFallbackValue(const AsyncData<String>(''));
    registerFallbackValue(registration);

    WidgetsFlutterBinding.ensureInitialized();

    when(
      () => mockAppManagerSelectionRepository.selectedAppManager,
    ).thenAnswer(
      (_) => Future.value(
        const AppManagerSelection(id: 1, oin: ''),
      ),
    );

    when(
      () => mockFetchSchemeAppManager.build(),
    ).thenAnswer(
      (_) => Future.value(
        const SchemeAppManager(
          id: 1,
          oin: '',
          name: '',
          discoveryUrl: '',
          publicKey: '',
          available: true,
        ),
      ),
    );
  });

  test('Should run successfully', () async {
    final container = createContainer(
      overrides: [
        loggingProvider.overrideWithValue(mockLoggingRepository),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        appManagerSelectionRepositoryProvider
            .overrideWith((ref) => mockAppManagerSelectionRepository),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async =>
              Future.value(
                  const AppSession(id: 1, key: '', token: '', oin: '')),
        ),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        unregisterAppDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            unregisterAppRequest,
          ) async {
            return Future.value();
          },
        ),
      ],
    );

    final sut = revokeRegistrationHelperProvider(registration);

    await handleSuccesTests(
      container: container,
      sut: sut,
      callSut: () async {
        await container.read(sut.future);
      },
    );

    final registrations =
        await container.read(registrationRepositoryProvider.future);
    expect(registrations.length, 1);
    expect(registrations.single.revoked, true);
  });

  test('Should throw when signing fails', () async {
    final container = createContainer(
      overrides: [
        loggingProvider.overrideWithValue(mockLoggingRepository),
        appManagerSelectionRepositoryProvider
            .overrideWith((ref) => mockAppManagerSelectionRepository),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        unregisterAppDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            unregisterAppRequest,
          ) async {
            return Future.value();
          },
        ),
      ],
    );

    final sut = revokeRegistrationHelperProvider(registration);

    final matcherFailureText = Exception(
      "Failed to sign message",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } on Exception catch (e) {
          expect(e.toString(), matcherFailureText.toString());
        }
      },
    );
  });
}
