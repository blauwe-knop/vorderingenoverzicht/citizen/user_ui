import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/entities/financial_claims_information_configuration.dart';
import 'package:user_ui/entities/financial_claims_information_request.dart';
import 'package:user_ui/entities/financial_claims_information_request_status.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/device_event.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/request_financial_claims_information/steps/reuse_or_create_configuration.dart';

import '../../../utilities/create_container.dart';
import '../../../utilities/mocks.dart';
import '../../../utilities/test_helpers.dart';

void main() {
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepository;
  late MockUserSettingsRepository mockUserSettingsRepository;
  late MockLoggingHelper mockLoggingHelper;

  const financialClaimsInformationConfiguration =
      FinancialClaimsInformationConfiguration(
    id: 1,
    oin: 'oin',
    document: 'document',
    documentSignature: 'documentSignature',
    envelope: 'envelope',
    encryptedEnvelope: 'encryptedEnvelope',
    configuration: 'configuration',
    expired: false,
  );

  final financialClaimsInformationRequest = FinancialClaimsInformationRequest(
    id: 1,
    oin: 'oin',
    dateTimeRequested: DateTime.now(),
    status: FinancialClaimsInformationRequestStatus.queue,
    lock: true,
  );

  final sut =
      reuseOrCreateConfigurationProvider(financialClaimsInformationRequest);

  const userSettings = UserSettings(
    hasSeenOnboarding: true,
    autoSelectAllOrganizations: true,
    hasCompletedRegistration: true,
    hasCompletedOrganizationSelection: true,
    delay: false,
    showDevelopmentOverlay: false,
    showCriticalErrorAlerts: false,
    pincode: '1234',
    betalingsregelingRijkFeature: false,
  );

  setUp(() {
    mockLoggingHelper = MockLoggingHelper();
    mockFinancialClaimsInformationConfigurationRepository =
        MockFinancialClaimsInformationConfigurationRepository();
    mockFinancialClaimsInformationRequestRepository =
        MockFinancialClaimsInformationRequestRepository();
    mockUserSettingsRepository = MockUserSettingsRepository(userSettings);

    registerFallbackValue(const AsyncData(''));
    registerFallbackValue(financialClaimsInformationRequest);
    registerFallbackValue(financialClaimsInformationConfiguration);
    registerFallbackValue(DeviceEvent.uu_10);

    when(
      () => mockUserSettingsRepository.userSettings,
    ).thenAnswer(
      (_) => Future.value(const UserSettings(
        hasSeenOnboarding: true,
        autoSelectAllOrganizations: true,
        hasCompletedRegistration: true,
        hasCompletedOrganizationSelection: true,
        delay: false,
        showDevelopmentOverlay: false,
        showCriticalErrorAlerts: false,
        pincode: '1234',
        betalingsregelingRijkFeature: false,
      )),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepository.update(any()),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationConfigurationRepository.add(
          oin: any(named: 'oin')),
    ).thenAnswer(
      (_) => Future.value(),
    );
  });

  group('Reuse or create configuration', () {
    test('Should set state to encryptedEnvelopeMailed', () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future.value(financialClaimsInformationConfiguration),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
              status: FinancialClaimsInformationRequestStatus
                  .encryptedEnvelopeMailed);

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    test('Should set state to configurationCreated', () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future<FinancialClaimsInformationConfiguration?>.value(null),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: sut,
        callSut: () async {
          await container.read(sut.future);
        },
      );

      final financialClaimsInformationRequestUpdate =
          financialClaimsInformationRequest.copyWith(
              status:
                  FinancialClaimsInformationRequestStatus.configurationCreated);

      verify(() => mockFinancialClaimsInformationRequestRepository
          .update(financialClaimsInformationRequestUpdate)).called(1);
    });

    test('Should throw when previous request is in progress', () async {
      when(
        () => mockFinancialClaimsInformationConfigurationRepository
            .getUnexpiredConfigurationForOin(any()),
      ).thenAnswer(
        (_) => Future<FinancialClaimsInformationConfiguration?>.value(
            financialClaimsInformationConfiguration.copyWith(
                configuration: null)),
      );

      final container = createContainer(
        overrides: [
          loggingProvider.overrideWith((ref) => mockLoggingHelper),
          userSettingsRepositoryProvider
              .overrideWith((ref) => mockUserSettingsRepository),
          financialClaimsInformationRequestRepositoryProvider.overrideWith(
              (ref) => mockFinancialClaimsInformationRequestRepository),
          financialClaimsInformationConfigurationRepositoryProvider
              .overrideWith((ref) =>
                  mockFinancialClaimsInformationConfigurationRepository),
        ],
      );

      final matcherFailureText = Exception(
        "previous request is in progress",
      );

      await handleErrorTests(
        container: container,
        sut: sut,
        callSut: () async {
          try {
            await container.read(sut.future);
          } on Exception catch (e) {
            expect(e.toString(), matcherFailureText.toString());
          }
        },
      );
    });
  });
}
