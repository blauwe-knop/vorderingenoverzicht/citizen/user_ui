import 'dart:convert';

import 'package:dart_connect/dart_connect.dart';
import 'package:dart_jsonwebtoken/dart_jsonwebtoken.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';
import 'package:user_ui/clients/app_manager/fetch_certificate_delegate.dart';
import 'package:user_ui/clients/app_manager/models/registration_expired_exception.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_live.dart';
import 'package:user_ui/clients/service_discovery/service_discovery_client_mock.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/registration.dart';
import 'package:user_ui/entities/user_settings.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/latest_app_session_delegate.dart';
import 'package:user_ui/repositories/app_identity/mocks/app_root_keypair_mock.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/certificate/certificate_repository_mock.dart';
import 'package:user_ui/repositories/financial_claims_information_configuration/financial_claims_information_configuration_repository.dart';
import 'package:user_ui/repositories/financial_claims_information_request/financial_claims_information_request_repository.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/registration/registration_repository.dart';
import 'package:user_ui/repositories/registration/registration_repository_mock.dart';
import 'package:user_ui/repositories/scheme_app_manager/fetch_scheme_app_manager_notifier.dart';
import 'package:user_ui/repositories/scheme_app_manager/mocks/fetch_scheme_app_manager_mock_notifier.dart';
import 'package:user_ui/repositories/user_settings/user_settings_repository.dart';
import 'package:user_ui/usecases/helpers/fetch_and_store_certificate_helper.dart';
import 'package:user_ui/usecases/registration/models/certificate.dart';

import '../../utilities/claims.dart';
import '../../utilities/create_container.dart';
import '../../utilities/mocks.dart';
import '../../utilities/test_helpers.dart';

// ignore: subtype_of_sealed_class
class AsyncValueVoidFake extends Fake implements AsyncValue<void> {}

void main() {
  late MockLoggingHelper mockLoggingHelper;
  late MockUserSettingsRepository mockUserSettingsRepository;
  late MockFinancialClaimsInformationRequestRepository
      mockFinancialClaimsInformationRequestRepositoryProvider;
  late MockFinancialClaimsInformationConfigurationRepository
      mockFinancialClaimsInformationConfigurationRepositoryProvider;

  final registration = Registration(
    id: 1,
    dateTimeStarted: DateTime.now(),
    appPublicKey: 'appPublicKey',
    appManagerOin: 'appManagerOin',
    appManagerPublicKey: 'appManagerPublicKey',
    registrationToken: 'registrationToken',
    dateTimeCompleted: DateTime.now(),
    givenName: 'givenName',
    expired: false,
    revoked: false,
  );

  const userSettings = UserSettings(
    hasSeenOnboarding: true,
    autoSelectAllOrganizations: true,
    hasCompletedRegistration: true,
    hasCompletedOrganizationSelection: true,
    delay: false,
    showDevelopmentOverlay: false,
    showCriticalErrorAlerts: false,
    pincode: '1234',
    betalingsregelingRijkFeature: false,
  );

  setUp(() {
    WidgetsFlutterBinding.ensureInitialized();
    mockLoggingHelper = MockLoggingHelper();
    mockUserSettingsRepository = MockUserSettingsRepository(userSettings);
    mockFinancialClaimsInformationRequestRepositoryProvider =
        MockFinancialClaimsInformationRequestRepository();
    mockFinancialClaimsInformationConfigurationRepositoryProvider =
        MockFinancialClaimsInformationConfigurationRepository();

    registerFallbackValue(registration);
    registerFallbackValue(userSettings);
    registerFallbackValue(Future.value());
    registerFallbackValue(AsyncValueVoidFake());

    when(
      () => mockUserSettingsRepository.userSettings,
    ).thenAnswer(
      (_) => Future.value(userSettings),
    );

    when(
      () => mockFinancialClaimsInformationRequestRepositoryProvider
          .cancelAllUncompletedRequests(),
    ).thenAnswer(
      (_) => Future.value(),
    );

    when(
      () => mockFinancialClaimsInformationConfigurationRepositoryProvider
          .expireAll(),
    ).thenAnswer(
      (_) => Future.value(),
    );
  });
  test('Should run successfully', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));

    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        certificateRepositoryProvider
            .overrideWith(() => CertificateRepositoryMock()),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async {
            final claims = Claims().copyWith(
              appPublicKey: keys["publicKey"],
              appManagerPublicKey: keys["publicKey"],
            );
            return await createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode(
              claims: claims,
              appPrivateKey: keys["privateKey"],
              aesKey: keys["aesKey"],
            );
          },
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: keys["publicKey"],
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    await handleSuccesTests(
      container: container,
      sut: sut,
      callSut: () async {
        await container.read(sut.future);
      },
    );

    final certificates =
        await container.read(registrationRepositoryProvider.future);
    expect(certificates.length, 1);
    expect(certificates.single.givenName, "John");
  });

  test('Should throw when appPublicKey changed', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        certificateRepositoryProvider
            .overrideWith(() => CertificateRepositoryMock()),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async {
            final claims = Claims();
            return await createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode(
              claims: claims,
              appPrivateKey: keys["privateKey"],
              aesKey: keys["aesKey"],
            );
          },
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: "wrong key",
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    final matcherFailureText = Exception(
      "appPublicKey changed, cannot complete registration",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } catch (e) {
          expect(e.runtimeType, matcherFailureText.runtimeType);
          expect(e.toString().contains(matcherFailureText.toString()), true);
        }
      },
    );
  });

  test('Should throw RegistrationExpiredException', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        financialClaimsInformationRequestRepositoryProvider.overrideWith(
            (ref) => mockFinancialClaimsInformationRequestRepositoryProvider),
        financialClaimsInformationConfigurationRepositoryProvider.overrideWith(
            (ref) =>
                mockFinancialClaimsInformationConfigurationRepositoryProvider),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async =>
              throw RegistrationExpiredException('expired'),
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: keys["publicKey"],
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    final matcherFailureText = RegistrationExpiredException(
      "expired",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } catch (e) {
          expect(e.runtimeType, matcherFailureText.runtimeType);
          expect(e.toString(), matcherFailureText.toString());
        }
      },
    );
  });

  test('Should throw when certificate expired', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        certificateRepositoryProvider
            .overrideWith(() => CertificateRepositoryMock()),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async {
            final claims = Claims().copyWith(ucCertificateExpirationMinutes: 0);
            return await createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode(
              claims: claims,
              appPrivateKey: keys["privateKey"],
              aesKey: keys["aesKey"],
            );
          },
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: keys["publicKey"],
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    final matcherFailureText = Exception(
      "certificate expired",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } catch (e) {
          expect(e.runtimeType, matcherFailureText.runtimeType);
          expect(e.toString().contains(matcherFailureText.toString()), true);
        }
      },
    );
  });

  test('Should throw when invalid signature on registration result', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        certificateRepositoryProvider
            .overrideWith(() => CertificateRepositoryMock()),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async {
            final claims = Claims().copyWith(leewayNotBeforeMinutes: 1);
            return await createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode(
              claims: claims,
              appPrivateKey: keys["privateKey"],
              aesKey: keys["aesKey"],
            );
          },
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: keys["publicKey"],
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    final matcherFailureText = Exception(
      "Invalid signature on registration result:",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } catch (e) {
          expect(e.runtimeType, matcherFailureText.runtimeType);
          expect(e.toString().contains(matcherFailureText.toString()), true);
        }
      },
    );
  });

  test('Should throw when appPublicKey changed', () async {
    final keys =
        jsonDecode(await rootBundle.loadString('assets/mock_app_keys.json'));
    final container = createContainer(
      overrides: [
        userSettingsRepositoryProvider
            .overrideWith((ref) => mockUserSettingsRepository),
        loggingProvider.overrideWith((ref) => mockLoggingHelper),
        appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
        fetchSchemeAppManagerProvider
            .overrideWith(() => FetchSchemeAppManagerMock()),
        serviceDiscoveryClientProvider
            .overrideWith((ref) => ServiceDiscoveryClientMock()),
        latestAppSessionDelegateProvider.overrideWith(
          (ref) => (
            ref,
            oin,
          ) async {
            final keys = jsonDecode(
                await rootBundle.loadString('assets/mock_app_keys.json'));
            final aesKey = keys["aesKey"];
            return AppSession(id: 1, key: aesKey, token: 'token', oin: oin);
          },
        ),
        certificateRepositoryProvider
            .overrideWith(() => CertificateRepositoryMock()),
        registrationRepositoryProvider
            .overrideWith(() => RegistrationRepositoryMock()),
        fetchCertificateDelegateProvider.overrideWith(
          (ref) => (
            ref,
            serviceUrl,
            sessionToken,
            registrationToken,
          ) async {
            final claims = Claims().copyWith(appPublicKey: keys["privateKey"]);
            return await createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode(
              claims: claims,
              appPrivateKey: keys["privateKey"],
              aesKey: keys["aesKey"],
            );
          },
        ),
      ],
    );

    final registrationUpdate = registration.copyWith(
      appPublicKey: keys["publicKey"],
      appManagerPublicKey: keys["publicKey"],
    );

    final sut = fetchAndStoreCertificateHelperProvider(
        registration: registrationUpdate);

    final matcherFailureText = Exception(
      "Invalid started registration result: wrong appPublicKey",
    );

    await handleErrorTests(
      container: container,
      sut: sut,
      callSut: () async {
        try {
          await container.read(sut.future);
        } catch (e) {
          expect(e.runtimeType, matcherFailureText.runtimeType);
          expect(e.toString().contains(matcherFailureText.toString()), true);
        }
      },
    );
  });
}

Future<String>
    createCertificateWithClaimsSignedWithPrivateKeyAndEncryptAndEncode({
  required Claims claims,
  required String aesKey,
  required String appPrivateKey,
}) async {
  Map<String, dynamic> jsonClaims = claims.toJson();

  String accessToken = JWT(jsonClaims)
      .sign(ECPrivateKey(appPrivateKey), algorithm: JWTAlgorithm.ES256);

  Certificate certificate = Certificate(
    type: 'AppManagerJWTCertificate',
    value: utf8.encode(accessToken),
  );

  final certificateJson = json.encode(certificate.toJson());
  final encryptedCertificate = Base64.parseFromBytes(
    Aes.encrypt(
      Base64.toBytes(aesKey),
      utf8.encode(certificateJson),
    ),
  );

  return Future.value(encryptedCertificate);
}
