import 'dart:convert';
import 'dart:typed_data';

import 'package:dart_connect/dart_connect.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:user_ui/clients/session/models/challenge.dart';
import 'package:user_ui/clients/session/models/challenge_response.dart';
import 'package:user_ui/clients/session/session_client_live.dart';
import 'package:user_ui/entities/app_session.dart';
import 'package:user_ui/entities/certificate.dart';
import 'package:user_ui/entities/certificate_type.dart';
import 'package:user_ui/entities/organization_type.dart';
import 'package:user_ui/repositories/app_identity/app_root_keypair.dart';
import 'package:user_ui/repositories/app_identity/app_session_repository.dart';
import 'package:user_ui/repositories/app_identity/mocks/app_root_keypair_mock.dart';
import 'package:user_ui/repositories/certificate/certificate_repository.dart';
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/usecases/app_identity/generate_and_store_session_key_delegate.dart';
import 'package:user_ui/usecases/helpers/start_session.dart';

import '../../utilities/create_container.dart';
import '../../utilities/mocks.dart';
import '../../utilities/test_helpers.dart';

void main() {
  late MockLoggingHelper loggingRepository;
  late MockSessionClient sessionClient;
  late MockAppSessionRepository appSessionRepository;
  late MockCertificateRepository certificateRepository;
  late MockAppRootKeypair appRootKeypair;

  const String sessionApiUrl = '';
  const String organizationOin = 'oin';
  const String publicEcKey =
      '-----BEGIN PUBLIC KEY-----\nMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80\nAoHZKXHK3JL8sqBUbYlC6MoGh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==\n-----END PUBLIC KEY-----';
  const String privateEcKey =
      '-----BEGIN EC PRIVATE KEY-----\nMHcCAQEEIL+WYjDkIULn/pSZGgQDApAYA3kk5ZYkKUfwD6WVHbkeoAoGCCqGSM49\nAwEHoUQDQgAEtTbVn130qkzHvvM4RGnYTz6Pxq80AoHZKXHK3JL8sqBUbYlC6MoG\nh0MCJ5n/I056quv/FEpdiIyRUgwgsfK7VA==\n-----END EC PRIVATE KEY-----';
  Uint8List aesKey = Uint8List.fromList([
    228,
    229,
    118,
    178,
    143,
    199,
    245,
    225,
    150,
    206,
    170,
    29,
    245,
    54,
    106,
    26
  ]);
  const OrganizationType contactType = OrganizationType.appManager;

  setUp(() {
    loggingRepository = MockLoggingHelper();
    sessionClient = MockSessionClient();
    appSessionRepository = MockAppSessionRepository();
    certificateRepository = MockCertificateRepository();
    appRootKeypair = MockAppRootKeypair();

    registerFallbackValue(const AsyncData<String>(''));
    registerFallbackValue(
        ChallengeResponse(nonce: '', encryptedChallengeResult: ''));
    registerFallbackValue(const AppSession(id: 1, key: '', token: '', oin: ''));

    TestWidgetsFlutterBinding.ensureInitialized();
  });

  group('Start session tests', () {
    setUp(() {
      when(
        () => appSessionRepository.build(),
      ).thenAnswer(
        (_) {
          return Future.value([
            AppSession(
              id: 1,
              key: Base64.parseFromBytes(aesKey),
              token: '',
              oin: 'oin',
            ),
          ]);
        },
      );

      when(
        () => appSessionRepository.add(key: '', oin: 'oin'),
      ).thenAnswer(
        (_) => Future.value(),
      );

      when(
        () => appSessionRepository.updateAppSession(any()),
      ).thenAnswer(
        (_) => Future.value(),
      );

      when(
        () => certificateRepository.build(),
      ).thenAnswer(
        (_) => Future.value([
          Certificate(
            id: 1,
            type: CertificateType.appManagerJWTCertificate,
            value: utf8.encode('string'),
            bsn: '',
            givenName: '',
            expiresAt: DateTime.now(),
            deemedExpiredBySourceOrganization: false,
            scope: '',
          ),
        ]),
      );
    });

    test('Should create a session token', () async {
      const nonce = 'random nonce';
      final nonceBytes = utf8.encode(nonce);
      // ignore: deprecated_member_use
      final privateKey = Ec.parsePrivateKeyFromPem(utf8.encode(privateEcKey));
      // ignore: deprecated_member_use
      final signatureBytes = Ecdsa.sign(privateKey, nonceBytes);
      final organizationNonceSignature = Base64.parseFromBytes(signatureBytes);

      when(
        () => sessionClient.requestChallenge(sessionApiUrl),
      ).thenAnswer(
        (_) {
          return Future.value(
            Challenge(
              organizationNonceSignature: organizationNonceSignature,
              nonce: nonce,
              ephemeralOrganizationEcPublicKey: publicEcKey,
            ),
          );
        },
      );

      when(
        () => sessionClient.completeChallenge(any(), any()),
      ).thenAnswer(
        (_) {
          final plainText = utf8.encode('session token');
          final encryptedSessionToken = Aes.encrypt(aesKey, plainText);
          final decryptedPlainText = Aes.decrypt(aesKey, encryptedSessionToken);

          expect(decryptedPlainText, equals(plainText));

          return Future.value(Base64.parseFromBytes(encryptedSessionToken));
        },
      );

      final container = createContainer(
        overrides: [
          sessionClientProvider.overrideWithValue(sessionClient),
          appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
          loggingProvider.overrideWithValue(loggingRepository),
          appSessionRepositoryProvider.overrideWith(() => appSessionRepository),
          certificateRepositoryProvider
              .overrideWith(() => certificateRepository),
          generateAndStoreSessionKeyDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async =>
                Future.value(),
          ),
        ],
      );

      final StartSessionProvider sut = startSessionProvider(
        sessionApiUrl: sessionApiUrl,
        rootOrganizationEcPublicKey: publicEcKey,
        organizationOin: organizationOin,
        contactType: contactType,
      );

      await handleSuccesTests<String>(
        container: container,
        sut: sut,
        callSut: () async {
          final sessionToken = await container.read(sut.future);
          expect(sessionToken, isNotEmpty);
        },
      );

      verify(() => sessionClient.requestChallenge(sessionApiUrl)).called(1);
    });

    group('Error Handling Tests', () {
      test('Should throw when root organization EC public key is invalid',
          () async {
        when(
          () => sessionClient.requestChallenge(sessionApiUrl),
        ).thenAnswer(
          (_) {
            return Future.value(
              Challenge(
                organizationNonceSignature: 'invalid signature',
                nonce: 'nonce',
                ephemeralOrganizationEcPublicKey: '',
              ),
            );
          },
        );

        final matcherFailureText = Exception(
          "invalid root organization EC public key",
        );

        final container = createContainer(
          overrides: [
            sessionClientProvider.overrideWithValue(sessionClient),
            loggingProvider.overrideWithValue(loggingRepository),
            appSessionRepositoryProvider
                .overrideWith(() => appSessionRepository),
            certificateRepositoryProvider
                .overrideWith(() => certificateRepository),
            generateAndStoreSessionKeyDelegateProvider.overrideWith(
              (ref) => (
                ref,
                oin,
              ) async =>
                  Future.value(),
            ),
          ],
        );

        final StartSessionProvider sut = startSessionProvider(
          sessionApiUrl: sessionApiUrl,
          rootOrganizationEcPublicKey: 'invalid key',
          organizationOin: organizationOin,
          contactType: contactType,
        );

        await handleErrorTests<String>(
          container: container,
          sut: sut,
          callSut: () async {
            try {
              await container.read(sut.future);
            } on Exception catch (e) {
              expect(e.toString(), matcherFailureText.toString());
            }
          },
        );
      });

      test('Should throw when an invalid signature is used', () async {
        when(
          () => sessionClient.requestChallenge(sessionApiUrl),
        ).thenAnswer(
          (_) {
            return Future.value(
              Challenge(
                organizationNonceSignature: 'invalid signature',
                nonce: 'nonce',
                ephemeralOrganizationEcPublicKey: '',
              ),
            );
          },
        );

        final matcherFailureText = Exception(
          "Failed to verify signature",
        );
        final container = createContainer(
          overrides: [
            sessionClientProvider.overrideWithValue(sessionClient),
            loggingProvider.overrideWithValue(loggingRepository),
            appSessionRepositoryProvider
                .overrideWith(() => appSessionRepository),
            certificateRepositoryProvider
                .overrideWith(() => certificateRepository),
            appRootKeypairProvider.overrideWith(() => appRootKeypair),
            generateAndStoreSessionKeyDelegateProvider.overrideWith(
              (ref) => (
                ref,
                oin,
              ) async =>
                  Future.value(),
            ),
          ],
        );

        final StartSessionProvider sut = startSessionProvider(
          sessionApiUrl: sessionApiUrl,
          rootOrganizationEcPublicKey: publicEcKey,
          organizationOin: organizationOin,
          contactType: contactType,
        );

        await handleErrorTests<String>(
          container: container,
          sut: sut,
          callSut: () async {
            try {
              await container.read(sut.future);
            } on Exception catch (e) {
              expect(e.toString(), matcherFailureText.toString());
            }
          },
        );
      });

      test('Should throw when invalid organization ec key is used', () async {
        when(
          () => sessionClient.requestChallenge(sessionApiUrl),
        ).thenAnswer(
          (_) {
            const nonce = 'random nonce';
            final nonceBytes = utf8.encode(nonce);
            final privateKey =
                // ignore: deprecated_member_use
                Ec.parsePrivateKeyFromPem(utf8.encode(privateEcKey));
            // ignore: deprecated_member_use
            final signatureBytes = Ecdsa.sign(privateKey, nonceBytes);
            final signature = Base64.parseFromBytes(signatureBytes);

            final publicKey =
                Ec.parsePublicKeyFromPem(utf8.encode(publicEcKey));
            Ecdsa.verify(publicKey, signatureBytes, nonceBytes);

            return Future.value(
              Challenge(
                organizationNonceSignature: signature,
                nonce: nonce,
                ephemeralOrganizationEcPublicKey: 'invalid key',
              ),
            );
          },
        );

        final matcherFailureText = Exception(
          "Failed to encrypt challenge result",
        );

        final container = createContainer(
          overrides: [
            sessionClientProvider.overrideWithValue(sessionClient),
            loggingProvider.overrideWithValue(loggingRepository),
            appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
            appSessionRepositoryProvider
                .overrideWith(() => appSessionRepository),
            certificateRepositoryProvider
                .overrideWith(() => certificateRepository),
            generateAndStoreSessionKeyDelegateProvider.overrideWith(
              (ref) => (
                ref,
                oin,
              ) async =>
                  Future.value(),
            ),
          ],
        );

        final StartSessionProvider sut = startSessionProvider(
          sessionApiUrl: sessionApiUrl,
          rootOrganizationEcPublicKey: publicEcKey,
          organizationOin: organizationOin,
          contactType: contactType,
        );

        await handleErrorTests<String>(
          container: container,
          sut: sut,
          callSut: () async {
            try {
              await container.read(sut.future);
            } on Exception catch (e) {
              expect(e.toString(), matcherFailureText.toString());
            }
          },
        );
      });

      test('Should throw when invalid session token is received', () async {
        when(
          () => sessionClient.requestChallenge(sessionApiUrl),
        ).thenAnswer(
          (_) {
            const nonce = 'random nonce';
            final nonceBytes = utf8.encode(nonce);
            final privateKey =
                // ignore: deprecated_member_use
                Ec.parsePrivateKeyFromPem(utf8.encode(privateEcKey));
            // ignore: deprecated_member_use
            final signatureBytes = Ecdsa.sign(privateKey, nonceBytes);
            final signature = Base64.parseFromBytes(signatureBytes);

            final publicKey =
                Ec.parsePublicKeyFromPem(utf8.encode(publicEcKey));
            Ecdsa.verify(publicKey, signatureBytes, nonceBytes);

            return Future.value(
              Challenge(
                organizationNonceSignature: signature,
                nonce: nonce,
                ephemeralOrganizationEcPublicKey: publicEcKey,
              ),
            );
          },
        );

        when(
          () => sessionClient.completeChallenge(any(), any()),
        ).thenAnswer(
          (_) {
            return Future.value('invalid token');
          },
        );

        final container = createContainer(
          overrides: [
            sessionClientProvider.overrideWithValue(sessionClient),
            loggingProvider.overrideWithValue(loggingRepository),
            appSessionRepositoryProvider
                .overrideWith(() => appSessionRepository),
            certificateRepositoryProvider
                .overrideWith(() => certificateRepository),
            appRootKeypairProvider.overrideWith(() => AppRootKeypairMock()),
            generateAndStoreSessionKeyDelegateProvider.overrideWith(
              (ref) => (
                ref,
                oin,
              ) async =>
                  Future.value(),
            ),
          ],
        );

        final StartSessionProvider sut = startSessionProvider(
          sessionApiUrl: sessionApiUrl,
          rootOrganizationEcPublicKey: publicEcKey,
          organizationOin: organizationOin,
          contactType: contactType,
        );

        await handleErrorTests<String>(
          container: container,
          sut: sut,
          callSut: () async {
            try {
              await container.read(sut.future);
            } on Exception catch (e) {
              expect(e, isInstanceOf<FormatException>());
            }
          },
        );
      });
    });

    test('Should throw when orgNonceSignature is not verified', () async {
      when(
        () => sessionClient.requestChallenge(sessionApiUrl),
      ).thenAnswer(
        (_) {
          const nonce = 'random nonce';
          final nonceBytes = utf8.encode(nonce);
          final privateKey =
              // ignore: deprecated_member_use
              Ec.parsePrivateKeyFromPem(utf8.encode(privateEcKey));
          // ignore: deprecated_member_use
          final signatureBytes = Ecdsa.sign(privateKey, nonceBytes);
          final signature = Base64.parseFromBytes(signatureBytes);

          return Future.value(
            Challenge(
              organizationNonceSignature: signature,
              nonce: 'nonce',
              ephemeralOrganizationEcPublicKey: publicEcKey,
            ),
          );
        },
      );

      when(
        () => sessionClient.completeChallenge(any(), any()),
      ).thenAnswer(
        (_) {
          return Future.value('invalid token');
        },
      );

      final container = createContainer(
        overrides: [
          sessionClientProvider.overrideWithValue(sessionClient),
          loggingProvider.overrideWithValue(loggingRepository),
          appSessionRepositoryProvider.overrideWith(() => appSessionRepository),
          certificateRepositoryProvider
              .overrideWith(() => certificateRepository),
          appRootKeypairProvider.overrideWith(() => appRootKeypair),
          generateAndStoreSessionKeyDelegateProvider.overrideWith(
            (ref) => (
              ref,
              oin,
            ) async =>
                Future.value(),
          ),
        ],
      );

      final StartSessionProvider sut = startSessionProvider(
        sessionApiUrl: sessionApiUrl,
        rootOrganizationEcPublicKey: publicEcKey,
        organizationOin: organizationOin,
        contactType: contactType,
      );

      await handleErrorTests<String>(
        container: container,
        sut: sut,
        callSut: () async {
          await expectLater(
            () => container.read(sut.future),
            throwsA(
              predicate((e) =>
                  e is Exception &&
                  e.toString().contains('invalid challenge signature')),
            ),
          );
        },
      );
    });
  });
}
