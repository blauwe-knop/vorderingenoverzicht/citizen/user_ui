import 'package:fcid_library/fcid_library.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:user_ui/clients/scheme/model/scheme.dart';
import 'package:user_ui/clients/scheme/model/scheme_app_manager.dart'
    as app_manager_model;
import 'package:user_ui/clients/scheme/model/scheme_document_type.dart'
    as document_model;
import 'package:user_ui/clients/scheme/model/scheme_organization.dart'
    as organization_model;
import 'package:user_ui/clients/scheme/scheme_client_live.dart';
import 'package:user_ui/entities/scheme_app_manager.dart' as app_manager_entity;
import 'package:user_ui/entities/scheme_document_type.dart' as document_entity;
import 'package:user_ui/repositories/logging/logging_helper.dart';
import 'package:user_ui/repositories/scheme_app_manager/scheme_app_manager_repository.dart';
import 'package:user_ui/repositories/scheme_document_type/scheme_document_type_repository.dart';
import 'package:user_ui/repositories/scheme_organization/scheme_organization_repository.dart';
import 'package:user_ui/usecases/scheme/update_local_scheme.dart';

import '../../utilities/create_container.dart';
import '../../utilities/mocks.dart';
import '../../utilities/test_helpers.dart';

void main() {
  late MockSchemeClient schemeClient;
  late MockLoggingHelper loggingRepository;
  late MockDriftSchemeAppManagerRepository schemeAppManagerRepository;
  late MockDriftSchemeOrganizationRepository schemeOrganizationRepository;
  late MockDriftSchemeDocumentTypeRepository schemeDocumentTypeRepository;

  setUp(() {
    schemeClient = MockSchemeClient();
    loggingRepository = MockLoggingHelper();
    schemeAppManagerRepository = MockDriftSchemeAppManagerRepository([]);
    schemeOrganizationRepository = MockDriftSchemeOrganizationRepository([]);
    schemeDocumentTypeRepository = MockDriftSchemeDocumentTypeRepository([]);

    registerFallbackValue(FakeSchemeDocumentEntity());
    registerFallbackValue(const AsyncData<void>(null));
    registerFallbackValue(FakeSchemeAppManagerEntity());
    registerFallbackValue(FakeSchemeOrganizationEntity());
  });

  test(
    'Should run successfully when scheme is empty',
    () async {
      when(
        () => schemeClient.fetchScheme(),
      ).thenAnswer(
        (_) => Future.value(
          Scheme(
            organizations: [],
            documentTypes: [],
            appManagers: [],
          ),
        ),
      );

      when(
        () => schemeOrganizationRepository.organizations,
      ).thenAnswer(
        (_) => Future.value([]),
      );

      when(
        () => schemeAppManagerRepository.appManagers,
      ).thenAnswer(
        (_) => Future.value([]),
      );

      when(
        () => schemeDocumentTypeRepository.documentTypes,
      ).thenAnswer(
        (_) => Future.value([]),
      );

      final container = createContainer(
        overrides: [
          schemeClientProvider.overrideWithValue(schemeClient),
          loggingProvider.overrideWithValue(loggingRepository),
          schemeOrganizationRepositoryProvider
              .overrideWith((ref) => schemeOrganizationRepository),
          schemeAppManagerRepositoryProvider
              .overrideWith((ref) => schemeAppManagerRepository),
          schemeDocumentTypeRepositoryProvider
              .overrideWith((ref) => schemeDocumentTypeRepository),
        ],
      );

      await handleSuccesTests(
        container: container,
        sut: updateLocalSchemeProvider,
        callSut: () async {
          await container.read(updateLocalSchemeProvider.future);
        },
      );

      verify(() => schemeClient.fetchScheme()).called(1);
    },
  );

  group(
    'Organization Update Tests',
    () {
      const stubSchemeOrganizationEntity = SchemeOrganization(
        id: 1,
        oin: '1',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
        available: false,
      );

      const stubSchemeOrganizationEntity2 = SchemeOrganization(
        id: 2,
        oin: '2',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
        available: false,
      );

      final stubSchemeOrganizationModel = organization_model.SchemeOrganization(
        oin: '1',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
      );

      setUp(
        () {
          when(
            () => schemeOrganizationRepository.update(any()),
          ).thenAnswer(
            (_) => Future.value(),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeOrganizationRepository.add(
              oin: any(named: 'oin'),
              name: any(named: 'name'),
              publicKey: any(named: 'publicKey'),
              discoveryUrl: any(named: 'discoveryUrl'),
              available: any(named: 'available'),
            ),
          ).thenAnswer((_) => Future.value());
        },
      );

      test(
        'Should update organization in local scheme',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [stubSchemeOrganizationModel],
                documentTypes: [],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeOrganizationEntity,
                stubSchemeOrganizationEntity2,
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeOrganizationRepository
                .update(stubSchemeOrganizationEntity2),
          ).called(1);

          verifyNever(
            () => schemeOrganizationRepository.update(
              stubSchemeOrganizationEntity,
            ),
          );
        },
      );

      test(
        'Should throw exception for duplicate organizations',
        () async {
          final matcherFailureText = Exception(
            "multiple organizations with the same OIN in scheme organization repository",
          );

          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [stubSchemeOrganizationModel],
                documentTypes: [],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeOrganizationEntity,
                stubSchemeOrganizationEntity,
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleErrorTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              try {
                await container.read(updateLocalSchemeProvider.future);
              } on Exception catch (error) {
                expect(error.toString(), matcherFailureText.toString());
              }
            },
          );
        },
      );

      test(
        'Should add new organization to local scheme',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [stubSchemeOrganizationModel],
                documentTypes: [],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeOrganizationRepository.add(
              oin: stubSchemeOrganizationModel.oin,
              name: stubSchemeOrganizationModel.name,
              publicKey: stubSchemeOrganizationModel.publicKey,
              discoveryUrl: stubSchemeOrganizationModel.discoveryUrl,
              available: true,
            ),
          ).called(1);
        },
      );

      test(
        'Should update organization with mismatched values',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [stubSchemeOrganizationModel],
                documentTypes: [],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeOrganizationEntity.copyWith(
                  name: 'other',
                  available: true,
                ),
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeOrganizationRepository.update(
              stubSchemeOrganizationEntity.copyWith(
                name: stubSchemeOrganizationModel.name,
                publicKey: stubSchemeOrganizationModel.publicKey,
                discoveryUrl: stubSchemeOrganizationModel.discoveryUrl,
                available: true,
              ),
            ),
          ).called(1);
        },
      );

      test(
        'Should update organization with local scheme not available',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [stubSchemeOrganizationModel],
                documentTypes: [],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeOrganizationEntity.copyWith(available: false),
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeOrganizationRepository.update(
              stubSchemeOrganizationEntity.copyWith(
                name: stubSchemeOrganizationModel.name,
                publicKey: stubSchemeOrganizationModel.publicKey,
                discoveryUrl: stubSchemeOrganizationModel.discoveryUrl,
                available: true,
              ),
            ),
          ).called(1);
        },
      );
    },
  );
  group(
    'DocumentType Update Tests',
    () {
      setUp(
        () {
          when(
            () => schemeDocumentTypeRepository.update(any()),
          ).thenAnswer(
            (_) => Future.value(),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeDocumentTypeRepository.add(
              name: any(named: 'name'),
              available: any(named: 'available'),
            ),
          ).thenAnswer(
            (_) => Future.value(),
          );
        },
      );

      const stubSchemeDocumentEntity = document_entity.SchemeDocumentType(
        id: 1,
        name: 'name1',
        available: false,
      );

      const stubSchemeDocumentEntity2 = document_entity.SchemeDocumentType(
        id: 2,
        name: 'name2',
        available: false,
      );

      final stubSchemeDocumentModel = document_model.SchemeDocumentType(
        name: 'name1',
      );

      test(
        'Should update document type in local scheme',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [stubSchemeDocumentModel],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeDocumentEntity,
                stubSchemeDocumentEntity2,
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () =>
                schemeDocumentTypeRepository.update(stubSchemeDocumentEntity2),
          ).called(1);

          verifyNever(
            () => schemeDocumentTypeRepository.update(stubSchemeDocumentEntity),
          );
        },
      );

      test(
        'Should throw exception for duplicate document types',
        () async {
          final matcherFailureText = Exception(
            "multiple documentTypes with the same name in scheme document type repository",
          );

          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [stubSchemeDocumentModel],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeDocumentEntity,
                stubSchemeDocumentEntity,
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleErrorTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              try {
                await container.read(updateLocalSchemeProvider.future);
              } on Exception catch (error) {
                expect(error.toString(), matcherFailureText.toString());
              }
            },
          );
        },
      );

      test(
        'Should add new document type to local scheme',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [stubSchemeDocumentModel],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeDocumentTypeRepository.add(
              name: stubSchemeDocumentEntity.name,
              available: true,
            ),
          ).called(1);
        },
      );

      test(
        'Should update document type with local scheme not available',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [stubSchemeDocumentModel],
                appManagers: [],
              ),
            ),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeDocumentEntity.copyWith(available: false),
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeDocumentTypeRepository.update(
              stubSchemeDocumentEntity.copyWith(
                name: stubSchemeDocumentEntity.name,
                available: true,
              ),
            ),
          ).called(1);
        },
      );
    },
  );

  group(
    'App Manager Update Tests',
    () {
      setUp(
        () {
          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeDocumentTypeRepository.documentTypes,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeOrganizationRepository.organizations,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          when(
            () => schemeAppManagerRepository.update(
              any(),
            ),
          ).thenAnswer(
            (_) => Future.value(),
          );

          when(
            () => schemeAppManagerRepository.add(
              oin: any(named: 'oin'),
              name: any(named: 'name'),
              publicKey: any(named: 'publicKey'),
              discoveryUrl: any(named: 'discoveryUrl'),
              available: any(named: 'available'),
            ),
          ).thenAnswer(
            (_) => Future.value(),
          );
        },
      );

      const stubSchemeAppManagerEntity = app_manager_entity.SchemeAppManager(
        id: 1,
        oin: '1',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
        available: false,
      );

      const stubSchemeAppManagerEntity2 = app_manager_entity.SchemeAppManager(
        id: 2,
        oin: '2',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
        available: false,
      );

      final stubSchemeAppManagerModel = app_manager_model.SchemeAppManager(
        oin: '1',
        name: 'name',
        publicKey: 'publicKey',
        discoveryUrl: 'discoveryUrl',
      );

      test('Should update app manager in local scheme', () async {
        when(
          () => schemeClient.fetchScheme(),
        ).thenAnswer(
          (_) => Future.value(
            Scheme(
              organizations: [],
              documentTypes: [],
              appManagers: [stubSchemeAppManagerModel],
            ),
          ),
        );

        when(
          () => schemeAppManagerRepository.appManagers,
        ).thenAnswer(
          (_) => Future.value(
            [
              stubSchemeAppManagerEntity,
              stubSchemeAppManagerEntity2,
            ],
          ),
        );

        final container = createContainer(
          overrides: [
            schemeClientProvider.overrideWithValue(schemeClient),
            loggingProvider.overrideWithValue(loggingRepository),
            schemeOrganizationRepositoryProvider
                .overrideWith((ref) => schemeOrganizationRepository),
            schemeAppManagerRepositoryProvider
                .overrideWith((ref) => schemeAppManagerRepository),
            schemeDocumentTypeRepositoryProvider
                .overrideWith((ref) => schemeDocumentTypeRepository),
          ],
        );

        await handleSuccesTests(
          container: container,
          sut: updateLocalSchemeProvider,
          callSut: () async {
            await container.read(updateLocalSchemeProvider.future);
          },
        );

        verify(
          () => schemeAppManagerRepository.update(stubSchemeAppManagerEntity2),
        ).called(1);

        verifyNever(
          () => schemeAppManagerRepository.update(stubSchemeAppManagerEntity),
        );
      });

      test(
        'Should throw exception for duplicate app managers',
        () async {
          final matcherFailureText = Exception(
              "multiple appmanagers with the same OIN in scheme app manager repository");

          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [],
                appManagers: [stubSchemeAppManagerModel],
              ),
            ),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeAppManagerEntity,
                stubSchemeAppManagerEntity,
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleErrorTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              try {
                await container.read(updateLocalSchemeProvider.future);
              } on Exception catch (error) {
                expect(error.toString(), matcherFailureText.toString());
              }
            },
          );
        },
      );

      test(
        'Should add new app manager to local scheme',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [],
                appManagers: [stubSchemeAppManagerModel],
              ),
            ),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value([]),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeAppManagerRepository.add(
              oin: stubSchemeAppManagerModel.oin,
              name: stubSchemeAppManagerModel.name,
              publicKey: stubSchemeAppManagerModel.publicKey,
              discoveryUrl: stubSchemeAppManagerModel.discoveryUrl,
              available: true,
            ),
          ).called(1);
        },
      );

      test(
        'Should update app manager with mismatched values',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [],
                appManagers: [stubSchemeAppManagerModel],
              ),
            ),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeAppManagerEntity.copyWith(
                    name: 'other', available: true),
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeAppManagerRepository.update(
              stubSchemeAppManagerEntity.copyWith(
                name: stubSchemeAppManagerModel.name,
                publicKey: stubSchemeAppManagerModel.publicKey,
                discoveryUrl: stubSchemeAppManagerModel.discoveryUrl,
                available: true,
              ),
            ),
          ).called(1);
        },
      );

      test(
        'Should update app manager with local scheme not available',
        () async {
          when(
            () => schemeClient.fetchScheme(),
          ).thenAnswer(
            (_) => Future.value(
              Scheme(
                organizations: [],
                documentTypes: [],
                appManagers: [stubSchemeAppManagerModel],
              ),
            ),
          );

          when(
            () => schemeAppManagerRepository.appManagers,
          ).thenAnswer(
            (_) => Future.value(
              [
                stubSchemeAppManagerEntity.copyWith(available: false),
              ],
            ),
          );

          final container = createContainer(
            overrides: [
              schemeClientProvider.overrideWithValue(schemeClient),
              loggingProvider.overrideWithValue(loggingRepository),
              schemeOrganizationRepositoryProvider
                  .overrideWith((ref) => schemeOrganizationRepository),
              schemeAppManagerRepositoryProvider
                  .overrideWith((ref) => schemeAppManagerRepository),
              schemeDocumentTypeRepositoryProvider
                  .overrideWith((ref) => schemeDocumentTypeRepository),
            ],
          );

          await handleSuccesTests(
            container: container,
            sut: updateLocalSchemeProvider,
            callSut: () async {
              await container.read(updateLocalSchemeProvider.future);
            },
          );

          verify(
            () => schemeAppManagerRepository.update(
              stubSchemeAppManagerEntity.copyWith(
                name: stubSchemeAppManagerModel.name,
                publicKey: stubSchemeAppManagerModel.publicKey,
                discoveryUrl: stubSchemeAppManagerModel.discoveryUrl,
                available: true,
              ),
            ),
          ).called(1);
        },
      );
    },
  );
}
