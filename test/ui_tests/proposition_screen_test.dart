import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:user_ui/screens/common/themed_button.dart';
import 'package:user_ui/screens/features/onboarding/data_use_screen.dart';
import 'package:user_ui/screens/features/onboarding/propositon_screen.dart';

import '../utilities/mock_go_router.dart';

void main() {
  testWidgets(
    'PropositionScreen renders correctly',
    (WidgetTester tester) async {
      await tester.pumpWidget(
        const MaterialApp(
          home: PropositionScreen(),
        ),
      );

      expect(find.text("Wat heeft u aan de app?"), findsOneWidget);
      expect(
        find.byWidgetPredicate(
          (widget) =>
              widget is RichText &&
              widget.text.toPlainText() ==
                  "Één overzicht van wat u nog moet betalen aan de overheid, bijvoorbeeld aan de Belastingdienst of het CJIB",
        ),
        findsOneWidget,
      );
      expect(
        find.byWidgetPredicate(
          (widget) =>
              widget is RichText &&
              widget.text.toPlainText() ==
                  'Hulp bij vragen over wat u moet betalen',
        ),
        findsOneWidget,
      );
      expect(find.bySemanticsLabel('video placeholder'), findsOneWidget);
      expect(find.byType(ThemedButton), findsOneWidget);
      expect(find.text("Volgende"), findsOneWidget);
    },
  );

  testWidgets(
    'Footer button navigates to DataUseScreen',
    (tester) async {
      final mockGoRouter = MockGoRouter();

      await tester.pumpWidget(
        MaterialApp(
          home: MockGoRouterProvider(
            goRouter: mockGoRouter,
            child: const PropositionScreen(),
          ),
        ),
      );

      await tester.ensureVisible(find.byType(ThemedButton));
      await tester.tap(find.byType(ThemedButton));
      await tester.pumpAndSettle();

      verify(
        () => mockGoRouter.goNamed(DataUseScreen.routeName),
      ).called(1);
    },
  );
}
