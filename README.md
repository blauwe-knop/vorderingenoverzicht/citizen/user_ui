# user_ui

The UI for users to see there financial claims.

## Getting Started

To get started with this project you need:
[FVM](https://fvm.app/docs/getting_started/installation), as [Flutter](https://docs.flutter.dev/get-started/install) Version Management.

## Running App
Steps to run the app locally from the command line.

First determine which flavor of the app you want to build.
* mock - run with mocked data, without backend
* local - run against a local minikube environment
* test - run against the Test environment
* demo - run against the Demo environment

### Web
Demo:
```sh
flutter run -d chrome --web-port=49430 
```

### Mobile
Lists all available devices

```sh
flutter devices
```

 Lists all available emulators

```sh
flutter emulators
```

Copie the id or name of the device/emulator you want to run the app on

```sh
flutter run -d [device id/name] --flavor=[flavor]
```

\*For flavor local also the backend services are needed, see readme of developer-tools\*

## Test

```sh
flutter test
```

### Mock generation

[Mockito Null Safety](https://github.com/dart-lang/mockito/blob/master/NULL_SAFETY_README.md)

```sh
flutter pub run build_runner build --delete-conflicting-outputs
```

### Launch Screen generation

To generate the Launch Screens, change settings in pubspec.yaml and run

```sh
dart run flutter_native_splash:create
```

### Coverage

Install lcov (Mac):

```sh
brew install lcov
```

Run tests, generate coverage files and convert to HTML:

```sh
flutter test --coverage
genhtml coverage/lcov.info -o coverage/html
```

Open `coverage/html/index.html` in your browser.

### Pushing a build to Testflight (manual)

Make sure bundler and Fastlane is installed and you have access to the certificates.
If you really want to push the build to Testflight, make sure to uncomment
`upload_to_testflight` in `fastlane/Fastfile`.

```sh
flutter build ipa
cd ios
bundle exec fastlane beta
```

#### Android APK build

To build an APK-file run the following command:

```sh
flutter build apk --release
```

#### Database build steps

Build the database:

```sh
dart run build_runner build --delete-conflicting-outputs
```

### Suggested reading

#### Fastlane

- <https://docs.flutter.dev/deployment/cd>
- <https://docs.gitlab.com/ee/ci/runners/saas/macos_saas_runner.html>
- <https://www.runway.team/blog/how-to-build-the-perfect-fastlane-pipeline-for-ios>
- <https://docs.fastlane.tools/getting-started/ios/authentication/>
- <https://docs.fastlane.tools/getting-started/ios/beta-deployment/>
- <https://docs.fastlane.tools/best-practices/continuous-integration/gitlab/>

#### Flutter

- <https://github.com/abuanwar072/Flutter-Responsive-Admin-Panel-or-Dashboard>
- <https://www.slideshare.net/BartoszKosarzycki/provider-vs-bloc-vs-redux>
- <https://github.com/rrousselGit/flutter_hooks>

# Deployment

## Versioning example
- 1.0.0     # new release
- 1.0.1     # bug fix
- 1.1.0     # new feature added
- 1.0.0+1   # build number increase

# Automatic deployments (preffered)

Automatic deployments are our preferred method due to their reliability in using accurate information and automatically incrementing build numbers.

## Utilizing GitLab CI/CD for Android and iOS
We've configured GitLab CI/CD pipelines within the gitlab-ci.yml file to handle build and upload processes for both Android and iOS applications.

* Upon merging a branch into the master branch, the build process is automatically initiated for both platforms.
* However, the upload process requires manual initiation post-build completion.

### Deployment Process for New Releases
To deploy a new release, follow these steps:

1. Merge Branch into Master: This action triggers the build process for both Android and iOS.
2. Manual Upload Trigger: After the build completes, manually initiate the upload job within the pipeline. This uploads the build to the respective app stores.
    * iOS:
        1. Navigate to App Store Connect > TestFlight.
        2. Select the newly uploaded build.
        3. Add release notes and assign testers. Confirm the release to testers upon prompting.
    * Android:
        1. Access the Google Play Store > Internal Testing.
        2. Name the release and include release notes.
        3. Proceed to the next step.
3. Review and Publish:
    * Ensure all details are accurately entered.
    * Click Save and Publish to finalize the release.

This structured approach ensures a streamlined deployment process, enhancing efficiency and reducing potential errors during the release cycle.

# Manual deployments

It is better to make use of the automatic deployments. When doing a manual deployment, this could impact the automatic releases and build number increasments.

## Android
### Manually with fastlane (preffered)

#### Build app

To manually create an android release build with fastlane, perform the following steps:

1. Make sure bundler is installed and run the following command. This installs the correct fastlane version and dotenv version.
```bash
 bundle install
```
2. Cd to the `lib/android/fastlane` folder and make a copy of the file `.env.default.sample` and rename it to `.env.default`
3. Uncomment the fields in this file and fill them in.
    - GOOGLE_APPLICATION_CREDENTIALS = Service account key
    - STORE_PASSWORD = password of the upload key store
    - KEY_PASSWORD = password of the upload key (same as storePassword)
    - KEY_ALIAS = defaults to “upload”
    - STORE_FILE = Path to the keystore.jks file
    - BUILD_FLAVOR = App flavor    
    - PACKAGE_NAME = app.blauweknop.vorderingenoverzicht.citizenui + .[flavor]
    - AAB_FILE_PATH = Path to the release build → `build/app/outputs/bundle/[flavor]Release/app-[flavor]-release.aab`
    - RELEASE_DIRECTLY = Determines if the app is released directly or is marked as draft to release later on. True or false.
4. For the store file, service account key file, and passwords, consult one of the devs.
5. Run the following command.

```bash
bundle exec fastlane android playstore_build --env .env.default

```
7. This results in an appbundle in the folder `build/app/outputs/bundle/[flavor]Release/app-[flavor]-release.aab`

<aside>
💡 One of the advantages of this method is that the build number is automatically incremented in the `get_playstore_build_number` lane.
</aside>

#### Upload app

1. Run the following command. This creates a draft internal test release in the Google Play Console.
```bash
bundle exec fastlane android playstore_upload --env .env.default
```
2. Go to the Google Play Console.
3. Open the app **Vorderingenoverzicht**
4. Navigate to `Testing → Internal Testing`
5. Give the release a name and add the release notes.

<aside>
💡 The default name for a build is as follows: build number (version code) → 1 (0.0.1)
</aside>

6. Click next
7. Check all the details and ensure everything is correctly filled in, then click **Save and publish**

### Manually without fastlane (not preffered)

#### Build app

To manually create an android release build, perform the following steps:

1. Go to the Google Play Console.
2. Open the app **Vorderingenoverzicht**
3. Navigate to `Testing → Internal Testing` and check the latest version code and build number.
4. Go to pubspec.yaml and increase the version code and build number. (See versioning examples)
5. Cd to the `lib/android` folder and make a copy of the file `key-example.properties` and rename it to `key.properties`
6. Uncomment the fields in this file and fill them in.
    - storePassword = password of the upload key store
    - keyPassword = password of the upload key (same as storePassword)
    - keyAlias = defaults to “upload”
    - storeFile = Path to the keystore.jks file
7. For the store file and passwords, consult one of the devs.
8. Run the following command
```bash
flutter build appbundle --flavor=[flavor] --target=[target]

```
9. This results in an appbundle in the folder `build/app/outputs/bundle/[flavor]Release/app-[flavor]-release.aab`

<aside>
💡 Make sure that the key.properties file is not committed in version history ‼️
</aside>

#### Upload app

1. Go to the Google Play Console.
2. Open the app **Vorderingenoverzicht**
3. Navigate to `Testing → Internal Testing`
4. Click the **Create new release** button at the top right.
5. Click upload to upload the just-generated appbundle.
6. Give the release a name and add the release notes.

<aside>
💡 The default name for a build is as follows: build number (version code) → 1 (0.0.1)
</aside>

7. Click next
8. Check all the details and ensure everything is correctly filled in, then click **Save and publish**

## iOS

For iOS app deployment, we utilize Fastlane Match, which centralizes the management of signing certificates and provisioning profiles. To streamline this process, we leverage GitLab Secure Files for storage. When Fastlane Match is invoked, it uses a shared GitLab personal access token for authentication. Upon successful authentication, all necessary certificates and profiles are automatically downloaded and installed on the user's machine or within the CI pipeline. This approach significantly reduces the time and effort required to manage certificates and profiles individually.

### Manually with fastlane (preffered)

#### Build app

To manually create an iOS release build with fastlane, follow these steps:

1. Make sure bundler is installed and run the following command. This installs the correct fastlane version and dotenv version.

```bash
bundle install
```

1. Cd to the `lib/ios/fastlane` folder and make a copy of the file `env.default.sample` and rename it to `.env.default`
2. Uncomment the fields in this file and fill them in.
    1. APP_STORE_CONNECT_API_KEY_PATH
    2. GITLAB_PAT
    3. IPA_OUTPUT_DIR
    4. IPA_NAME    
    5. BUILD_FLAVOR
    6. APP_IDENTIFIER    
3. Explanation of the fields is in the sample file. For the gitlab personal access token and passwords, consult one of the devs.
4. Run the following command.

```bash
bundle exec fastlane ios appstore_build --env .env.default
```

5. This results in an appbundle in the folder `ios/build/ipa/[IPA_NAME]`

💡 The advantage of this method is that the build number is automatically incremented in the `get_testflight_build_number` lane.

#### Upload app

1. Run the following command. This creates a Ready to Submit Testflight release in the App Store.

```bash
bundle exec fastlane ios appstore_upload --env .env.default
```
2. Go to the App Store Connect page.
3. Open the app **Vorderingenoverzicht**
4. Navigate to `TestFlight → Versions -> latest build`
5. Add the release notes under Test Details.
6. Click on Save
7. Check all the details and make sure everything is filled in correctly.
8. Add testers to the build and click on **Send invitation**.

### Manually without fastlane (not preffered)
#### Build app

To manually create an iOS release build, follow these steps:

1. Go to the App Store Connect website.
2. Open the app **Vorderingenoverzicht**
3. Navigate to `vorderingenoverzicht -> testflight` and check the latest version code and build number.
4. Go to pubspec.yaml and increase the version code and build number. (See versioning examples)
5. Cd to the `lib/ios` folder and make a copy of the file `env.default.sample` and rename it to `env.default`
6. Uncomment the fields in this file and fill them in.
    1. GITLAB_PAT
    2. RELEASE_DIRECTLY
7. Explanation of the fields is in the `.sample` file. For the gitlab personal access token, consult one of the devs.
8. Run the following command. This ensures that the correct certificates and provisioning profiles are downloaded and installed:

```bash
bundle exec fastlane match appstore
```

9. Run the following command

```bash
flutter build ipa --flavor=[flavor] --target=[target]
```

10. This results in an ipa in the folder `user_ui/build/ios/ipa/user_ui.ipa`

💡 Make sure that the env.default file is not committed in version history ‼️

#### Upload app

1. Follow the steps in the following link
 https://docs.flutter.dev/deployment/ios#upload-the-app-bundle-to-app-store-connect